<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define('SPECIAL_USER', 1);
define('USER', 2);

define('BRANCH', 1);
define('ALAMAT', 2);
define('KOTA', 3);
define('TLP', 4);
define('NIK_PDM', 5);
define('NAMA_PDM', 6);
define('POS_PDM', 7);
define('UNIT', 8);
define('BM', 9);
define('PDM', 10);
define('KODE', 11);

/* End of file constants.php */ 
/* Location: ./constants.php */
