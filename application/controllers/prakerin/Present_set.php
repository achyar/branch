<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
* Dashboard controllers class
 *
 * @package     SYSCMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */

class Present_set extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_prakerin') == NULL) {
            header("Location:" . site_url('prakerin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
    }

    // Dashboard View
    public function index($ket = 'all', $offset = NULL)
    {
        if($ket != 'all'){
            $params['desc'] = $ket;
        }
        $this->load->library('pagination');
        $this->load->model('Present_model');
        $params['limit'] = 10;
		$params['offset'] = $offset;
        $id = $params['prakerin_id'] = $this->session->userdata('prakerin_id');
        $params['prakerin_id'] = $this->session->userdata('prakerin_id');
        $data['present'] = $this->Present_model->get($params);
		$data['hadir'] = count($this->Present_model->get(array('prakerin_id' => $id, 'desc' => 'HADIR')));
        $data['sakit'] = count($this->Present_model->get(array('prakerin_id' => $id, 'desc' => 'SAKIT')));
        $data['izin'] = count($this->Present_model->get(array('prakerin_id' => $id, 'desc' => 'IZIN')));       
        $data['semua'] = count($this->Present_model->get(array('prakerin_id' => $id)));
        $config['base_url'] = site_url('prakerin/present/index/all');
        $config['total_rows'] = count($this->Present_model->get());
        $this->pagination->initialize($config);
        
        $data['title'] = 'Kehadiran';
        $data['main'] = 'prakerin/present/present';
        $this->load->view('prakerin/layout', $data);
    }

}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
