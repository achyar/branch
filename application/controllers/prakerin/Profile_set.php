<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * User controllers class
 *
 * @package     SYSCMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Sistiandy Syahbana nugraha <sistiandy.web.id>
 */
class Profile_set extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_prakerin') == NULL) {
            header("Location:" . site_url('prakerin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model('Prakerin_model');
        $this->load->model('Logs_model');
        $this->load->helper(array('form', 'url'));
    }

    // User_customer view in list
    public function index($offset = NULL) {
        $id = $this->session->userdata('prakerin_id');
        if ($this->Prakerin_model->get(array('id' => $id)) == NULL) {
            redirect('prakerin/profile');
        }
        $data['prakerin'] = $this->Prakerin_model->get(array('id' => $id));
        $data['title'] = 'Detail Profil';
        $data['main'] = 'prakerin/profile/profile_view';
        $this->load->view('prakerin/layout', $data);
    }

    // Add User_customer and Update
    public function edit($id = NULL) {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('prakerin_name', 'Nama Lengkap', 'trim|required|xss_clean');
        $this->form_validation->set_rules('prakerin_phone', 'No. Tlp/HP', 'trim|required|xss_clean');
        $this->form_validation->set_rules('prakerin_gender', 'Jenis Kelamin', 'trim|required|xss_clean');
        $this->form_validation->set_rules('prakerin_school', 'Asal Sekolah', 'trim|required|xss_clean');       
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = 'Sunting';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            $nip = $this->input->post('prakerin_nip');
            $params['prakerin_id'] = $this->input->post('prakerin_id');
            $params['prakerin_last_update'] = date('Y-m-d H:i:s');
            $params['prakerin_name'] = $this->input->post('prakerin_name');
            $params['prakerin_gender'] = $this->input->post('prakerin_gender');
            $params['prakerin_education'] = $this->input->post('prakerin_education');
            $params['prakerin_place'] = $this->input->post('prakerin_place');
            $params['prakerin_birth_date'] = $this->input->post('prakerin_birth_date');
            $params['prakerin_school'] = $this->input->post('prakerin_school');
            $params['prakerin_phone'] = $this->input->post('prakerin_phone');
            $params['prakerin_address'] = $this->input->post('prakerin_address');            
            $params['prakerin_entry_date'] = $this->input->post('prakerin_entry_date');           
            $status = $this->Prakerin_model->add($params);

            if (!empty($_FILES['inputImage']['name'])) {

                $paramsupdate['prakerin_image'] = $this->do_upload($name = 'inputImage', $nip);
            }
            $paramsupdate['prakerin_id'] = $status;
            $this->Prakerin_model->add($paramsupdate);

            $this->session->set_flashdata('success', $data['operation'] . ' Profil Berhasil');
            redirect('prakerin/profile');
        } else {

            // Edit mode
            $data['prakerin'] = $this->Prakerin_model->get(array('id' => $this->session->userdata('prakerin_id')));
            $data['button'] = 'Ubah';
            $data['title'] = $data['operation'] . ' Profil';
            $data['main'] = 'prakerin/profile/profile_edit';
            $this->load->view('prakerin/layout', $data);
        }
    }

    // Setting Upload File Requied
    function do_upload($name, $nip) {
        $this->load->library('upload');

        $config['upload_path'] = FCPATH . 'uploads/prakerin/';

        /* create directory if not exist */
        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0777, TRUE);
        }

        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '32000';
        $config['file_name'] = $nip;
        $this->upload->initialize($config);

        if (!$this->upload->do_upload($name)) {
           // echo $config['upload_path'];
//            echo $this->upload->display_errors('');
//            die();
            $this->session->set_flashdata('failed', $this->upload->display_errors('', ''));
            redirect(uri_string());
        }

        $upload_data = $this->upload->data();

        return $upload_data['file_name'];
    }

    function cpw($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('prakerin_password', 'Password', 'trim|required|xss_clean|min_length[6]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|xss_clean|min_length[6]|matches[prakerin_password]');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        if ($_POST AND $this->form_validation->run() == TRUE) {
            $id = $this->input->post('prakerin_id');
            $params['prakerin_password'] = sha1($this->input->post('prakerin_password'));
            $status = $this->Prakerin_model->change_password($id, $params);

            $this->session->set_flashdata('success', 'Ubah Password Berhasil');
            redirect('prakerin/profile');
        } else {
            if ($this->Prakerin_model->get(array('id' => $id)) == NULL) {
                redirect('prakerin/profile');
            }
            $data['prakerin'] = $this->Prakerin_model->get(array('id' => $id));
            $data['title'] = 'Ubah Password';
            $data['main'] = 'prakerin/profile/change_pass';
            $this->load->view('prakerin/layout', $data);
        }
    }

}

/* End of file user.php */
/* Location: ./application/controllers/ccp/user.php */
