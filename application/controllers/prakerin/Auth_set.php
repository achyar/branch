<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Auth controllers class
 *
 * @package     SYSCMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Auth_set extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Prakerin_model');
        $this->load->library('form_validation');
        $this->load->helper('string');
        $this->load->helper('url');
    }

    function index() {
        redirect('prakerin/auth/login');
    }

    function present($lokasi = '') {
        $this->load->model('Present_model');
        if ($this->session->userdata('logged_prakerin')) {
            redirect('prakerin');
        }

        if ($lokasi != '') {
            $lokasi = $this->input->post('location');
        } else {
            $lokasi = NULL;
        }
        $this->form_validation->set_rules('prakerin_nip', 'Nip', 'trim|required');
        $this->form_validation->set_rules('prakerin_password', 'Password', 'trim|required');
        $this->form_validation->set_rules('desc', 'Keterangan', 'trim|required');
        if ($_POST AND $this->form_validation->run() == TRUE) {
            $nip = $this->input->post('prakerin_nip', TRUE);
            $password = sha1($this->input->post('prakerin_password', TRUE));
            $desc = $this->input->post('desc', TRUE);
            $this->db->from('prakerin');
            $this->db->where('prakerin_nip', $nip);       
            $this->db->where('prakerin_password', $password);     
            $this->db->where('prakerin_status', TRUE); 
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                $params['prakerin_prakerin_nip'] = $nip;
                $params['prakerin_prakerin_id'] = $query->row('prakerin_id');
                $params['present_year'] = date('Y');
                $params['present_month'] = date('m');
                $params['present_date'] = date('Y-m-d');
                if ($desc == 0) {
                    $checkin = $this->Present_model->get(array('date' => date('Y-m-d'), 'prakerin_nip' => $nip, 'prakerin_password' => $password));
                    if (!empty($checkin)) {
                        $this->session->set_flashdata('failedpresent', 'Maaf, anda sudah mengisi jam kedatangan untuk hari ini');
                        header("Location:" . site_url('prakerin/auth/login') . "?location=" . urlencode($lokasi));
                    } else {
                        if (date('H:i:s') > '08:15:59') {
                            $params['present_is_late'] = TRUE;
                        }
                        $params['present_desc'] = 'HADIR';
                        $params['present_entry_time'] = date('H:i:s');
                        $this->Present_model->add($params);
                        if ($lokasi != '') {
                            $this->session->set_flashdata('alert', 'Selamat datang, ' . $query->row('prakerin_name') . ' absen masuk berhasil diinput.');
                            header("Location:" . site_url('prakerin/auth/login') . "?location=" . urlencode($lokasi));
                        } else {
                            $this->session->set_flashdata('alert', 'Selamat datang, ' . $query->row('prakerin_name') . ' absen masuk berhasil diinput.');
                            redirect('prakerin/auth/login');
                        }
                    }
                } else {
                    if (date('H:i:s') < '17:00:00') {
                        $params['present_is_before'] = TRUE;
                    }
                    $checkout = $this->Present_model->get(array('date' => date('Y-m-d'), 'prakerin_nip' => $nip, 'prakerin_password' => $password));
                    if (empty($checkout)) {
                        $this->session->set_flashdata('failedpresent', 'Maaf, Anda belum presensi masuk');
                        header("Location:" . site_url('prakerin/auth/login') . "?location=" . urlencode($lokasi));
                    } else {
                        $params['present_id'] = $checkout['present_id'];
                        $params['present_out_time'] = date('H:i:s');
                        $this->Present_model->add($params);
                        if ($lokasi != '') {
                            $this->session->set_flashdata('alert', 'Selamat jalan, ' . $query->row('prakerin_name') . ' Absen pulang berhasil diinput hati-hati dijalan.');
                            header("Location:" . site_url('prakerin/auth/login') . "?location=" . urlencode($lokasi));
                        } else {
                            $this->session->set_flashdata('alert', 'Selamat jalan, ' . $query->row('prakerin_name') . ' Absen pulang berhasil diinput hati-hati dijalan.');
                            redirect('prakerin/auth/login');
                        }
                    }
                }
            } else {
                if ($lokasi != '') {
                    $this->session->set_flashdata('failedpresent', 'Maaf, NIP yang anda masukan tidak terdaftar');
                    header("Location:" . site_url('prakerin/auth/login') . "?location=" . urlencode($lokasi));
                } else {
                    $this->session->set_flashdata('failedpresent', 'Maaf, NIP yang anda tidak terdaftar');
                    redirect('prakerin/auth/login');
                }
            }
        } else {
            $this->session->set_flashdata('failedpresent', 'Maaf, NIP yang anda masukan tidak terdaftar');
            header("Location:" . site_url('prakerin/auth/login') . "?location=" . urlencode($lokasi));
        }
    }

    function login($lokasi = '') {
        
        $this->load->model('Present_model');
		$this->load->model('Prakerin_model');
        $this->load->helper('text');
		$this->load->library('pagination');
        if ($this->session->userdata('logged_prakerin')) {
            redirect('prakerin');
        }
        $this->form_validation->set_rules('prakerin_nip', 'NIPM', 'trim|required');
        $this->form_validation->set_rules('prakerin_password', 'Password', 'trim|required');        
        $data['present'] = $this->Present_model->get(array('date' => date('Y-m-d')));
		$data['prakerin'] = $this->Prakerin_model->get(array('status' => TRUE,'order_by' => 'prakerin_name'));
        if ($_POST AND $this->form_validation->run() == TRUE) {
            if ($this->input->post('location')) {
                $lokasi = $this->input->post('location');
            } else {
                $lokasi = NULL;
            }
            $this->process_login($lokasi);
        } else {
            $this->load->view('prakerin/login', $data);
        }
    }

    // Login Prosessing
    function process_login($lokasi = '') {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('prakerin_nip', 'NIPM', 'required');
        $this->form_validation->set_rules('prakerin_password', 'Password', 'required');

        if ($this->form_validation->run() == TRUE) {
            $username = $this->input->post('prakerin_nip', TRUE);
            $password = $this->input->post('prakerin_password', TRUE);
            $this->db->from('prakerin');
            $this->db->where('prakerin_nip', $username);
            $this->db->where('prakerin_password', sha1($password));
            $this->db->where('prakerin_status', TRUE);
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                $this->session->set_userdata('logged_prakerin', TRUE);
                $this->session->set_userdata('prakerin_id', $query->row('prakerin_id'));
                $this->session->set_userdata('prakerin_nip', $query->row('prakerin_nip'));
                $this->session->set_userdata('prakerin_name', $query->row('prakerin_name'));
                $this->session->set_userdata('prakerin_image', $query->row('prakerin_image'));
                if ($lokasi != '') {
                    header("Location:" . htmlspecialchars($lokasi));
                } else {
                    redirect('prakerin');
                }
            } else {
                if ($lokasi != '') {
                    $this->session->set_flashdata('failed', 'Sorry, username and password do not match');
                    header("Location:" . site_url('prakerin/auth/login') . "?location=" . urlencode($lokasi));
                } else {
                    $this->session->set_flashdata('failed', 'Sorry, username and password do not match');
                    redirect('prakerin/auth/login');
                }
            }
        } else {
            $this->session->set_flashdata('failed', 'Sorry, username and password are not complete');
            redirect('prakerin/auth/login');
        }
    }

    // Logout Processing
    function logout() {
        $this->session->unset_userdata('logged_prakerin');
        $this->session->unset_userdata('prakerin_id');
        $this->session->unset_userdata('prakerin_name');
        $this->session->unset_userdata('prakerin_name');
        $this->session->unset_userdata('prakerin_image');
        if ($this->input->post('location')) {
            $lokasi = $this->input->post('location');
        } else {
            $lokasi = NULL;
        }
        header("Location:" . $lokasi);
    }

}
