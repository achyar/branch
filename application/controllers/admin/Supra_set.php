<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * supra controllers class 
 *
 * @package     HRA CMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Supra_set extends CI_Controller {

    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Supra_model', 'Logs_model', 'Prakerin_model', 'Letter_model', 'Setting_model'));
        $this->load->helper('string');
    }

    // Surat Keterangan Magang view in list
    public function index($offset = NULL) {
        $this->load->library('pagination');
        $data['supra'] = $this->Supra_model->get();
        $config['base_url'] = site_url('admin/supra/index');
        $config['total_rows'] = count($this->Supra_model->get(array('status' => TRUE)));
        $this->pagination->initialize($config);

        $data['title'] = 'Surat Keterangan Magang';
        $data['main'] = 'admin/supra/supra_list';
        $this->load->view('admin/layout', $data);
    }

    function detail($id = NULL) {
        if ($this->Supra_model->get(array('id' => $id)) == NULL) {
            redirect('admin/supra');
        }
        $data['supra'] = $this->Supra_model->get(array('id' => $id));               
        $data['title'] = 'Surat Keterangan Magang';
        $data['main'] = 'admin/supra/supra_view';
        $this->load->view('admin/layout', $data);
    }

    // Add supra and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');
        // $this->form_validation->set_rules('ket_id', 'Keterangan Surat', 'trim|required|xss_clean');         
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Update';

        if ($_POST == TRUE) {

            if ($this->input->post('supra_id')) {
                $params['supra_id'] = $this->input->post('supra_id');
            } else {

               $lastletter = $this->Letter_model->get(array('limit' => 1));
               $this->load->helper('tanggal');
               $namaBulan = konversiBulan(date('m'));
               $unit = $this->Setting_model->get(array('id' => UNIT));

               if ($lastletter['letter_year'] < date('Y') OR count($lastletter) == 0) {
                $this->Letter_model->add(array('letter_number' => '001', 'letter_month' => date('m'), 'letter_year' => date('Y')));
                $nomor = sprintf('%03d', '001');
                $nofull = $nomor . '/HRA-Ext/' . $unit['setting_value'] .'/'. $namaBulan . '/'. date('Y');
            } else {
                $nomor = sprintf('%03d', $lastletter['letter_number'] + 001);
                $this->Letter_model->add(array('letter_number' => $nomor, 'letter_month' => date('m'), 'letter_year' => date('Y')));
                $nofull = $nomor . '/HRA-Ext/' . $unit['setting_value'] .'/'. $namaBulan . '/'. date('Y');
            }

            $params['supra_input_date'] = date('Y-m-d H:i:s');

        }
        $params['supra_number'] = $nofull;
        $params['supra_nip'] = $this->input->post('prakerin_nip');
        $params['supra_name'] = $this->input->post('prakerin_name');            
        $params['supra_dept'] = $this->input->post('prakerin_dept');
        $params['supra_education'] = $this->input->post('prakerin_education');
        $params['supra_school'] = $this->input->post('prakerin_school');
        $params['supra_entry_date'] = $this->input->post('prakerin_entry_date');
        $params['supra_out_date'] = $this->input->post('prakerin_out_date');
        $params['user_id'] = $this->session->userdata('uid');
        $params['supra_last_update'] = date('Y-m-d H:i:s');
        $status = $this->Supra_model->add($params);


            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Surat Keterangan Magang',
                'log_action' => $data['operation'],
                'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('supra_name') 
                )
            );

        $this->session->set_flashdata('success', $data['operation'] . ' Surat Keterangan Magang berhasil');
        redirect('admin/supra');
    } else {
        if ($this->input->post('supra_id')) {
            redirect('admin/supra/edit/' . $this->input->post('supra_id'));
        }

            // Edit mode
        if (!is_null($id)) {
            $data['supra'] = $this->Supra_model->get(array('id' => $id));
        }
        
        $data['prakerin'] = $this->Prakerin_model->get();
        $data['title'] = $data['operation'] . ' Surat Keterangan Magang';
        $data['main'] = 'admin/supra/supra_add';
        $this->load->view('admin/layout', $data);
    } 
}

    // Delete Surat Keterangan Magang
public function delete($id = NULL) {
    if ($_POST) {
        $this->Supra_model->delete($id);
            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Surat Keterangan Magang',
                'log_action' => 'Hapus',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Hapus Surat Keterangan Magang berhasil');
        redirect('admin/supra');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/supra/edit/' . $id);
    }
} 

function printPdf($id = NULL) {
    $this->load->helper(array('dompdf'));
    $this->load->helper(array('tanggal'));
    if ($id == NULL)
        redirect('admin/supra');
    $data['supra'] = $this->Supra_model->get(array('id' => $id));
    $data['setting_employe_nik'] = $this->Setting_model->get(array('id' => NIK_PDM));
    $data['setting_employe_name'] = $this->Setting_model->get(array('id' => NAMA_PDM));
    $data['setting_employe_position'] = $this->Setting_model->get(array('id' => POS_PDM)); 
    $data['kota'] = $this->Setting_model->get(array('id' => KOTA)); 
    $data['alamat'] = $this->Setting_model->get(array('id' => ALAMAT)); 
    $data['tlp'] = $this->Setting_model->get(array('id' => TLP)); 
    $data['unit'] = $this->Setting_model->get(array('id' => UNIT));
    $data['branch'] = $this->Setting_model->get(array('id' => BRANCH));
    $html = $this->load->view('admin/supra/supra_pdf', $data, true);
    $data = pdf_create($html, $data['supra']['supra_name'], TRUE, 'A4', TRUE);
}

// Delete Ket
public function delete_ket($id = NULL) {
    if ($this->Supra_model->get_ket(array('id' => $id)) == NULL) {
        redirect('admin/supra/ket');
    }
    if ($_POST) {

        $this->Supra_model->delete_ket($this->input->post('del_id'));
            // activity log
        $this->load->model('Logs_model');
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Keterangan Surat',
                'log_action' => 'Delete',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Delete Keterangan Berhasil');
        redirect('admin/supra/ket');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/supra/ket/edit/' . $id);
    }
}

function getHitung($masa_kerja){
        $date1 = new DateTime(pretty_date($this->input->post('prakerin_entry_date'),'Y-m-d',false), strtotime($masa_kerja));
        $date2 = new DateTime(pretty_date($this->input->post('prakerin_out_date'),'Y-m-d',false));
        $interval = $date1->diff($date2);
        //echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days ";
        $data = array(
                'year'=>$interval->y,
                'month'=>$interval->m,
                'day'=>$interval->d
        );
    
        return $data;
    }


}



/* End of file supra.php */
/* Location: ./application/controllers/admin/supra.php */
