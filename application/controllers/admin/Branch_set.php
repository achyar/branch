<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
/**
 * branch controllers class
 *
 * @package     HCA
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Branch_set extends CI_Controller {
 
    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Branch_model', 'Logs_model'));
        $this->load->helper('string');
    }

    // branch view in list
    public function index($offset = NULL) {
        $this->load->library('pagination');
        $data['branch'] = $this->Branch_model->get();
        $config['base_url'] = site_url('admin/branch/index');
        $config['total_rows'] = count($this->Branch_model->get(array('status' => TRUE)));
        $this->pagination->initialize($config);

        $data['title'] = 'Master Branch';
        $data['main'] = 'admin/branch/branch_list';
        $this->load->view('admin/layout', $data);
    }

    function detail($id = NULL) {
        if ($this->Branch_model->get(array('id' => $id)) == NULL) {
            redirect('admin/branch');
        }
        $data['branch'] = $this->Branch_model->get(array('id' => $id));
        $data['title'] = 'Detail branch';
        $data['main'] = 'admin/branch/branch_view';
        $this->load->view('admin/layout', $data);
    }

    // Add branch and Update
    public function add($id = NULL) {
        $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

        if ($_POST == TRUE) {

            if ($this->input->post('branch_id')) {
                $params['branch_id'] = $this->input->post('branch_id');
            } else {                
                
            }

            $params['branch_kode'] = $this->input->post('branch_kode');
            $params['branch_nama'] = $this->input->post('branch_nama');  
            $params['user_id'] = $this->session->userdata('uid');            
            $status = $this->Branch_model->add($params);

            // activity log
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('uid'),
                    'log_module' => 'branch',
                    'log_action' => $data['operation'],
                    'log_info' => 'ID:'.$status.';Title:' . $params['branch_nama']
                    )
                );

            $this->session->set_flashdata('success', $data['operation'] . ' branch berhasil');
            redirect('admin/branch');
        } else {
            if ($this->input->post('branch_id')) {
                redirect('admin/branch/edit/' . $this->input->post('branch_id'));
            }

            // Edit mode
            if (!is_null($id)) {
                $data['branch'] = $this->Branch_model->get(array('id' => $id));
            }
            $data['title'] = $data['operation'] . ' branch';
            $data['main'] = 'admin/branch/branch_add';
            $this->load->view('admin/layout', $data);
        }
    }

    // Delete branch
    public function delete($id = NULL) {
        if ($_POST) {
            $this->Branch_model->delete($id);
            // activity log
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('uid'),
                    'log_module' => 'branch',
                    'log_action' => 'Hapus',
                    'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_nama')
                    )
                );
            $this->session->set_flashdata('success', 'Hapus branch berhasil');
            redirect('admin/branch');
        } elseif (!$_POST) {
            $this->session->set_flashdata('delete', 'Delete');
            redirect('admin/branch/edit/' . $id);
        }
    }

}

/* End of file branch.php */
/* Location: ./application/controllers/admin/branch.php */
