<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * agenda controllers class
 *
 * @package     HCA
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Agenda_set extends CI_Controller {

    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Agenda_model', 'Logs_model'));
        $this->load->helper('string');
    }

    // view in list
    public function index($offset = NULL) {
      $this->load->library('pagination');
      // Apply Filter
      // Get $_GET variable
      $f = $this->input->get(NULL, TRUE);

      $data['f'] = $f;

      $params = array();
      // Nip
      if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
          $params['agenda_date'] = $f['n'];
      }

      $paramsPage = $params;
      $params['limit'] = 5;
      $params['offset'] = $offset;
      $data['agenda'] = $this->Agenda_model->get($params);
 
      $config['per_page'] = 5;
      $config['uri_segment'] = 4;
      $config['base_url'] = site_url('admin/agenda/index');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['total_rows'] = count($this->Agenda_model->get($paramsPage));
      $this->pagination->initialize($config);

      $data['title'] = 'Agenda';
      $data['main'] = 'admin/agenda/agenda_list';
      $this->load->view('admin/layout', $data);
  }

    function view($id = NULL) {
        if ($this->Agenda_model->get(array('id' => $id)) == NULL) {
            redirect('admin/agenda');
        }
        $data['agenda'] = $this->Agenda_model->get(array('id' => $id));
        $data['title'] = 'Detail Agenda';
        $data['main'] = 'admin/agenda/agenda_view';
        $this->load->view('admin/layout', $data);
    }

    // Add agenda and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('agenda_text', 'Isi Agenda', 'trim|required|xss_clean');
        $this->form_validation->set_rules('agenda_date', 'Tanggal', 'trim|required|xss_clean');
        $this->form_validation->set_rules('agenda_status', 'Status', 'trim|required|xss_clean');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('agenda_id')) {
                $params['agenda_id'] = $this->input->post('agenda_id');
            } else {
                $params['agenda_input_date'] = date('Y-m-d H:i:s');
            }

            $params['agenda_text'] = $this->input->post('agenda_text');
            $params['agenda_date'] = $this->input->post('agenda_date');
            $params['agenda_status'] = $this->input->post('agenda_status');
            $params['user_id'] = $this->session->userdata('uid');
            $status = $this->Agenda_model->add($params);


            // activity log
            $this->Logs_model->add(
                    array(
                        'log_date' => date('Y-m-d H:i:s'),
                        'user_id' => $this->session->userdata('user_id'),
                        'log_module' => 'Agenda',
                        'log_action' => $data['operation'],
                        'log_info' => 'ID:' . $status . ';Title:' . $params['agenda_text']
                    )
            );

            $this->session->set_flashdata('success', $data['operation'] . ' Agenda berhasil');
            redirect('admin/agenda');
        } else {
            if ($this->input->post('agenda_id')) {
                redirect('admin/agenda/edit/' . $this->input->post('agenda_id'));
            }

            // Edit mode
            if (!is_null($id)) {
                $data['agenda'] = $this->Agenda_model->get(array('id' => $id));
            }
            $data['title'] = $data['operation'] . ' Agenda';
            $data['main'] = 'admin/agenda/agenda_add';
            $this->load->view('admin/layout', $data);
        }
    }

    // Delete agenda
    public function delete($id = NULL) {
        if ($id == NULL) {

            if ($this->session->userdata('uroleid') != SPECIAL_USER) {
                redirect('admin/agenda');
            } else {
                $this->Agenda_model->delete_all();
                redirect('admin/agenda');
            }
        }

        if ($_POST) {
            $this->Agenda_model->delete($id);
            // activity log
            $this->Logs_model->add(
                    array(
                        'log_date' => date('Y-m-d H:i:s'),
                        'user_id' => $this->session->userdata('uid'),
                        'log_module' => 'Agenda',
                        'log_action' => 'Hapus',
                        'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                    )
            );
            $this->session->set_flashdata('success', 'Hapus Agenda berhasil');
            redirect('admin/agenda');
        } elseif (!$_POST) {
            $this->session->set_flashdata('delete', 'Delete');
            redirect('admin/agenda/edit/' . $id);
        }
    }

}

/* End of file agenda.php */
/* Location: ./application/controllers/admin/agenda.php */
