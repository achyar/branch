<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * catalog controllers class 
 *
 * @package     Neptune
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Catalog_set extends CI_Controller {

    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Catalog_model', 'Logs_model', 'Category_model', 'Supplier_model'));
        $this->load->helper(array('form', 'url'));
    }

    public function index($offset = NULL) {
        $this->load->library('pagination');
        // Apply Filter
        // Get $_GET variable
        $f = $this->input->get(NULL, TRUE);

        $data['f'] = $f;

        $params = array();
        // SKU
        if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
            $params['catalog_sku'] = $f['n'];
        }

        if (isset($f['k']) && !empty($f['k']) && $f['k'] != '') {
            $params['catalog_name'] = $f['k'];
        }
        
        $paramsPage = $params;
        $params['limit'] = 5;
        $params['offset'] = $offset;
        $data['catalog'] = $this->Catalog_model->get($params);
        
        $config['per_page'] = 5;
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/catalog/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['total_rows'] = count($this->Catalog_model->get($paramsPage));
        $this->pagination->initialize($config);

        $data['title'] = 'Item Stock';
        $data['main'] = 'admin/catalog/catalog_list';
        $this->load->view('admin/layout', $data);
    }

    function detail($id = NULL) {
        if ($this->Catalog_model->get(array('id' => $id)) == NULL) {
            redirect('admin/catalog');
        }
        $data['catalog'] = $this->Catalog_model->get(array('id' => $id)); 
        $data['image'] = $this->Catalog_model->get_image(array('catalog_id' => $id));              
        $data['title'] = 'Item Stock';
        $data['main'] = 'admin/catalog/catalog_view';
        $this->load->view('admin/layout', $data);
    }

    // Add catalog and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('variant_id', 'Varian', 'trim|required|xss_clean');         
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Update';

        if ($_POST AND $this->form_validation->run() == TRUE) {

             if (isset($_FILES['inputGambar']['name'])) {
                $params['catalog_image'] = $this->do_upload();
            }

            if ($this->input->post('catalog_id')) {
                $params['catalog_id'] = $this->input->post('catalog_id');
            } else {

                $params['catalog_input_date'] = date('Y-m-d H:i:s');

            }

            $params['catalog_sku'] = $this->input->post('catalog_sku');
            $params['catalog_name'] = $this->input->post('catalog_name');
            $params['catalog_bhn'] = $this->input->post('catalog_bhn');            
            $params['catalog_model'] = $this->input->post('catalog_model');
            $params['catalog_weight'] = $this->input->post('catalog_weight');
            $params['catalog_price'] = $this->input->post('catalog_price');
            $params['catalog_desc'] = $this->input->post('catalog_desc');
            $params['catalog_stock'] = $this->input->post('catalog_stock');
            $params['variant_variant_id'] = $this->input->post('variant_id');
            $params['category_category_id'] = $this->input->post('category_id');
            $params['supplier_supplier_id'] = $this->input->post('supplier_id');
            $params['user_id'] = $this->session->userdata('user_id');
            $params['catalog_last_update'] = date('Y-m-d H:i:s');
            $status = $this->Catalog_model->add($params);
            $this->do_upload($status);


            // activity log
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('user_id'),
                    'log_module' => 'Item Stock',
                    'log_action' => $data['operation'],
                    'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('catalog_name') 
                    )
                );

            $this->session->set_flashdata('success', $data['operation'] . ' Item Stock berhasil');
            redirect('admin/catalog');
        } else {
            if ($this->input->post('catalog_id')) {
                redirect('admin/catalog/edit/' . $this->input->post('catalog_id'));
            }

            // Edit mode
            if (!is_null($id)) {
                $data['catalog'] = $this->Catalog_model->get(array('id' => $id));
                $data['image'] = $this->Catalog_model->get_image(array('catalog_id' => $id));
            }
            $data['variant'] = $this->Catalog_model->get_variant();
            $data['category'] = $this->Category_model->get();
            $data['supplier'] = $this->Supplier_model->get();
            $data['ngapp'] = 'ng-app="comApp"'; 
            $data['title'] = $data['operation'] . ' Item Stock';
            $data['main'] = 'admin/catalog/catalog_add';
            $this->load->view('admin/layout', $data);
        } 
    }

    // Delete Item Stock
    public function delete($id = NULL) {
        if ($_POST) {
            $this->Catalog_model->delete($id);
            // activity log
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('user_id'),
                    'log_module' => 'Item Stock',
                    'log_action' => 'Hapus',
                    'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                    )
                );
            $this->session->set_flashdata('success', 'Hapus Item Stock berhasil');
            redirect('admin/catalog');
        } elseif (!$_POST) {
            $this->session->set_flashdata('delete', 'Delete');
            redirect('admin/catalog/edit/' . $id);
        }
    } 


// Delete Ket
    public function delete_variant($id = NULL) {
        if ($this->Catalog_model->get_variant(array('id' => $id)) == NULL) {
            redirect('admin/catalog/variant');
        }
        if ($_POST) {

            $this->Catalog_model->delete_variant($this->input->post('del_id'));
            // activity log
            $this->load->model('Logs_model');
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('user_id'),
                    'log_module' => 'Item Stock',
                    'log_action' => 'Delete',
                    'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                    )
                );
            $this->session->set_flashdata('success', 'Delete Item Berhasil');
            redirect('admin/catalog/variant');
        } elseif (!$_POST) {
            $this->session->set_flashdata('delete', 'Delete');
            redirect('admin/catalog/variant/edit/' . $id);
        }
    }

     // User view in list
    public function variant($offset = NULL) {
        $this->load->library('pagination');

        $data['variant'] = $this->Catalog_model->get_variant(array('limit' => 10, 'offset' => $offset));
        $data['title'] = 'Daftar Varian';
        $data['main'] = 'admin/catalog/variant_list';
        $config['base_url'] = site_url('admin/variant');
        $config['total_rows'] = count($this->Catalog_model->get_variant());
        $this->pagination->initialize($config);

        $this->load->view('admin/layout', $data);
    }


    // Add User_customer and Update
    public function add_variant($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('variant_name', 'Name', 'trim|required|xss_clean');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button ket="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('variant_id')) {
                $params['variant_id'] = $this->input->post('variant_id');
            }
            $params['variant_name'] = $this->input->post('variant_name');
            $status = $this->Catalog_model->add_variant($params);

            // activity log
            $this->load->model('Logs_model');
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('user_id'),
                    'log_module' => 'Keterangan Varian',
                    'log_action' => $data['operation'],
                    'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('variant_name')
                    )
                );

            $this->session->set_flashdata('success', $data['operation'] . ' Keterangan Varian');
            redirect('admin/catalog/add');

            if ($this->input->post('from_angular')) {
                echo $status;
            }
        } else {
            if ($this->input->post('variant_id')) {
                redirect('admin/catalog/variant/edit/' . $this->input->post('variant_id'));
            }

            // Edit mode
            if (!is_null($id)) {
                $object = $this->Catalog_model->get_variant(array('id' => $id));
                if ($object == NULL) {
                    redirect('admin/catalog/variant');
                } else {
                    $data['variant'] = $object;
                }
            }
            $data['title'] = $data['operation'] . ' Keterangan Item';
            $data['main'] = 'admin/catalog/variant_add';
            $this->load->view('admin/layout', $data);
        }
    }

    function do_upload($id = null)
    {
        if (is_null($id)) {
            return false;
        }

        $ret = '';

        $config['upload_path'] = './uploads';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG';
        
        $this->load->library('upload', $config);

        $no = 0;

        $files = $_FILES['catalogImage'];
        
        $cpt = count($_FILES['catalogImage']['name']);
        
        for ($i = 0; $i < $cpt; $i++) {

            $_FILES['catalogImage']['name'] = $files['name'][$i];
            $_FILES['catalogImage']['type'] = $files['type'][$i];
            $_FILES['catalogImage']['tmp_name'] = $files['tmp_name'][$i];
            $_FILES['catalogImage']['error'] = $files['error'][$i];
            $_FILES['catalogImage']['size'] = $files['size'][$i];

            $this->upload->do_upload('catalogImage');

            $data = $this->upload->data();

            $input['name'] = base_url('uploads'). '/' . $data['file_name'];
            $input['catalog_id'] = $id;

            if ($data['file_name'] != '') {
                $this->Catalog_model->add_catalog_image($input);
            }

            if ($i == 0) {
                if ($data['file_name'] != '') {
                    $cover['catalog_id'] = $id;
                    $cover['catalog_image'] =  $input['name'];
                    $this->Catalog_model->add($cover);

                    $ret = $cover['catalog_image'];
                }
            }
        }
        
        return $ret;
    }

    public function delete_image($id = null, $catalog_id = null)
    {

        if (is_null($id) OR is_null($catalog_id)) {
            redirect('gadmin/catalog/');
        }


        $catalog_image = $this->Catalog_model->get_image(array('id' => $id, 'catalog_id' => $catalog_id));

        if (count($catalog_image) == 0) {

            $this->session->set_flashdata('success', 'Gambar tidak ditemukan');
            redirect('admin/catalog/detail/'.$catalog_id);
            return false;

        }

        $ret = $this->Catalog_model->delete_image($id);

        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('user_id'),
                'log_module' => 'Foto Katalog',
                'log_action' => 'Hapus',
                'log_info' => 'ID:' . $id . ';'
                )
            );

        
        $this->session->set_flashdata('success', 'Hapus Gambar katalog berhasil');
        redirect('admin/catalog/detail/'.$catalog_id);
    }

}



/* End of file catalog.php */
/* Location: ./application/controllers/admin/catalog.php */
