<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * prakerin controllers Class
 *
 * @package     HRA 
 * @subpackage  Controllers
 * @category    Controllers 
 * @author      Achyar Anshorie
 */
class Prakerin_set extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Prakerin_model', 'Logs_model', 'Employee_model'));
        $this->load->helper('string');
    }

    public function index($offset = NULL) {
        $this->load->library('pagination');

        $q = $this->input->get(NULL, TRUE);

        $data['q'] = $q;
        $params = array(); 

        // Date start
        if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
            $params['date_start'] = $q['ds'];
        }

        // Date end
        if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
            $params['date_end'] = $q['de'];
        }

        $paramsPage = $params;
        $params['limit'] = 5;
        $params['offset'] = $offset;
        $data['prakerin'] = $this->Prakerin_model->get($params);

        $config['per_page'] = 5;
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/prakerin/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['total_rows'] = count($this->Prakerin_model->get($paramsPage));
        $this->pagination->initialize($config);

        $data['title'] = 'Prakerin';
        $data['main'] = 'admin/prakerin/prakerin_list';
        $this->load->view('admin/layout', $data);
    }

    // Add User and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');

        if (!$this->input->post('prakerin_id')) {

            $this->form_validation->set_rules('prakerin_password', 'Password', 'trim|required|min_length[6]|xss_clean');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|min_length[6]|matches[prakerin_password]');
        }
        $this->form_validation->set_rules('prakerin_name', 'Nama Lengkap', 'trim|required|xss_clean');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button position="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('prakerin_id')) {
                $params['prakerin_id'] = $this->input->post('prakerin_id'); 
                
            } else {                
                $params['prakerin_password'] = sha1($this->input->post('prakerin_password'));
                $params['prakerin_input_date'] = date('Y-m-d H:i:s'); 

                $lastnip = $this->Prakerin_model->get(array('order_by' => 'prakerin_id', 'limit' => 1));
                // $num['month'][] = $month;
                // $maxmonth = max($num['month']);
                if (empty($lastnip)) {
                    $dgt = sprintf('%02d', 01);
                    $nip = pretty_date($this->input->post('prakerin_entry_date'),'Ym',false) . $dgt;
                } else {
                    if ((pretty_date($lastnip['prakerin_entry_date'],'m',false) < pretty_date($this->input->post('prakerin_entry_date'),'m',false)) OR ((pretty_date($lastnip['prakerin_entry_date'],'Y',false) < pretty_date($this->input->post('prakerin_entry_date'),'Y',false))) ) {
                        $dgt = sprintf('%02d', 01);
                        $nip = pretty_date($this->input->post('prakerin_entry_date'),'Ym',false) . $dgt; 
                    } else {
                        $run = substr($lastnip['prakerin_nip'], 6, 2);
                        $dgt = sprintf('%02d', $run + 01);
                        $nip = pretty_date($this->input->post('prakerin_entry_date'),'Ym',false) . $dgt;
                    }
                }
            }
            
            $params['prakerin_nip'] = $nip;
            $params['prakerin_name'] = $this->input->post('prakerin_name');
            $params['prakerin_place'] = $this->input->post('prakerin_place');
            $params['prakerin_birth_date'] = $this->input->post('prakerin_birth_date');
            $params['prakerin_education'] = $this->input->post('prakerin_education');
            $params['prakerin_school'] = $this->input->post('prakerin_school');
            $params['prakerin_address'] = $this->input->post('prakerin_address');
            $params['prakerin_gender'] = $this->input->post('prakerin_gender');
            $params['prakerin_entry_date'] = $this->input->post('prakerin_entry_date');
            $params['prakerin_out_date'] = $this->input->post('prakerin_out_date');
            $params['prakerin_phone'] = $this->input->post('prakerin_phone');
            $params['prakerin_status'] = $this->input->post('prakerin_status');
            $params['prakerin_mentor_nik'] = $this->input->post('employee_nik');
            $params['prakerin_mentor_name'] = $this->input->post('employee_name');
            $params['prakerin_dept'] = $this->input->post('employee_dept');
            $params['user_id'] = $this->session->userdata('uid');
            $params['prakerin_last_update'] = date('Y-m-d H:i:s');
            $status = $this->Prakerin_model->add($params);

            if (!empty($_FILES['inputImage']['name'])) {

                $paramsupdate['prakerin_image'] = $this->do_upload($name = 'inputImage', $nip);
            }
            $paramsupdate['prakerin_id'] = $status;
            $this->Prakerin_model->add($paramsupdate);

            // activity log
            $this->load->model('Logs_model');
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('uid'),
                    'log_module' => 'Prakerin',
                    'log_action' => $data['operation'],
                    'log_info' => 'ID:' . $status . ';Name:' . $this->input->post('prakerin_name')
                    )
                );

            $this->session->set_flashdata('success', $data['operation'] . ' Prakerin Berhasil');
            redirect('admin/prakerin');
        } else {
            if ($this->input->post('prakerin_id')) {
                redirect('admin/prakerin/edit/' . $this->input->post('prakerin_id'));
            }

            // Edit mode
            if (!is_null($id)) {
                $data['prakerin'] = $this->Prakerin_model->get(array('id' => $id));
            }

            $data['employee'] = $this->Employee_model->get();
            $data['title'] = $data['operation'] . ' Prakerin';
            $data['main'] = 'admin/prakerin/prakerin_add';
            $this->load->view('admin/layout', $data);
        }
    }

    function view($id = NULL) {
        $object = $this->Prakerin_model->get(array('id' => $id));
        if ($object == NULL) {
            redirect('admin/prakerin');
        }
        $data['prakerin'] = $object;
        $data['title'] = 'Detail Prakerin';
        $data['main'] = 'admin/prakerin/prakerin_view';
        $this->load->view('admin/layout', $data);
    }

    // Delete prakerin
    public function delete($id = NULL) {
        if ($this->Prakerin_model->get(array('id' => $id)) == NULL) {
            redirect('admin/prakerin');
        }
        if ($_POST) {

            $this->Prakerin_model->delete($this->input->post('del_id'));
            // activity log
            $this->load->model('Logs_model');
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user)id' => $this->session->userdata('uid'),
                    'log_module' => 'prakerin',
                    'log_action' => 'Delete',
                    'log_info' => 'ID:' . $this->input->post('del_id') . ';Name:' . $this->input->post('del_name')
                    )
                );
            $this->session->set_flashdata('success', 'Hapus Prakerin Berhasil');
            redirect('admin/prakerin');
        } elseif (!$_POST) {
            $this->session->set_flashdata('delete', 'Delete');
            redirect('admin/prakerin/edit/' . $id);
        }
    }

// Setting Upload File Requied
    function do_upload($name, $nip) {
        $this->load->library('upload');

        $config['upload_path'] = FCPATH . 'uploads/prakerin/';

        /* create directory if not exist */
        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0777, TRUE);
        }

        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '32000';
        $config['file_name'] = $nip;
        $this->upload->initialize($config);

        if (!$this->upload->do_upload($name)) {
           // echo $config['upload_path'];
//            echo $this->upload->display_errors('');
//            die();
            $this->session->set_flashdata('failed', $this->upload->display_errors('', ''));
            redirect(uri_string());
        }

        $upload_data = $this->upload->data();

        return $upload_data['file_name'];
    }

    function rpw($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('prakerin_password', 'Password', 'trim|required|xss_clean|min_length[3]');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        if ($_POST AND $this->form_validation->run() == TRUE) {
            $id = $this->input->post('prakerin_id');
            $params['prakerin_password'] = sha1($this->input->post('prakerin_password'));
            $status = $this->Prakerin_model->change_password($id, $params);

            // activity log
            $this->load->model('Logs_model');
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('uid'),
                    'log_module' => 'prakerin',
                    'log_action' => 'Reset Password',
                    'log_info' => 'ID:null;Title:' . $this->input->post('prakerin_name')
                    )
                );
            $this->session->set_flashdata('success', 'Reset Password Berhasil');
            redirect('admin/prakerin');
        } else {
            if ($this->Prakerin_model->get(array('id' => $id)) == NULL) {
                redirect('admin/prakerin');
            }
            $data['prakerin'] = $this->Prakerin_model->get(array('id' => $id));
            $data['title'] = 'Reset Password';
            $data['main'] = 'admin/prakerin/change_pass';
            $this->load->view('admin/layout', $data);
        }
    }    

    public function export_excel()
    {
        // Apply Filter
        // Get $_GET variable
        $q = $this->input->get(NULL, TRUE);

        $data['q'] = $q;

        $params = array();

        // Date start
        if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
            $params['date_start'] = $q['ds'];
        }

        // Date end
        if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
            $params['date_end'] = $q['de'];
        }


        $data['prakerin'] = $this->Prakerin_model->get($params);

        $this->load->library("PHPExcel");
        $objXLS   = new PHPExcel();
        $objSheet = $objXLS->setActiveSheetIndex(0);            
        $cell     = 2;        
        $no       = 1;

        $objSheet->setCellValue('A1', 'NO');
        $objSheet->setCellValue('B1', 'NIPM');
        $objSheet->setCellValue('C1', 'NAMA');
        $objSheet->setCellValue('D1', 'JENIS KELAMIN');
        $objSheet->setCellValue('E1', 'TEMPAT LAHIR');
        $objSheet->setCellValue('F1', 'TANGGAL LAHIR');
        $objSheet->setCellValue('G1', 'JENJANG PENDIDIKAN');
        $objSheet->setCellValue('H1', 'NAMA SEKOLAH');
        $objSheet->setCellValue('I1', 'DEPARTEMEN');
        $objSheet->setCellValue('J1', 'PEMBIMBING');
        $objSheet->setCellValue('K1', 'TANGGAL MASUK');
        $objSheet->setCellValue('L1', 'STATUS');
        

        foreach ($data['prakerin'] as $row) {

            $objSheet->setCellValue('A'.$cell, $no);
            $objSheet->setCellValueExplicit('B'.$cell, $row['prakerin_nip'],PHPExcel_Cell_DataType::TYPE_STRING);
            $objSheet->setCellValue('C'.$cell, $row['prakerin_name']);
            $objSheet->setCellValue('D'.$cell, $row['prakerin_gender']);
            $objSheet->setCellValue('E'.$cell, $row['prakerin_place']);
            $objSheet->setCellValue('F'.$cell, pretty_date($row['prakerin_birth_date'],'d/m/Y',false));
            $objSheet->setCellValue('G'.$cell, $row['prakerin_education']);
            $objSheet->setCellValue('H'.$cell, $row['prakerin_school']);            
            $objSheet->setCellValue('I'.$cell, $row['prakerin_dept']);
            $objSheet->setCellValue('J'.$cell, $row['prakerin_mentor_name']);
            $objSheet->setCellValue('K'.$cell, pretty_date($row['prakerin_entry_date'], 'd/m/Y', FALSE));
            $objSheet->setCellValue('L'.$cell, ($row['prakerin_status'] == 0) ? 'AKTIF' : 'NON AKTIF');               
            $cell++;
            $no++;    
        }                    

        $objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(9);
        $objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(30);

        foreach(range('D', 'Z') as $alphabet)
        {
            $objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(20);
        }

        $objXLS->getActiveSheet()->getColumnDimension('L')->setWidth(20);

        $font = array('font' => array( 'bold' => true));
        $objXLS->getActiveSheet()
        ->getStyle('A1:L1')
        ->applyFromArray($font);

        $objXLS->setActiveSheetIndex(0);        
        $styleArray = array(
          'borders' => array(
           'allborders' => array(
               'style' => PHPExcel_Style_Border::BORDER_THIN,
               'color' => array(
                  'rgb'  => '111111' 
                  ),
               ),
           ),
          );
        $objXLS->getActiveSheet()
        ->getStyle('A1:L1')
        ->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('00bbff');

        $objSheet->getStyle('A1:L'.$no)->applyFromArray($styleArray);
        $objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="HRD_PRKRN_'.date('dmY').'.xls"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter->save('php://output'); 
        exit();      
    }

}
