<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * supplier controllers class 
 *
 * @package     Neptune
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Supplier_set extends CI_Controller {

    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Supplier_model', 'Logs_model'));
        $this->load->helper(array('form', 'url'));
    }

    public function index($offset = NULL) {
        $this->load->library('pagination');
        // Apply Filter
        // Get $_GET variable
        $f = $this->input->get(NULL, TRUE);

        $data['f'] = $f;

        $params = array();
        // Nip
        if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
            $params['supplier_name'] = $f['n'];
        }
        
        $paramsPage = $params;
        $params['limit'] = 5;
        $params['offset'] = $offset;
        $data['supplier'] = $this->Supplier_model->get($params);
        
        $config['per_page'] = 5;
        $config['uri_segment'] = 4;
        $config['base_url'] = site_url('admin/supplier/index');
        $config['suffix'] = '?' . http_build_query($_GET, '', "&");
        $config['total_rows'] = count($this->Supplier_model->get($paramsPage));
        $this->pagination->initialize($config);

        $data['title'] = 'Pengguna';
        $data['main'] = 'admin/supplier/supplier_list';
        $this->load->view('admin/layout', $data);
    }

    function detail($id = NULL) {
        if ($this->Supplier_model->get(array('id' => $id)) == NULL) {
            redirect('admin/supplier');
        }
        $data['supplier'] = $this->Supplier_model->get(array('id' => $id));               
        $data['title'] = 'Supplier';
        $data['main'] = 'admin/supplier/supplier_view';
        $this->load->view('admin/layout', $data);
    }

    // Add supplier and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('supplier_name', 'Varian', 'trim|required|xss_clean');         
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Update';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('supplier_id')) {
                $params['supplier_id'] = $this->input->post('supplier_id');
            } else {

                $params['supplier_input_date'] = date('Y-m-d H:i:s');

            }

            $params['supplier_name'] = $this->input->post('supplier_name');
            $params['supplier_phone'] = $this->input->post('supplier_phone');
            $params['supplier_last_update'] = date('Y-m-d H:i:s');
            $status = $this->Supplier_model->add($params);


            // activity log
            $this->Logs_model->add(
                array(
                    'log_date' => date('Y-m-d H:i:s'),
                    'user_id' => $this->session->userdata('user_id'),
                    'log_module' => 'Supplier',
                    'log_action' => $data['operation'],
                    'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('supplier_name') 
                    )
                );

            $this->session->set_flashdata('success', $data['operation'] . ' Supplier berhasil');
            redirect('admin/supplier');
        } else {
            if ($this->input->post('supplier_id')) {
                redirect('admin/supplier/edit/' . $this->input->post('supplier_id'));
            }

            // Edit mode
        
        if (!is_null($id)) {
            $data['supplier'] = $this->Supplier_model->get(array('id' => $id));
        }
        $data['title'] = $data['operation'] . ' Supplier';
        $data['main'] = 'admin/supplier/supplier_add';
        $this->load->view('admin/layout', $data);
    }
}

    // Delete Item Stock
public function delete($id = NULL) {
    if ($_POST) {
        $this->Supplier_model->delete($id);
            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('user_id'),
                'log_module' => 'Supplier',
                'log_action' => 'Hapus',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Hapus Supplier berhasil');
        redirect('admin/supplier');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/supplier/edit/' . $id);
    }
} 

}



/* End of file supplier.php */
/* Location: ./application/controllers/admin/supplier.php */
