<?php

defined('BASEPATH') OR exit('No direct script access allowed');

    /**
     * Dashboard controllers Class
     *
     * @package     Arca CMS
     * @subpackage  Controllers
     * @category    Controllers
     * @author      Achyar Anshorie
     */
    class Dashboard_set extends CI_Controller {

      public function __construct()
      {
        parent::__construct();
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
    }

    public function index() {
        $this->load->model('Users_model');


        $data['user'] = count($this->Users_model->get());
        $data['title'] = 'Dashboard';
        $data['main'] = 'admin/dashboard/dashboard';
        $this->load->view('admin/layout', $data);
    }

}
