<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * bap controllers class 
 *
 * @package     HRA CMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Bap_set extends CI_Controller {

    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Bap_model', 'Logs_model', 'Branch_model'));
        $this->load->helper('string');
    }

    // Rekap BAP view in list
    public function index($offset = NULL) {
        $this->load->library('pagination');

        $q = $this->input->get(NULL, TRUE);

        $data['q'] = $q;
        $params = array(); 

        // Date start
        if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
            $params['date_start'] = $q['ds'];
        }

        // Date end
        if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
            $params['date_end'] = $q['de'];
        }

        $data['bap'] = $this->Bap_model->get($params);
        $config['base_url'] = site_url('admin/bap/index');
        $config['total_rows'] = count($this->Bap_model->get(array('status' => TRUE)));
        $this->pagination->initialize($config);

        $data['title'] = 'Rekap BAP';
        $data['main'] = 'admin/bap/bap_list';
        $this->load->view('admin/layout', $data);
    }

    function detail($id = NULL) {
        if ($this->Bap_model->get(array('id' => $id)) == NULL) {
            redirect('admin/bap');
        }
        $data['bap'] = $this->Bap_model->get(array('id' => $id));               
        $data['title'] = 'Rekap BAP';
        $data['main'] = 'admin/bap/bap_view';
        $this->load->view('admin/layout', $data);
    }

    // Add bap and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('bap_karyawan_nik', 'NIK Karyawan', 'trim|required|xss_clean');         
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Update';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('bap_id')) {
                $params['bap_id'] = $this->input->post('bap_id');
            } else {
             $params['bap_karyawan_nik'] = $this->input->post('bap_karyawan_nik');
             $params['bap_input_date'] = date('Y-m-d H:i:s');

         }

         $params['bap_karyawan_nama'] = $this->input->post('bap_karyawan_nama');            
         $params['bap_tgl_kejadian'] = $this->input->post('bap_tgl_kejadian');
         $params['bap_tgl_surat'] = $this->input->post('bap_tgl_surat');
         $params['bap_proses'] = $this->input->post('bap_proses');
         $params['branch_branch_id'] = $this->input->post('branch_id');
         $params['desc_desc_id'] = $this->input->post('desc_id');
         $params['user_id'] = $this->session->userdata('uid');
         $params['bap_last_update'] = date('Y-m-d H:i:s');
         $status = $this->Bap_model->add($params);


            // activity log
         $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Rekap BAP',
                'log_action' => $data['operation'],
                'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('bap_karyawan_nama') 
                )
            );

         $this->session->set_flashdata('success', $data['operation'] . ' Rekap BAP berhasil');
         redirect('admin/bap');
     } else {
        if ($this->input->post('bap_id')) {
            redirect('admin/bap/edit/' . $this->input->post('bap_id'));
        }

            // Edit mode
        if (!is_null($id)) {
            $data['bap'] = $this->Bap_model->get(array('id' => $id));
        }
        $data['ngapp'] = 'ng-app="comApp"';
        $data['desc'] = $this->Bap_model->get_desc();
        $data['branch'] = $this->Branch_model->get();
        $data['title'] = $data['operation'] . ' Rekap BAP';
        $data['main'] = 'admin/bap/bap_add';
        $this->load->view('admin/layout', $data);
    } 
}

    // Delete Rekap BAP
public function delete($id = NULL) {
    if ($_POST) {
        $this->Bap_model->delete($id);
            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Rekap BAP',
                'log_action' => 'Hapus',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Hapus Rekap BAP berhasil');
        redirect('admin/bap');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/bap/edit/' . $id);
    }
} 

function printPdf($id = NULL) {
    $this->load->helper(array('dompdf'));
    $this->load->helper(array('tanggal'));
    if ($id == NULL)
        redirect('admin/bap');
    $data['bap'] = $this->Bap_model->get(array('id' => $id));
    $html = $this->load->view('admin/bap/bap_pdf', $data, true);
    $data = pdf_create($html, $data['bap']['bap_karyawan_nama'], TRUE, 'A4', TRUE);
}

// Delete desc
public function delete_desc($id = NULL) {
    if ($this->Bap_model->get_desc(array('id' => $id)) == NULL) {
        redirect('admin/bap/desc');
    }
    if ($_POST) {

        $this->Bap_model->delete_desc($this->input->post('del_id'));
            // activity log
        $this->load->model('Logs_model');
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Keterangan Surat',
                'log_action' => 'Delete',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Delete Keterangan Berhasil');
        redirect('admin/bap/desc');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/bap/desc/edit/' . $id);
    }
}

     // User view in list
public function desc($offset = NULL) {
    $this->load->library('pagination');

    $data['desc'] = $this->Bap_model->get_desc(array('limit' => 10, 'offset' => $offset));
    $data['title'] = 'Daftar Keterangan';
    $data['main'] = 'admin/bap/desc_list';
    $config['base_url'] = site_url('admin/desc');
    $config['total_rows'] = count($this->Bap_model->get_desc());
    $this->pagination->initialize($config);

    $this->load->view('admin/layout', $data);
}


    // Add User_customer and Update
public function add_desc($id = NULL) {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('desc_pilihan', 'Nama', 'trim|required|xss_clean');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button desc="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
    $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

    if ($_POST AND $this->form_validation->run() == TRUE) {

        if ($this->input->post('desc_id')) {
            $params['desc_id'] = $this->input->post('desc_id');
        }
        $params['desc_pilihan'] = $this->input->post('desc_pilihan');
        $status = $this->Bap_model->add_desc($params);

            // activity log
        $this->load->model('Logs_model');
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Keterangan Surat',
                'log_action' => $data['operation'],
                'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('desc_pilihan')
                )
            );

        $this->session->set_flashdata('success', $data['operation'] . ' Keterangan Surat');
        redirect('admin/bap/add');

        if ($this->input->post('from_angular')) {
            echo $status;
        }
    } else {
        if ($this->input->post('desc_id')) {
            redirect('admin/bap/desc/edit/' . $this->input->post('desc_id'));
        }

            // Edit mode
        if (!is_null($id)) {
            $object = $this->Bap_model->get_desc(array('id' => $id));
            if ($object == NULL) {
                redirect('admin/bap/desc');
            } else {
                $data['desc'] = $object;
            }
        }
        $data['title'] = $data['operation'] . ' Keterangan Surat';
        $data['main'] = 'admin/bap/desc_add';
        $this->load->view('admin/layout', $data);
    }
}

function ok($id = NULL) {
    $this->Bap_model->add(array('bap_id' => $id, 'bap_proses' => 'OK'));
    $this->session->set_flashdata('success', 'Proses berhasil');
    redirect('admin/bap');
}
function nok($id = NULL) {
    $this->Bap_model->add(array('bap_id' => $id, 'bap_proses' => 'NOK'));
    $this->session->set_flashdata('success', 'Proses berhasil');
    redirect('admin/bap');
}

public function export_excel()
{
        // Apply Filter
        // Get $_GET variable
    $q = $this->input->get(NULL, TRUE);

    $data['q'] = $q;

    $params = array();

        // Date start
    if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
        $params['date_start'] = $q['ds'];
    }

        // Date end
    if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
        $params['date_end'] = $q['de'];
    }

    $data['bap'] = $this->Bap_model->get($params);

    $this->load->library("PHPExcel");
    $objXLS   = new PHPExcel();
    $objSheet = $objXLS->setActiveSheetIndex(0);            
    $cell     = 2;        
    $no       = 1;

    $objSheet->setCellValue('A1', 'NO');
    $objSheet->setCellValue('B1', 'BRANCH');
    $objSheet->setCellValue('C1', 'TANGGAL KEJADIAN');
    $objSheet->setCellValue('D1', 'TANGGAL BAP');
    $objSheet->setCellValue('E1', 'NIK');
    $objSheet->setCellValue('F1', 'NAMA');
    $objSheet->setCellValue('G1', 'DESKRIPSI');
    $objSheet->setCellValue('H1', 'STATUS');
    $objSheet->setCellValue('I1', 'TANGGAL BUAT');
    $objSheet->setCellValue('J1', 'USER INPUT');


    foreach ($data['bap'] as $row) {

        $objSheet->setCellValue('A'.$cell, $no);
        $objSheet->setCellValue('B'.$cell, $row['branch_nama']);
        $objSheet->setCellValue('C'.$cell, pretty_date($row['bap_tgl_kejadian'], 'd/m/Y', FALSE));
        $objSheet->setCellValue('D'.$cell, pretty_date($row['bap_tgl_surat'], 'd/m/Y', FALSE));
        $objSheet->setCellValueExplicit('E'.$cell, $row['bap_karyawan_nik'],PHPExcel_cell_DataType::TYPE_STRING);
        $objSheet->setCellValue('F'.$cell, $row['bap_karyawan_nama']);
        $objSheet->setCellValue('G'.$cell, $row['desc_pilihan']);
        $objSheet->setCellValue('H'.$cell, $row['bap_proses'] != NULL ? $row['bap_proses'] : 'PENDING');
        $objSheet->setCellValue('I'.$cell, pretty_date($row['bap_input_date'], 'd/m/Y', FALSE));
        $objSheet->setCellValue('J'.$cell, $row['user_full_name']);

        $cell++;
        $no++;    
    }                    

    $objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(10);
    $objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(30);

    foreach(range('D', 'Z') as $alphabet)
    {
        $objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(20);
    }

    $objXLS->getActiveSheet()->getColumnDimension('J')->setWidth(20);

    $font = array('font' => array( 'bold' => true));
    $objXLS->getActiveSheet()
    ->getStyle('A1:J1')
    ->applyFromArray($font);

    $objXLS->setActiveSheetIndex(0);  

    $objSheet->getStyle('A1:J1')->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '09aff4')
                )
            )
        );

    $styleArray = array(
      'borders' => array(
       'allborders' => array(
           'style' => PHPExcel_Style_Border::BORDER_THIN,
           'color' => array(
              'rgb'  => '111111' 
              ),
           ),
       ),
      );
    
    $objSheet->getStyle('A1:J'.$no)->applyFromArray($styleArray);
    $objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
    header('Content-Type: application/vnd.ms-excel'); 
    header('Content-Disposition: attachment;filename="HCO_PS_'.date('dmY').'.xls"'); 
    header('Cache-Control: max-age=0'); 
    $objWriter->save('php://output'); 
    exit();      
}

}



/* End of file bap.php */
/* Location: ./application/controllers/admin/bap.php */
