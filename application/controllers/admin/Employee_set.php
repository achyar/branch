<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * employee controllers class
 *
 * @package     HCA
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Employee_set extends CI_Controller {

    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Employee_model', 'Logs_model'));
        $this->load->helper('string');
    }

    // employee view in list
    public function index($offset = NULL) {
      $this->load->library('pagination');
      // Apply Filter
      // Get $_GET variable
      $f = $this->input->get(NULL, TRUE);

      $data['f'] = $f;

      $params = array();
      // Nip
      if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
          $params['employee_nik'] = $f['n'];
      }

      $paramsPage = $params;
      $params['limit'] = 5;
      $params['offset'] = $offset;
      $data['employee'] = $this->Employee_model->get($params);

      $config['per_page'] = 5;
      $config['uri_segment'] = 4;
      $config['base_url'] = site_url('admin/employee/index');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['total_rows'] = count($this->Employee_model->get($paramsPage));
      $this->pagination->initialize($config);

      $data['title'] = 'Pengguna';
      $data['main'] = 'admin/employee/employee_list';
      $this->load->view('admin/layout', $data);
  }

    function view($id = NULL) {
        if ($this->Employee_model->get(array('id' => $id)) == NULL) {
            redirect('admin/employee');
        }
        $data['employee'] = $this->Employee_model->get(array('id' => $id));
        $data['title'] = 'Detail employee';
        $data['main'] = 'admin/employee/employee_view';
        $this->load->view('admin/layout', $data);
    }

    // Add employee and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('employee_nik', 'NIK', 'trim|required|xss_clean');
        $this->form_validation->set_rules('employee_name', 'name', 'trim|required|xss_clean');
        $this->form_validation->set_rules('employee_place', 'Tempat Lahir', 'trim|required|xss_clean');
        $this->form_validation->set_rules('employee_birth_date', 'Tanggal Lahir', 'trim|required|xss_clean');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('employee_id')) {
                $params['employee_id'] = $this->input->post('employee_id');
            } else {
                $params['employee_nik'] = $this->input->post('employee_nik');
            }

            $var = $this->input->post('employee_entry_date');
            $date = str_replace('/', '-', $var);
            $var2 = $this->input->post('employee_birth_date');
            $birthdate = str_replace('/', '-', $var2);

            $params['employee_name'] = $this->input->post('employee_name');
            $params['employee_account'] = $this->input->post('employee_account');
            $params['employee_place'] = $this->input->post('employee_place');
            $params['employee_birth_date'] = date('Y-m-d', strtotime($birthdate));
            $params['employee_address'] = $this->input->post('employee_address');
            $params['employee_entry_date'] = date('Y-m-d', strtotime($date));
            $params['employee_position'] = $this->input->post('employee_position');
            $params['employee_divisi'] = $this->input->post('employee_divisi');
            $params['employee_dept'] = $this->input->post('employee_dept');
            $params['employee_unit'] = $this->input->post('employee_unit');
            $params['employee_bussiness'] = $this->input->post('employee_bussiness');
            $params['employee_phone'] = $this->input->post('employee_phone');
            $status = $this->Employee_model->add($params);


            // activity log
            $this->Logs_model->add(
                    array(
                        'log_date' => date('Y-m-d H:i:s'),
                        'user_id' => $this->session->userdata('user_id'),
                        'log_module' => 'Karyawan',
                        'log_action' => $data['operation'],
                        'log_info' => 'ID:' . $status . ';Title:' . $params['employee_name']
                    )
            );

            $this->session->set_flashdata('success', $data['operation'] . ' Karyawan berhasil');
            redirect('admin/employee');
        } else {
            if ($this->input->post('employee_id')) {
                redirect('admin/employee/edit/' . $this->input->post('employee_id'));
            }

            // Edit mode
            if (!is_null($id)) {
                $data['employee'] = $this->Employee_model->get(array('id' => $id));
            }
            $data['title'] = $data['operation'] . ' Karyawan';
            $data['main'] = 'admin/employee/employee_add';
            $this->load->view('admin/layout', $data);
        }
    }

    // Delete employee
    public function delete($id = NULL) {
        if ($id == NULL) {

            if ($this->session->userdata('uroleid') != SPECIAL_USER) {
                redirect('admin/employee');
            } else {
                $this->Employee_model->delete_all();
                redirect('admin/employee');
            }
        }

        if ($_POST) {
            $this->Employee_model->delete($id);
            // activity log
            $this->Logs_model->add(
                    array(
                        'log_date' => date('Y-m-d H:i:s'),
                        'user_id' => $this->session->userdata('uid'),
                        'log_module' => 'Karyawan',
                        'log_action' => 'Hapus',
                        'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                    )
            );
            $this->session->set_flashdata('success', 'Hapus Karyawan berhasil');
            redirect('admin/employee');
        } elseif (!$_POST) {
            $this->session->set_flashdata('delete', 'Delete');
            redirect('admin/employee/edit/' . $id);
        }
    }


    public function import() {
      if ($_POST) {
         $rows= explode("\n", $this->input->post('rows'));
         $success = 0;
         $failled = 0;
         $exist = 0;
         $nik = '';
         foreach($rows as $row) {
            $exp = explode("\t", $row);
            if (count($exp) != 13) continue;
            $nik = trim($exp[0]);
            $arr = [
               'employee_nik' => trim($exp[0]),
               'employee_name' => trim($exp[1]),
               'employee_account' => trim($exp[2]),
               'employee_place' => trim($exp[3]),
               'employee_birth_date' => trim($exp[4]),
               'employee_address' => trim($exp[5]),
               'employee_entry_date' => trim($exp[6]),
               'employee_position' => trim($exp[7]),
               'employee_divisi' => trim($exp[8]),
               'employee_dept' => trim($exp[9]),
               'employee_unit' => trim($exp[10]),
               'employee_bussiness' => trim($exp[11]),
               'employee_phone' => trim($exp[12])
            ];

            $check = $this->db
                     ->where('employee_nik', trim($exp[0]))
                     ->count_all_results('employee');
            if ($check == 0) {
               if ($this->db->insert('employee', $arr)) {
                  $success++;
               } else {
                  $failled++;
               }
            } else {
               $exist++;
            }
         }
         $msg = 'Sukses : ' . $success. ' baris, Gagal : '. $failled .', Duplikat : ' . $exist;
         $this->session->set_flashdata('success', $msg);
         redirect('admin/employee/import');
      } else {
         $data['title'] = 'Import Data Karyawan';
         $data['main'] = 'admin/employee/employee_upload';
         $data['action'] = site_url(uri_string());
         $data['employee'] = $this->data['import_employee'] = TRUE;
         $data['alert'] = $this->session->flashdata('alert');
         $data['query'] = FALSE;
         $data['content'] = 'employee/import';
         $this->load->view('admin/layout', $data);
      }
   }

    public function download() {
        $data = file_get_contents("./media/template_excel/Template_Data_employee.xls");
        $name = 'Template_Data_employee.xls';
        $this->load->helper('download');
        force_download($name, $data);
    }

}

/* End of file employee.php */
/* Location: ./application/controllers/admin/employee.php */
