<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Dashboard controllers class
 *
 * @package     HRA Cms
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Present_set extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model('Present_model');
        $this->load->model('Prakerin_model');
        $this->load->model('Setting_model');
    }

    // Dashboard View
    public function index($offset = NULL) {
        $this->load->library('pagination');
        // Apply Filter
        // Get $_GET variable
        $q = $this->input->get(NULL, TRUE);

        $data['q'] = $q;

        $params = array();
        // Nip
        if (isset($q['n']) && !empty($q['n']) && $q['n'] != '') {
            $params['prakerin_nip'] = $q['n'];
        }

        // Date start
        if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
            $params['date_start'] = $q['ds'];
        }

        // Date end
        if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
            $params['date_end'] = $q['de'];
        }
        
        $data['present'] = $this->Present_model->get($params);
        $config['base_url'] = site_url('admin/prakerin/index');
        $config['total_rows'] = count($this->Present_model->get(array('status' => TRUE)));
        $this->pagination->initialize($config);

        $data['title'] = 'Presensi';
        $data['main'] = 'admin/present/present_list';
        $this->load->view('admin/layout', $data);
    }


    public function export_excel()
    {
        // Apply Filter
        // Get $_GET variable
        $q = $this->input->get(NULL, TRUE);

        $data['q'] = $q;

        $params = array();
        
        // Nip
        if (isset($q['n']) && !empty($q['n']) && $q['n'] != '') {
            $params['prakerin_nip'] = $q['n'];
        }

        // Date start
        if (isset($q['ds']) && !empty($q['ds']) && $q['ds'] != '') {
            $params['date_start'] = $q['ds'];
        }

        // Date end
        if (isset($q['de']) && !empty($q['de']) && $q['de'] != '') {
            $params['date_end'] = $q['de'];
        }
        

        $data['present'] = $this->Present_model->get($params);


        $this->load->library("PHPExcel");
        $objXLS   = new PHPExcel();
        $objSheet = $objXLS->setActiveSheetIndex(0);            
        $cell     = 2;        
        $no       = 1;
        $branch = $this->Setting_model->get(array('id' => BRANCH));
        $kode = $this->Setting_model->get(array('id' => KODE));
        $unit = $this->Setting_model->get(array('id' => UNIT));

        $objSheet->setCellValue('A1', 'NO');
        $objSheet->setCellValue('B1', 'BRANCH');
        $objSheet->setCellValue('C1', 'KODE TOKO');
        $objSheet->setCellValue('D1', 'NAMA TOKO');
        $objSheet->setCellValue('E1', 'NAMA');
        $objSheet->setCellValue('F1', 'TINGKAT SMA/SMK,D3,S1');
        $objSheet->setCellValue('G1', 'BANK');
        $objSheet->setCellValue('H1', 'NO REKENING');               
        $objSheet->setCellValue('I1', 'LAMA KONTRAK');               
        $objSheet->setCellValue('J1', 'TANGGAL MASUK');                
        $objSheet->setCellValue('K1', 'TANGGAL PRESENSI');  
        $objSheet->setCellValue('L1', 'JAM MASUK');  
        $objSheet->setCellValue('M1', 'JAM KELUAR');  
        $objSheet->setCellValue('N1', 'JUMLAH PRESENSI');  
        $objSheet->setCellValue('O1', 'TANGGAL OUT');  


        foreach ($data['present'] as $row) {

            $jmljam = $row['present_out_time'] - $row['present_entry_time'];

            $objSheet->setCellValue('A'.$cell, $no);
            $objSheet->setCellValue('B'.$cell, $branch['setting_value']);
            $objSheet->setCellValue('C'.$cell, $kode['setting_value']);
            $objSheet->setCellValue('D'.$cell, $unit['setting_value']);
            $objSheet->setCellValue('E'.$cell, $row['prakerin_name']); 
            $objSheet->setCellValue('F'.$cell, $row['prakerin_education']); 
            $objSheet->setCellValue('G'.$cell, '-'); 
            $objSheet->setCellValue('H'.$cell, '-'); 
            $objSheet->setCellValue('I'.$cell, '-'); 
            $objSheet->setCellValue('J'.$cell, pretty_date($row['prakerin_entry_date'], 'd/m/Y', FALSE)); 
            $objSheet->setCellValue('K'.$cell, pretty_date($row['present_date'], 'd/m/Y', FALSE));
            $objSheet->setCellValue('L'.$cell, $row['present_entry_time']); 
            $objSheet->setCellValue('M'.$cell, $row['present_out_time']); 
            $objSheet->setCellValue('N'.$cell, $jmljam);
            $objSheet->setCellValue('O'.$cell, ($row['prakerin_out_date'] < 1) ? '-' : pretty_date($row['prakerin_out_date'], 'd/m/Y', FALSE));

            

            $cell++;
            $no++;    
        }                    
        
        $objXLS->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $objXLS->getActiveSheet()->getColumnDimension('B')->setWidth(11);
        $objXLS->getActiveSheet()->getColumnDimension('C')->setWidth(11);
        $objXLS->getActiveSheet()->getColumnDimension('E')->setWidth(30);

        foreach(range('D', 'Z') as $alphabet)
        {
            $objXLS->getActiveSheet()->getColumnDimension($alphabet)->setWidth(20);
        }

        $objXLS->getActiveSheet()->getColumnDimension('O')->setWidth(20);

        $font = array('font' => array( 'bold' => true));
        $objXLS->getActiveSheet()
        ->getStyle('A1:O1')
        ->applyFromArray($font);

        $objXLS->setActiveSheetIndex(0);        
        $styleArray = array(
          'borders' => array(
           'allborders' => array(
               'style' => PHPExcel_Style_Border::BORDER_THIN,
               'color' => array(
                  'rgb'  => '111111' 
                  ),
               ),
           ),
          );
        $objXLS->getActiveSheet()
        ->getStyle('A1:O1')
        ->getFill()
        ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setRGB('00bbff');
        $objSheet->getStyle('A1:O'.$no)->applyFromArray($styleArray);
        $objWriter = PHPExcel_IOFactory::createWriter($objXLS, 'Excel5'); 
        header('Content-Type: application/vnd.ms-excel'); 
        header('Content-Disposition: attachment;filename="HRD_PRKRN_'.date('dmY').'.xls"'); 
        header('Cache-Control: max-age=0'); 
        $objWriter->save('php://output'); 
        exit();      
    }

    function printPdf($id = NULL) {
        $this->load->helper(array('dompdf'));
        $this->load->helper(array('tanggal'));
        if ($id == NULL)
            redirect('admin/present');
        $data['present'] = $this->Present_model->get(array('id' => $id));
        $data['setting_employe_nik'] = $this->Setting_model->get(array('id' => NIK_PDM));
        $data['setting_employe_name'] = $this->Setting_model->get(array('id' => NAMA_PDM));
        $data['setting_employe_position'] = $this->Setting_model->get(array('id' => POS_PDM)); 
        $data['kota'] = $this->Setting_model->get(array('id' => KOTA)); 
        $data['alamat'] = $this->Setting_model->get(array('id' => ALAMAT)); 
        $data['tlp'] = $this->Setting_model->get(array('id' => TLP)); 
        $data['unit'] = $this->Setting_model->get(array('id' => UNIT));
        $data['branch'] = $this->Setting_model->get(array('id' => BRANCH));
        $html = $this->load->view('admin/present/present_pdf', $data, true);
        $data = pdf_create($html, $data['present']['prakerin_name'], TRUE, 'A4', TRUE);
    }



}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
