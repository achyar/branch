<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * disnaker controllers class 
 *
 * @package     HRA CMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Disnaker_set extends CI_Controller {

    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Disnaker_model', 'Logs_model', 'Setting_model'));
        $this->load->helper('string');
    }

    // Surat Keterangan Disnaker view in list
    public function index($offset = NULL) {
      $this->load->library('pagination');
      // Apply Filter
      // Get $_GET variable
      $f = $this->input->get(NULL, TRUE);

      $data['f'] = $f;

      $params = array();
      // Nip
      if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
          $params['disnaker_employee_name'] = $f['n'];
      }

      $paramsPage = $params;
      $params['limit'] = 5;
      $params['offset'] = $offset;
      $data['disnaker'] = $this->Disnaker_model->get($params);
 
      $config['per_page'] = 5;
      $config['uri_segment'] = 4;
      $config['base_url'] = site_url('admin/disnaker/index');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['total_rows'] = count($this->Disnaker_model->get($paramsPage));
      $this->pagination->initialize($config);

      $data['title'] = 'Surat Keterangan Disnaker';
      $data['main'] = 'admin/disnaker/disnaker_list';
      $this->load->view('admin/layout', $data);
    }

    function view($id = NULL) {
        if ($this->Disnaker_model->get(array('id' => $id)) == NULL) {
            redirect('admin/disnaker');
        }
        $data['disnaker'] = $this->Disnaker_model->get(array('id' => $id));               
        $data['title'] = 'Surat Keterangan Disnaker';
        $data['main'] = 'admin/disnaker/disnaker_view';
        $this->load->view('admin/layout', $data);
    }

    // Add disnaker and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('disnaker_number', 'No Surat', 'trim|required|xss_clean');         
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Update';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('disnaker_id')) {
                $params['disnaker_id'] = $this->input->post('disnaker_id');
            } else {             
             $params['disnaker_input_date'] = date('Y-m-d H:i:s');

         }

         // $var = $this->input->post('disnaker_employee_entry_date');
         // $date = str_replace('/', '-', $var);
         // $var2 = $this->input->post('disnaker_employee_out_date');
         // $date2 = str_replace('/', '-', $var2);

         $params['disnaker_number'] = $this->input->post('disnaker_number');
         $params['disnaker_employee_nik'] = $this->input->post('disnaker_employee_nik');
         $params['disnaker_employee_name'] = $this->input->post('disnaker_employee_name');            
         $params['disnaker_employee_entry_date'] = $this->input->post('disnaker_employee_entry_date');
         $params['disnaker_employee_out_date'] = $this->input->post('disnaker_employee_out_date');
         $params['disnaker_employee_address'] = $this->input->post('disnaker_employee_address');
         $params['user_id'] = $this->session->userdata('uid');
         $params['disnaker_last_update'] = date('Y-m-d H:i:s');
         $status = $this->Disnaker_model->add($params);


            // activity log
         $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Surat Keterangan Disnaker',
                'log_action' => $data['operation'],
                'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('disnaker_employee_name') 
                )
            );

         $this->session->set_flashdata('success', $data['operation'] . ' Surat Keterangan Disnaker berhasil');
         redirect('admin/disnaker');
     } else {
        if ($this->input->post('disnaker_id')) {
            redirect('admin/disnaker/edit/' . $this->input->post('disnaker_id'));
        }

            // Edit mode
        if (!is_null($id)) {
            $data['disnaker'] = $this->Disnaker_model->get(array('id' => $id));
        }

        $data['title'] = $data['operation'] . ' Surat Keterangan Disnaker';
        $data['main'] = 'admin/disnaker/disnaker_add';
        $this->load->view('admin/layout', $data);
    } 
}

    // Delete Surat Keterangan Disnaker
public function delete($id = NULL) {
    if ($_POST) {
        $this->Disnaker_model->delete($id);
            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Surat Keterangan Disnaker',
                'log_action' => 'Hapus',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Hapus Surat Keterangan Disnaker berhasil');
        redirect('admin/disnaker');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/disnaker/edit/' . $id);
    }
} 

function printPdf($id = NULL) {
    $this->load->helper(array('dompdf'));
    $this->load->helper(array('tanggal'));
    if ($id == NULL)
        redirect('admin/disnaker');

    $data['disnaker'] = $this->Disnaker_model->get(array('id' => $id));
    $data['setting_employe_nik'] = $this->Setting_model->get(array('id' => NIK_PDM));
    $data['setting_employe_name'] = $this->Setting_model->get(array('id' => NAMA_PDM));
    $data['setting_employe_position'] = $this->Setting_model->get(array('id' => POS_PDM)); 
    $data['kota'] = $this->Setting_model->get(array('id' => KOTA)); 
    $html = $this->load->view('admin/disnaker/disnaker_pdf', $data, true);
    $data = pdf_create($html, $data['disnaker']['disnaker_employee_name'], TRUE, 'A4', TRUE);
}

}

/* End of file disnaker.php */
/* Location: ./application/controllers/admin/disnaker.php */
