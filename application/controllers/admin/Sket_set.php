<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * sket controllers class 
 *
 * @package     HRA CMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Sket_set extends CI_Controller {

    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Sket_model', 'Logs_model', 'Employee_model', 'Letter_model', 'Setting_model'));
        $this->load->helper('string');
    }

    // Surat Keterangan view in list
    public function index($offset = NULL) {
      $this->load->library('pagination');
      // Apply Filter
      // Get $_GET variable
      $f = $this->input->get(NULL, TRUE);

      $data['f'] = $f;

      $params = array();
      // Nip
      if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
          $params['sket_employee_name'] = $f['n'];
      }

      $paramsPage = $params;
      $params['limit'] = 5;
      $params['offset'] = $offset;
      $data['sket'] = $this->Sket_model->get($params);
 
      $config['per_page'] = 5;
      $config['uri_segment'] = 4;
      $config['base_url'] = site_url('admin/sket/index');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['total_rows'] = count($this->Sket_model->get($paramsPage));
      $this->pagination->initialize($config);

      $data['title'] = 'Surat Keterangan Kerja';
      $data['main'] = 'admin/sket/sket_list';
      $this->load->view('admin/layout', $data);
    }

    function view($id = NULL) {
        if ($this->Sket_model->get(array('id' => $id)) == NULL) {
            redirect('admin/sket');
        }
        $data['sket'] = $this->Sket_model->get(array('id' => $id));               
        $data['title'] = 'Surat Keterangan Kerja';
        $data['main'] = 'admin/sket/sket_view';
        $this->load->view('admin/layout', $data);
    }

    // Add sket and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('ket_id', 'Keterangan Surat', 'trim|required|xss_clean');         
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Update';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('sket_id')) {
                $params['sket_id'] = $this->input->post('sket_id');
            } else {

             $lastletter = $this->Letter_model->get(array('limit' => 1));
             $this->load->helper('tanggal');
             $namaBulan = konversiBulan(date('m'));
             $unit = $this->Setting_model->get(array('id' => UNIT));

             if ($lastletter['letter_year'] < date('Y') OR count($lastletter) == 0) {
                $this->Letter_model->add(array('letter_number' => '001', 'letter_month' => date('m'), 'letter_year' => date('Y')));
                $nomor = sprintf('%03d', '001');
                $nofull = $nomor . '/HRA-Ext/' . $unit['setting_value'] .'/'. $namaBulan . '/'. date('Y');
            } else {
                $nomor = sprintf('%03d', $lastletter['letter_number'] + 001);
                $this->Letter_model->add(array('letter_number' => $nomor, 'letter_month' => date('m'), 'letter_year' => date('Y')));
                $nofull = $nomor . '/HRA-Ext/' . $unit['setting_value'] .'/'. $namaBulan . '/'. date('Y');
            }

            $params['sket_input_date'] = date('Y-m-d H:i:s');

        }
        $params['sket_number'] = $nofull;
        $params['sket_employee_nik'] = $this->input->post('employee_nik');
        $params['sket_employee_name'] = $this->input->post('employee_name');            
        $params['sket_employee_position'] = $this->input->post('employee_position');
        $params['sket_employee_place'] = $this->input->post('employee_place');
        $params['sket_employee_birth_date'] = $this->input->post('employee_birth_date');
        $params['sket_employee_address'] = $this->input->post('employee_address');
        $params['sket_employee_entry_date'] = $this->input->post('employee_entry_date');
        $params['sket_status'] = $this->input->post('sket_status');
        $params['ket_ket_id'] = $this->input->post('ket_id');
        $params['user_id'] = $this->session->userdata('uid');
        $params['sket_last_update'] = date('Y-m-d H:i:s');
        $status = $this->Sket_model->add($params);


            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Surat Keterangan',
                'log_action' => $data['operation'],
                'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('sket_employee_name') 
                )
            );

        $this->session->set_flashdata('success', $data['operation'] . ' Surat Keterangan berhasil');
        redirect('admin/sket');
    } else {
        if ($this->input->post('sket_id')) {
            redirect('admin/sket/edit/' . $this->input->post('sket_id'));
        }

            // Edit mode
        if (!is_null($id)) {
            $data['sket'] = $this->Sket_model->get(array('id' => $id));
        }
        $data['ngapp'] = 'ng-app="comApp"';
        $data['ket'] = $this->Sket_model->get_ket();
        $data['employee'] = $this->Employee_model->get();
        $data['title'] = $data['operation'] . ' Surat Keterangan';
        $data['main'] = 'admin/sket/sket_add';
        $this->load->view('admin/layout', $data);
    } 
}

    // Delete Surat Keterangan
public function delete($id = NULL) {
    if ($_POST) {
        $this->Sket_model->delete($id);
            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Surat Keterangan',
                'log_action' => 'Hapus',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Hapus Surat Keterangan berhasil');
        redirect('admin/sket');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/sket/edit/' . $id);
    }
} 

function printPdf($id = NULL) {
    $this->load->helper(array('dompdf'));
    $this->load->helper(array('tanggal'));
    if ($id == NULL)
        redirect('admin/sket');
    $data['sket'] = $this->Sket_model->get(array('id' => $id));
    $data['setting_employe_nik'] = $this->Setting_model->get(array('id' => NIK_PDM));
    $data['setting_employe_name'] = $this->Setting_model->get(array('id' => NAMA_PDM));
    $data['setting_employe_position'] = $this->Setting_model->get(array('id' => POS_PDM)); 
    $data['kota'] = $this->Setting_model->get(array('id' => KOTA)); 
    $data['alamat'] = $this->Setting_model->get(array('id' => ALAMAT)); 
    $data['tlp'] = $this->Setting_model->get(array('id' => TLP)); 
    $data['unit'] = $this->Setting_model->get(array('id' => UNIT));
    $data['branch'] = $this->Setting_model->get(array('id' => BRANCH));
    $html = $this->load->view('admin/sket/sket_pdf', $data, true);
    $data = pdf_create($html, $data['sket']['sket_employee_name'], TRUE, 'A4', TRUE);
}

// Delete Ket
public function delete_ket($id = NULL) {
    if ($this->Sket_model->get_ket(array('id' => $id)) == NULL) {
        redirect('admin/sket/ket');
    }
    if ($_POST) {

        $this->Sket_model->delete_ket($this->input->post('del_id'));
            // activity log
        $this->load->model('Logs_model');
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Keterangan Surat',
                'log_action' => 'Delete',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Delete Keterangan Berhasil');
        redirect('admin/sket/ket');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/sket/ket/edit/' . $id);
    }
}

     // User view in list
public function ket($offset = NULL) {
    $this->load->library('pagination');

    $data['ket'] = $this->Sket_model->get_ket(array('limit' => 10, 'offset' => $offset));
    $data['title'] = 'Daftar Keterangan';
    $data['main'] = 'admin/sket/ket_list';
    $config['base_url'] = site_url('admin/ket');
    $config['total_rows'] = count($this->Sket_model->get_ket());
    $this->pagination->initialize($config);

    $this->load->view('admin/layout', $data);
}


    // Add User_customer and Update
public function add_ket($id = NULL) {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('ket_surat', 'Name', 'trim|required|xss_clean');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button ket="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
    $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

    if ($_POST AND $this->form_validation->run() == TRUE) {

        if ($this->input->post('ket_id')) {
            $params['ket_id'] = $this->input->post('ket_id');
        }
        $params['ket_surat'] = $this->input->post('ket_surat');
        $status = $this->Sket_model->add_ket($params);

            // activity log
        $this->load->model('Logs_model');
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Keterangan Surat',
                'log_action' => $data['operation'],
                'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('ket_surat')
                )
            );

        $this->session->set_flashdata('success', $data['operation'] . ' Keterangan Surat');
        redirect('admin/sket/add');

        if ($this->input->post('from_angular')) {
            echo $status;
        }
    } else {
        if ($this->input->post('ket_id')) {
            redirect('admin/sket/ket/edit/' . $this->input->post('ket_id'));
        }

            // Edit mode
        if (!is_null($id)) {
            $object = $this->Sket_model->get_ket(array('id' => $id));
            if ($object == NULL) {
                redirect('admin/sket/ket');
            } else {
                $data['ket'] = $object;
            }
        }
        $data['title'] = $data['operation'] . ' Keterangan Surat';
        $data['main'] = 'admin/sket/ket_add';
        $this->load->view('admin/layout', $data);
    }
}

}



/* End of file sket.php */
/* Location: ./application/controllers/admin/sket.php */
