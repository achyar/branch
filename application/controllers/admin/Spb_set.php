<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * spb controllers class 
 *
 * @package     HRA CMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class Spb_set extends CI_Controller {

    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('Spb_model', 'Logs_model', 'Setting_model', 'Letter_model'));
        $this->load->helper('string');
    }

    // view in list
    public function index($offset = NULL) {
      $this->load->library('pagination');
      // Apply Filter
      // Get $_GET variable
      $f = $this->input->get(NULL, TRUE);

      $data['f'] = $f;

      $params = array();
      // Nip
      if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
          $params['spb_date'] = $f['n'];
      }

      $paramsPage = $params;
      $params['limit'] = 5;
      $params['offset'] = $offset;
      $data['spb'] = $this->Spb_model->get($params);
 
      $config['per_page'] = 5;
      $config['uri_segment'] = 4;
      $config['base_url'] = site_url('admin/spb/index');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['total_rows'] = count($this->Spb_model->get($paramsPage));
      $this->pagination->initialize($config);

      $data['title'] = 'Surat Pengantar Bank';
      $data['main'] = 'admin/spb/spb_list';
      $this->load->view('admin/layout', $data);
  }

    function view($id = NULL) {
        if ($this->Spb_model->get(array('id' => $id)) == NULL) {
            redirect('admin/spb');
        }
        $data['spb'] = $this->Spb_model->get(array('id' => $id));               
        $data['title'] = 'Surat Pengantar Bank';
        $data['main'] = 'admin/spb/spb_view';
        $this->load->view('admin/layout', $data);
    }

    // Add spb and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('spb_name1', 'Nama', 'trim|required|xss_clean');  
        $this->form_validation->set_rules('spb_nik1', 'NIK', 'trim|required|xss_clean');       
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Update';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('spb_id')) {
                $params['spb_id'] = $this->input->post('spb_id');
            } else {

              $lastletter = $this->Letter_model->get(array('limit' => 1));
              $this->load->helper('tanggal');
              $namaBulan = konversiBulan(date('m'));
              $unit = $this->Setting_model->get(array('id' => UNIT));

              if ($lastletter['letter_year'] < date('Y') OR count($lastletter) == 0) {
                $this->Letter_model->add(array('letter_number' => '001', 'letter_month' => date('m'), 'letter_year' => date('Y')));
                $nomor = sprintf('%03d', '001');
                $nofull = $nomor . '/HRA-Ext/' . $unit['setting_value'] .'/'. $namaBulan . '/'. date('Y');
            } else {
                $nomor = sprintf('%03d', $lastletter['letter_number'] + 001);
                $this->Letter_model->add(array('letter_number' => $nomor, 'letter_month' => date('m'), 'letter_year' => date('Y')));
                $nofull = $nomor . '/HRA-Ext/' . $unit['setting_value'] .'/'. $namaBulan . '/'. date('Y');
            }

            $params['spb_input_date'] = date('Y-m-d H:i:s');

        }

        $var = $this->input->post('spb_date');
        $date = str_replace('/', '-', $var);

        $params['spb_number'] = $nofull;
        $params['spb_date'] = date('Y-m-d', strtotime($date));
        $params['spb_name1'] = $this->input->post('spb_name1');
        $params['spb_name2'] = $this->input->post('spb_name2');
        $params['spb_name3'] = $this->input->post('spb_name3');
        $params['spb_name4'] = $this->input->post('spb_name4');
        $params['spb_name5'] = $this->input->post('spb_name5');
        $params['spb_name6'] = $this->input->post('spb_name6');
        $params['spb_name7'] = $this->input->post('spb_name7');
        $params['spb_name8'] = $this->input->post('spb_name8');
        $params['spb_name9'] = $this->input->post('spb_name9');
        $params['spb_name10'] = $this->input->post('spb_name10');
        $params['spb_nik1'] = $this->input->post('spb_nik1');
        $params['spb_nik2'] = $this->input->post('spb_nik2');
        $params['spb_nik3'] = $this->input->post('spb_nik3');
        $params['spb_nik4'] = $this->input->post('spb_nik4');
        $params['spb_nik5'] = $this->input->post('spb_nik5');
        $params['spb_nik6'] = $this->input->post('spb_nik6');
        $params['spb_nik7'] = $this->input->post('spb_nik7');
        $params['spb_nik8'] = $this->input->post('spb_nik8');
        $params['spb_nik9'] = $this->input->post('spb_nik9');
        $params['spb_nik10'] = $this->input->post('spb_nik10');
        $params['bank_bank_id'] = $this->input->post('bank_id');
        $params['user_id'] = $this->session->userdata('uid');
        $params['spb_last_update'] = date('Y-m-d H:i:s');
        $status = $this->Spb_model->add($params);


            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Surat Pengantar Bank',
                'log_action' => $data['operation'],
                'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('spb_number') 
                )
            );

        $this->session->set_flashdata('success', $data['operation'] . ' Surat Pengantar Bank berhasil');
        redirect('admin/spb');
    } else {
        if ($this->input->post('spb_id')) {
            redirect('admin/spb/edit/' . $this->input->post('spb_id'));
        }

            // Edit mode
        if (!is_null($id)) {
            $data['spb'] = $this->Spb_model->get(array('id' => $id));
        }
        $data['ngapp'] = 'ng-app="comApp"';
        $data['bank'] = $this->Spb_model->get_bank();
        $data['title'] = $data['operation'] . ' Surat Pengantar Bank';
        $data['main'] = 'admin/spb/spb_add';
        $this->load->view('admin/layout', $data);
    } 
}

    // Delete Surat Pengantar Bank
public function delete($id = NULL) {
    if ($_POST) {
        $this->Spb_model->delete($id);
            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Surat Pengantar Bank',
                'log_action' => 'Hapus',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Hapus Surat Pengantar Bank berhasil');
        redirect('admin/spb');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/spb/edit/' . $id); 
    }
} 

function printPdf($id = NULL) {
    $this->load->helper(array('dompdf'));
    $this->load->helper(array('tanggal'));
    if ($id == NULL)
        redirect('admin/spb');
    $data['spb'] = $this->Spb_model->get(array('id' => $id));
    $data['setting_employe_nik'] = $this->Setting_model->get(array('id' => NIK_PDM));
    $data['setting_employe_name'] = $this->Setting_model->get(array('id' => NAMA_PDM));
    $data['setting_employe_position'] = $this->Setting_model->get(array('id' => POS_PDM)); 
    $data['kota'] = $this->Setting_model->get(array('id' => KOTA)); 
    $data['alamat'] = $this->Setting_model->get(array('id' => ALAMAT)); 
    $data['tlp'] = $this->Setting_model->get(array('id' => TLP)); 
    $data['unit'] = $this->Setting_model->get(array('id' => UNIT));
    $data['branch'] = $this->Setting_model->get(array('id' => BRANCH));
    $html = $this->load->view('admin/spb/spb_pdf', $data, true);
    $data = pdf_create($html, $data['spb']['spb_input_date'], TRUE, 'A4', TRUE);
}

// Delete bank
public function delete_bank($id = NULL) {
    if ($this->Spb_model->get_bank(array('id' => $id)) == NULL) {
        redirect('admin/spb/bank');
    }
    if ($_POST) {

        $this->Spb_model->delete_bank($this->input->post('del_id'));
            // activity log
        $this->load->model('Logs_model');
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Bank Surat',
                'log_action' => 'Delete',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Delete Bank Berhasil');
        redirect('admin/spb/bank');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/spb/bank/edit/' . $id);
    }
}

     // User view in list
public function bank($offset = NULL) {
    $this->load->library('pagination');

    $data['bank'] = $this->Spb_model->get_bank(array('limit' => 10, 'offset' => $offset));
    $data['title'] = 'Daftar Bank';
    $data['main'] = 'admin/spb/bank_list';
    $config['base_url'] = site_url('admin/bank');
    $config['total_rows'] = count($this->Spb_model->get_bank());
    $this->pagination->initialize($config);

    $this->load->view('admin/layout', $data);
}


    // Add User_customer and Update
public function add_bank($id = NULL) {
    $this->load->library('form_validation');
    $this->form_validation->set_rules('bank_name', 'Name', 'trim|required|xss_clean');
    $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button bank="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
    $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

    if ($_POST AND $this->form_validation->run() == TRUE) {

        if ($this->input->post('bank_id')) {
            $params['bank_id'] = $this->input->post('bank_id');
        }
        $params['bank_name'] = $this->input->post('bank_name');
        $status = $this->Spb_model->add_bank($params);

            // activity log
        $this->load->model('Logs_model');
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'Bank Surat',
                'log_action' => $data['operation'],
                'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('bank_name')
                )
            );

        $this->session->set_flashdata('success', $data['operation'] . ' Bank Surat');
        redirect('admin/spb/add');

        if ($this->input->post('from_angular')) {
            echo $status;
        }
    } else {
        if ($this->input->post('bank_id')) {
            redirect('admin/spb/bank/edit/' . $this->input->post('bank_id'));
        }

            // Edit mode
        if (!is_null($id)) {
            $object = $this->Spb_model->get_bank(array('id' => $id));
            if ($object == NULL) {
                redirect('admin/spb/bank');
            } else {
                $data['bank'] = $object;
            }
        }
        $data['title'] = $data['operation'] . ' Bank Surat';
        $data['main'] = 'admin/spb/bank_add';
        $this->load->view('admin/layout', $data);
    }
}

}



/* End of file spb.php */
/* Location: ./application/controllers/admin/spb.php */
