<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * no_manual controllers class 
 *
 * @package     HRA CMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */
class No_manual_set extends CI_Controller {

    public function __construct() {
        parent::__construct(TRUE);
        if ($this->session->userdata('logged') == NULL) {
            header("Location:" . site_url('admin/auth/login') . "?location=" . urlencode($_SERVER['REQUEST_URI']));
        }
        $this->load->model(array('No_manual_model', 'Logs_model', 'Letter_model', 'Setting_model'));
        $this->load->helper('string');
    }

   // bpjstk view in list
    public function index($offset = NULL) {
      $this->load->library('pagination');
      // Apply Filter
      // Get $_GET variable
      $f = $this->input->get(NULL, TRUE);

      $data['f'] = $f;

      $params = array();
      // Nip
      if (isset($f['n']) && !empty($f['n']) && $f['n'] != '') {
          $params[''] = $f['n'];
      }

      $paramsPage = $params;
      $params['limit'] = 5;
      $params['offset'] = $offset;
      $data['no_manual'] = $this->No_manual_model->get($params);
 
      $config['per_page'] = 5;
      $config['uri_segment'] = 4;
      $config['base_url'] = site_url('admin/no_manual/index');
      $config['suffix'] = '?' . http_build_query($_GET, '', "&");
      $config['total_rows'] = count($this->No_manual_model->get($paramsPage));
      $this->pagination->initialize($config);

      $data['title'] = 'No Surat Manual';
      $data['main'] = 'admin/no_manual/no_manual_list';
      $this->load->view('admin/layout', $data);
  }

    function view($id = NULL) {
        if ($this->No_manual_model->get(array('id' => $id)) == NULL) {
            redirect('admin/no_manual');
        }
        $data['no_manual'] = $this->No_manual_model->get(array('id' => $id));               
        $data['title'] = 'No Surat Manual';
        $data['main'] = 'admin/no_manual/no_manual_view';
        $this->load->view('admin/layout', $data);
    }

    // Add no_manual and Update
    public function add($id = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('no_manual_desc', 'Keterangan', 'trim|required|xss_clean');         
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>');
        $data['operation'] = is_null($id) ? 'Tambah' : 'Update';

        if ($_POST AND $this->form_validation->run() == TRUE) {

            if ($this->input->post('no_manual_id')) {
                $params['no_manual_id'] = $this->input->post('no_manual_id');
            } else {

               $lastletter = $this->Letter_model->get(array('limit' => 1));
               $this->load->helper('tanggal');
               $namaBulan = konversiBulan(date('m'));
               $unit = $this->Setting_model->get(array('id' => UNIT));

               if ($lastletter['letter_year'] < date('Y') OR count($lastletter) == 0) {
                $this->Letter_model->add(array('letter_number' => '001', 'letter_month' => date('m'), 'letter_year' => date('Y')));
                $nomor = sprintf('%03d', '001');
                $nofull = $nomor . '/HRA-Ext/' . $unit['setting_value'] .'/'. $namaBulan . '/'. date('Y');
            } else {
                $nomor = sprintf('%03d', $lastletter['letter_number'] + 001);
                $this->Letter_model->add(array('letter_number' => $nomor, 'letter_month' => date('m'), 'letter_year' => date('Y')));
                $nofull = $nomor . '/HRA-Ext/' . $unit['setting_value'] .'/'. $namaBulan . '/'. date('Y');
            }

            $params['no_manual_input_date'] = date('Y-m-d H:i:s');

        }
        $params['no_manual_letter'] = $nofull;
        $params['no_manual_desc'] = $this->input->post('no_manual_desc');
        $params['user_id'] = $this->session->userdata('uid');
        $params['no_manual_last_update'] = date('Y-m-d H:i:s');
        $status = $this->No_manual_model->add($params);


            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'No Surat Manual',
                'log_action' => $data['operation'],
                'log_info' => 'ID:' . $status . ';Title:' . $this->input->post('no_manual_desc') 
                )
            );

        $this->session->set_flashdata('success', $data['operation'] . ' No Surat Manual berhasil');
        redirect('admin/no_manual');
    } else {
        if ($this->input->post('no_manual_id')) {
            redirect('admin/no_manual/edit/' . $this->input->post('no_manual_id'));
        }

            // Edit mode
        if (!is_null($id)) {
            $data['no_manual'] = $this->No_manual_model->get(array('id' => $id));
        }
        $data['title'] = $data['operation'] . ' No Surat Manual';
        $data['main'] = 'admin/no_manual/no_manual_add';
        $this->load->view('admin/layout', $data);
    } 
}

    // Delete No Surat Manual
public function delete($id = NULL) {
    if ($_POST) {
        $this->No_manual_model->delete($id);
            // activity log
        $this->Logs_model->add(
            array(
                'log_date' => date('Y-m-d H:i:s'),
                'user_id' => $this->session->userdata('uid'),
                'log_module' => 'No Surat Manual',
                'log_action' => 'Hapus',
                'log_info' => 'ID:' . $this->input->post('del_id') . ';Title:' . $this->input->post('del_name')
                )
            );
        $this->session->set_flashdata('success', 'Hapus No Surat Manual berhasil');
        redirect('admin/no_manual');
    } elseif (!$_POST) {
        $this->session->set_flashdata('delete', 'Delete');
        redirect('admin/no_manual/edit/' . $id);
    }
} 


}



/* End of file no_manual.php */
/* Location: ./application/controllers/admin/no_manual.php */
