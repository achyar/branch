<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['admin/([a-zA-Z_-]+)'] = 'admin/$1_set';
$route['admin/auth'] = 'admin/auth_set/login';
$route['admin/auth/(:any)'] = 'admin/auth_set/$1';
$route['admin/([a-zA-Z_-]+)/(:any)'] = 'admin/$1_set/$2';
$route['admin/(:any)/edit/(:num)'] = "admin/$1_set/add/$2";
$route['admin/(:any)/(:any)/edit/(:num)'] = "admin/$1_set/add_$2/$3";
$route['admin/(:any)/(:any)/(:num)/(:num)'] = "admin/$1_set/$2/$3/$4";
$route['admin/(:any)/(:any)/(:num)'] = "admin/$1_set/$2/$3";
$route['admin/(:any)/(:any)/(:any)'] = "admin/$1_set/$3_$2";
$route['admin'] = "admin/dashboard_set";

$route['prakerin/([a-zA-Z_-]+)'] = 'prakerin/$1_set';
$route['prakerin/auth'] = 'prakerin/auth_set/login';
$route['prakerin/auth/(:any)'] = 'prakerin/auth_set/$1';
$route['prakerin/([a-zA-Z_-]+)/(:any)'] = 'prakerin/$1_set/$2';
$route['prakerin/(:any)/edit/(:num)'] = "prakerin/$1_set/add/$2";
$route['prakerin/(:any)/(:any)/edit/(:num)'] = "prakerin/$1_set/add_$2/$3";
$route['prakerin/(:any)/(:any)/(:num)/(:num)'] = "prakerin/$1_set/$2/$3/$4";
$route['prakerin/(:any)/(:any)/(:num)'] = "prakerin/$1_set/$2/$3";
$route['prakerin/(:any)/(:any)/(:any)'] = "prakerin/$1_set/$3_$2";
$route['prakerin'] = "prakerin/dashboard_set";


$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
