<script src="<?php echo base_url('/media/js/jquery.inputmask.bundle.js'); ?>"></script>
<script>
    $(function separator() {
        $('.numeric').inputmask("numeric", {
            removeMaskOnSubmit: true,
            radixPoint: ".",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            prefix: '', //Space after $, this will not truncate the first character.
            rightAlign: false,
            oncleared: function() {
                self.Value('');
            }
        });
    });
</script>