<?php

if (isset($user)) {
	$id = $user['user_id'];
	$NameValue = $user['user_nik'];
	$FullNameValue = $user['user_full_name'];
	$RoleValue = $user['user_role_role_id'];
	$ImageValue = $user['user_image'];
} else {
	$NameValue = set_value('user_nik');
	$FullNameValue = set_value('user_full_name');
	$RoleValue = set_value('role_id');
	$ImageValue = set_value('user_image');
}
?>

<div class="content">
	<div class="container-fluid">
		<?php echo form_open_multipart(current_url()); ?>
		<div class="row">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header card-header-icon" data-background-color="blue">
						<i class="material-icons">border_color</i>
					</div>
					<div class="card-content">
						<h4 class="card-title"><?php echo $operation ?></h4>
						<div class="card-content">

							<?php echo validation_errors(); ?>
							<?php if (isset($user)) { ?>
							<input type="hidden" name="user_id" value="<?php echo $user['user_id']; ?>">
							<?php } ?>

							<form>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group label-floating">
											<label class="control-label">NIK Karyawan</label>
											<input type="text" class="form-control" name="user_nik" <?php echo (isset($user)) ? 'disabled' : ''; ?> value="<?php echo $NameValue ?>">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="form-group label-floating">
											<label class="control-label">Nama Lengkap</label>
											<input class="form-control" name="user_full_name" value="<?php echo $FullNameValue ?>">
										</div>
									</div>
								</div>

								<?php if (!isset($user)) { ?>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group label-floating">
											<label class="control-label">Password</label>
											<input type="password" class="form-control" name="user_password">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-12">
										<div class="form-group label-floating">
											<label class="control-label">Konfirmasi Password</label>
											<input type="password" class="form-control" name="passconf">
										</div>
									</div>
								</div>
								<?php } ?>

								<div class="row">
									<div class="col-md-12">
										<div class="form-group label-floating">
											<label>Hak Akses</label>
											<select name="role_id" class="form-control">
												<option value="">-- Pilih Hak Akses --</option>
												<?php foreach ($roles as $row): ?>
													<option value="<?php echo $row['role_id']; ?>" <?php echo ($RoleValue == $row['role_id']) ? 'selected' : '' ?>><?php echo $row['role_name']; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4"> 
				<div class="card card-profile">
					<div class="card-avatar">
						<a href="#" class="thumbnail">
							<?php if (isset($user) AND $user['user_image'] != NULL) { ?>
							<img src="<?php echo upload_url('users/' . $user['user_image']) ?>" class="img-responsive avatar">
							<?php } else { ?>
							<img id="target" class="img-circle"><br><br>
							<?php } ?>
						</a>
					</div>
					<div class="content">
						<center><input type='file' id="user_image" name="user_image"></center>
						<h4 class="card-title">Upload Foto</h4>

						<br>
						<button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
						<a href="<?php echo site_url('admin/users'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
						<?php if (isset($user)): ?>
						<a href="#delModal" class="btn btn-flat btn-wd btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<?php if (isset($user)) { ?>
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">Konfirmasi Penghapusan</h4>
			</div>
			<div class="modal-body">
				<p>Apakah anda yakin akan menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
				<?php echo form_open('admin/users/delete/' . $user['user_id']); ?>
				<input type="hidden" name="delName" value="<?php echo $user['user_full_name']; ?>">
				<button type="button" class="btn btn-info btn-wd" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-danger btn-wd">Hapus</button>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>


<script>
	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#target').attr('src', e.target.result);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#user_image").change(function() {
		readURL(this);
	});
</script>

<script type="text/javascript">
	$('#datepicker').datepicker('hide');
</script>
