<div class="content">
	<div class="container-fluid">
		<?php echo form_open_multipart(current_url()); ?>
		<div class="row">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header card-header-icon" data-background-color="rose">
						<i class="material-icons">autorenew</i>
					</div>
					<div class="card-content">
						<h4 class="title">Reset Password</h4>
						<?php echo validation_errors(); ?>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group label-floating">
									<label class="control-label">Password Baru *</label>
									<input type="password" class="form-control" name="user_password">
									<?php if ($this->uri->segment(3) == 'cpw') { ?>
									<input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id'); ?>" >
									<?php } else { ?>
									<input type="hidden" name="user_id" value="<?php echo $user['user_id'] ?>" >
									<?php } ?>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group label-floating">
							<label class="control-label">Konfirmasi Password Baru *</label>
							<input type="password" class="form-control" name="passconf">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group label-floating">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card card-profile">
					<div class="content">
						<button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
						<a href="<?php echo site_url('admin/users'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

