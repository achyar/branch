<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-header-icon" data-background-color="red">
						<i class="material-icons">assignment</i>
					</div>
					<div class="card-content">
						<h4 class="card-title">Pengguna
							<a href="<?php echo site_url('admin/users/add') ?>" class="btn btn-danger btn-xs pull-right"><i class="material-icons">add_box</i> Tambah</a>
							<p class="category">List</p></h4>

							<div class="table-responsive">
								<table class="table table-hover">
									<thead class="text-danger">
										<th>No</th>
										<th>NIK Karyawan</th>
										<th>Nama</th>
										<th>Hak Akses</th>
										<th>Aksi</th>
									</thead>
									<tbody>
										<?php
										if (!empty($user)) {
											$i = 1;
											foreach ($user as $row):
												?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row['user_nik']; ?></td>
												<td><?php echo $row['user_full_name']; ?></td>
												<td><?php echo $row['role_name']; ?></td>
												<td>
													<a href="<?php echo site_url('admin/users/view/' . $row['user_id']) ?>" rel="tooltip" title="Lihat" class="text-warning"><i class="material-icons">pageview</i></a> &nbsp;

													<a href="<?php echo site_url('admin/users/edit/' . $row['user_id']) ?>" rel="tooltip" title="Edit" class="text-success"><i class="material-icons">mode_edit</i></a> &nbsp;

													<?php if ($this->session->userdata('uid') != $row['user_id']) { ?>
													<a href="<?php echo site_url('admin/users/rpw/' . $row['user_id']) ?>" rel="tooltip" title="Reset Password" class="text-info"><i class="material-icons">lock_open</i></a> &nbsp;
													<?php } else {
														?>
														<a href="<?php echo site_url('admin/profile/cpw/'); ?>" rel="tooltip" title="Ubah Password" class="text-info"><i class="material-icons">refresh</i></a> &nbsp;
														<?php } ?>

													</td>
												</tr>
												<?php
												$i++;
												endforeach;
											} else {
												?>
												<tr id="row">
													<td colspan="5" align="center">Data Kosong</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div>
								<?php echo $this->pagination->create_links(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
