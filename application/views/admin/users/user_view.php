<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-plain">
					<div class="card-header card-header-icon" data-background-color="blue">
					<i class="material-icons">details</i>
				</div>
				<h4 class="card-title">Pengguna</h4>
				<p class="category">Detail</p>
				<div class="col-md-10 col-sm-12 col-xs-12 pull-left">
					<br><br>
					<div class="row">
						<div class="col-md-2">
							<?php if (!empty($user['user_image'])) { ?>
							<img src="<?php echo upload_url('users/'.$user['user_image']) ?>" class="img-responsive avatar">
							<?php } else { ?>
							<img src="<?php echo media_url('img/photo.jpg') ?>" class="img-responsive avatar">
							<?php } ?>
						</div>
						<div class="card-content table-responsive">
							<table class="table table-hover">
								<tbody>
									<tr>
										<td>Nama Pengguna</td>
										<td>:</td>
										<td><?php echo $user['user_nik'] ?></td>
									</tr>
									<tr>
										<td>Nama Lengkap</td>
										<td>:</td>
										<td><?php echo $user['user_full_name'] ?></td>
									</tr>
									<tr>
										<td>Hak Akses</td>
										<td>:</td>
										<td><?php echo $user['role_name'] ?></td>
									</tr>
									<tr>
										<td>Di Buat Tanggal</td>
										<td>:</td>
										<td><?php echo pretty_date($user['user_input_date'],'d F Y',false) ?></td>
									</tr>

								</tbody>
							</table>
						</div>
						<div class="col-md-12">
							<a href="<?php echo site_url('admin/users') ?>" class="btn btn-app btn-success">
								<i class="fa fa-arrow-circle-o-left"></i> Kembali
							</a>
							<a href="<?php echo site_url('admin/users/edit/' . $user['user_id']) ?>" class="btn btn-app btn-info">
								<i class="fa fa-edit"></i> Edit
							</a>
							<?php if ($this->session->userdata('user_id') != $user['user_id']) { ?>
							<a href="#delModal" data-toggle="modal" class="btn btn-app btn-danger">
								<i class="fa fa-trash"></i> Hapus
							</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="delModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><b>Konfirmasi Penghapusan</b></h4>
			</div>
			<div class="modal-body">
				<p>Data yang dipilih akan dihapus oleh sistem, apakah anda yakin?;</p>
			</div>
			<?php echo form_open('admin/users/delete/' . $user['user_id']); ?>
			<div class="modal-footer">
				<a><button style="float: right;margin-left: 10px" type="button" class="btn btn-info" data-dismiss="modal">Tidak</button></a>
				<input type="hidden" name="del_id" value="<?php echo $user['user_id'] ?>" />
				<input type="hidden" name="del_name" value="<?php echo $user['user_name'] ?>" />
				<button type="submit" class="btn btn-danger">Ya</button>
			</div>
			<?php echo form_close(); ?>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
