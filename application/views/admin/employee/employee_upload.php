
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">border_color</i>
                    </div>
                    <div class="card-content">

                     <h3><i class="fa fa-file-excel-o text-green"></i> Upload Data Karyawan</h3>

                     <br>
                     <h4>Petunjuk Singkat</h4>
                     <p>Penginputan data Karyawan bisa dilakukan dengan mengcopy data dari file Ms. Excel. Format file excel harus sesuai kebutuhan aplikasi. Silahkan download formatnya <a href="<?php echo site_url('admin/employee/download');?>"><span class="label label-success">Disini</span></a>
                        <br><br>
                        <strong>CATATAN :</strong>
                        <ol>
                            <li>Pengisian jenis data <strong>TANGGAL</strong>  diisi dengan format <strong>YYYY-MM-DD</strong> Contoh <strong>2017-12-21</strong><br> Cara ubah : blok semua tanggal pilih format cell di excel ganti dengan format date pilih yang tahunnya di depan</li>  
                        </ol>
                    </p>
                    <hr>

                    <?php echo form_open_multipart(current_url()) ?>

                    <div class="form-group">
                        <div class="col-md-12">
                            <textarea placeholder="Copy data yang akan dimasukan dari file excel, dan paste disini" rows="10" class="form-control" name="rows"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success btn-sm btn-flat"><i class="fa fa-save"></i> Import</button>
                            <a href="<?php echo site_url('employee');?>" class="btn btn-info btn-sm btn-flat"><i class="fa fa-angle-double-left"></i> Kembali</a>
                        </div>
                    </div>
                    <?php echo form_close() ?>
                </div>   
            </div>
        </div>
    </div>
</div>
</div>








