<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-header-icon" data-background-color="red">
						<i class="material-icons">perm_identity</i>
					</div>
					<div class="card-content">
						<h4 class="card-title">Karyawan
							<a href="<?php echo site_url('admin/employee/add') ?>" class="btn btn-danger btn-xs pull-right"><i class="material-icons">add_box</i> Tambah</a>
							<a href="<?php echo site_url('admin/employee/delete'); ?>" onclick="return confirm('Apakah Anda akan menghapus semua data employee?')" class="btn btn-rose btn-xs pull-right"><i class="material-icons">highlight_off</i> Hapus Semua</a>
							<p class="category">List</p></h4>

							<div class="table-responsive">
								<table class="table table-hover"> 
									<thead class="text-danger">
										<th>No</th>
										<th>NIK Karyawan</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>Aksi</th>
									</thead>
									<tbody>
										<?php
										if (!empty($employee)) {
											$i = 1;
											foreach ($employee as $row):
												?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row['employee_nik']; ?></td>
												<td><?php echo $row['employee_name']; ?></td>
												<td><?php echo $row['employee_position']; ?></td>
												<td>
													<a href="<?php echo site_url('admin/employee/view/' . $row['employee_id']) ?>" rel="tooltip" title="Lihat" class="text-warning"><i class="material-icons">pageview</i></a> &nbsp;

													<a href="<?php echo site_url('admin/employee/edit/' . $row['employee_id']) ?>" rel="tooltip" title="Edit" class="text-success view-pdf"><i class="material-icons">mode_edit</i></a> &nbsp;

													</td>
												</tr>
												<?php
												$i++;
												endforeach;
											} else {
												?>
												<tr id="row">
													<td colspan="5" align="center">Data Kosong</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div>
								<?php echo $this->pagination->create_links(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
