<?php
// $this->load->view('admin/datepicker');
if (isset($employee)) {

    $NikValue       = $employee['employee_nik'];
    $NamaValue      = $employee['employee_name'];
    $AccValue      = $employee['employee_account'];
    $PlaceValue      = $employee['employee_place'];
    $BirthValue      = $employee['employee_birth_date'];
    $AddValue      = $employee['employee_address'];
    $EntValue      = $employee['employee_entry_date'];
    $PosValue      = $employee['employee_position'];
    $DivValue      = $employee['employee_divisi'];
    $DeptValue      = $employee['employee_dept'];
    $UnitValue      = $employee['employee_unit'];
    $BussValue      = $employee['employee_bussiness'];
    $PhoneValue     = $employee['employee_phone'];
} else {
    $NikValue       = set_value('employee_nik');
    $NamaValue      = set_value('employee_name');
    $AccValue      = set_value('employee_account');
    $PlaceValue      = set_value('employee_place');
    $BirthValue      = set_value('employee_birth_date');
    $AddValue      = set_value('employee_address');
    $EntValue      = set_value('employee_entry_date');
    $PosValue     = set_value('employee_position');
    $DivValue      = set_value('employee_divisi');
    $DeptValue      = set_value('employee_dept');
    $UnitValue      = set_value('employee_unit');
    $BussValue      = set_value('employee_bussiness');
    $PhoneValue     = set_value('employee_phone');
}
?>

<div class="content">
    <div class="container-fluid">
        <?php echo form_open_multipart(current_url()); ?>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="blue">
                        <i class="material-icons">border_color</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $operation ?></h4>
                        <div class="card-content">

                            <?php echo validation_errors(); ?>
                            <?php if (isset($employee)) { ?>
                            <input type="hidden" name="employee_id" value="<?php echo $employee['employee_id']; ?>">
                            <?php } ?>

                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">NIK Karyawan</label>
                                            <input type="text" class="form-control" name="employee_nik" value="<?php echo $NikValue ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama Lengkap</label>
                                            <input class="form-control" name="employee_name" value="<?php echo $NamaValue ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No Rekening</label>
                                            <input class="form-control" name="employee_account" value="<?php echo $AccValue ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Tempat Lahir</label>
                                            <input class="form-control" name="employee_place" value="<?php echo $PlaceValue ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Tanggal Lahir</label>
                                            <input type="text" class="form-control datepicker" name="employee_birth_date" value="<?php echo $BirthValue ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Tanggal Masuk</label>
                                            <input class="form-control datepicker" name="employee_entry_date" value="<?php echo $EntValue ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Alamat</label>
                                            <textarea class="form-control" name="employee_address"><?php echo $AddValue ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Jabatan</label>
                                            <input class="form-control" name="employee_position" value="<?php echo $PosValue ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Divisi</label>
                                            <input class="form-control" name="employee_divisi" value="<?php echo $DivValue ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Departement</label>
                                            <input class="form-control" name="employee_dept" value="<?php echo $DeptValue ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Kode Unit</label>
                                            <input class="form-control" name="employee_unit" value="<?php echo $UnitValue ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama Unit</label>
                                            <input class="form-control" name="employee_bussiness" value="<?php echo $BussValue ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No. Telepon</label>
                                            <input class="form-control" name="employee_phone" value="<?php echo $PhoneValue ?>">
                                        </div>
                                    </div>
                                </div>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4"> 
                <div class="card card-profile">
                    <div class="content">
                        <button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
                        <a href="<?php echo site_url('admin/employee'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                        <?php if (isset($employee)): ?>
                            <a href="#delModal" class="btn btn-flat btn-wd btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?php if (isset($employee)) { ?>
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Konfirmasi Penghapusan</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin akan menghapus data ini ?</p>
            </div>
            <div class="modal-footer">
                <?php echo form_open('admin/employee/delete/' . $employee['employee_id']); ?>
                <input type="hidden" name="delName" value="<?php echo $employee['employee_name']; ?>">
                <button type="button" class="btn btn-info btn-wd" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger btn-wd">Hapus</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>
