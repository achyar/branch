<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header card-header-icon" data-background-color="blue">
                        <i class="material-icons">details</i>
                    </div>
                    <h4 class="card-title">Karyawan</h4>
                    <p class="category">Detail</p>
                    <div class="col-md-10 col-sm-12 col-xs-12 pull-left">
                        <br><br>
                        <div class="row">
                            <div class="card-content table-responsive">
                                <table class="table table-hover">
                                    <tbody>
                                        <tr>
                                            <td>NIK Karyawan</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_nik'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Karyawan</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_name'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Rekening</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_account'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tempat Lahir</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_place'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal lahir</td>
                                            <td>:</td>
                                            <td><?php echo pretty_date($employee['employee_birth_date'],'d F Y',false) ?></td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_address'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Masuk</td>
                                            <td>:</td>
                                            <td><?php echo pretty_date($employee['employee_entry_date'],'d F Y',false) ?></td>
                                        </tr>
                                        <tr>
                                            <td>Jabatan</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_position'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Divisi</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_divisi'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Departement</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_dept'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Kode Toko</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_unit'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Toko</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_bussiness'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Nomor Telp</td>
                                            <td>:</td>
                                            <td><?php echo $employee['employee_phone'] ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <a href="<?php echo site_url('admin/employee') ?>" class="btn btn-app btn-success">
                                    <i class="fa fa-arrow-circle-o-left"></i> Kembali
                                </a>
                                <a href="<?php echo site_url('admin/employee/edit/' . $employee['employee_id']) ?>" class="btn btn-app btn-info">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                                <a href="#delModal" data-toggle="modal" class="btn btn-app btn-danger">
                                    <i class="fa fa-trash"></i> Hapus
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><b>Konfirmasi Penghapusan</b></h4>
                </div>
                <div class="modal-body">
                    <p>Data yang dipilih akan dihapus oleh sistem, apakah anda yakin?;</p>
                </div>
                <?php echo form_open('admin/employees/delete/' . $employee['employee_id']); ?>
                <div class="modal-footer">
                    <a><button style="float: right;margin-left: 10px" type="button" class="btn btn-info" data-dismiss="modal">Tidak</button></a>
                    <input type="hidden" name="del_id" value="<?php echo $employee['employee_id'] ?>" />
                    <input type="hidden" name="del_name" value="<?php echo $employee['employee_name'] ?>" />
                    <button type="submit" class="btn btn-danger">Ya</button>
                </div>
                <?php echo form_close(); ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
