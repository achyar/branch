<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-4">
				<div class="card card-chart">
					<div class="card-header" data-background-color="rose" data-header-animation="true">
					<center><i class="material-icons md-lg">home</i></center>
					</div>
					<div class="card-content">
						<div class="card-actions">
							Arca CMS
						</div>
						<h4 class="card-title">Dashboard</h4>
						<p class="category">Last Campaign Performance</p>
					</div>
					<div class="card-footer">
						<div class="stats">
							<i class="material-icons">access_time</i> view all
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card card-chart">
					<div class="card-header" data-background-color="blue" data-header-animation="true">
					<center><i class="material-icons md-lg">person</i></center>
					</div>
					<div class="card-content">
						<div class="card-actions">
							Arca CMS
						</div>
						<h4 class="card-title">Management Users</h4>
						<p class="category">Last Campaign Performance</p>
					</div>
					<div class="card-footer">
						<div class="stats">
							<i class="material-icons">access_time</i><a href="<?php echo site_url('admin/user') ?>"> View All</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card card-chart">
					<div class="card-header" data-background-color="purple" data-header-animation="true">
					<center><i class="material-icons md-lg">history</i></center>
					</div>
					<div class="card-content">
						<div class="card-actions">
							Arca CMS
						</div>
						<h4 class="card-title">Logs History</h4>
						<p class="category">Last Campaign Performance</p>
					</div>
					<div class="card-footer">
						<div class="stats">
							<i class="material-icons">access_time</i><a href="<?php echo site_url('admin/logs') ?>"> View All</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
	.material-icons.md-18 { font-size: 18px; }
	.material-icons.md-24 { font-size: 24px; }
	.material-icons.md-36 { font-size: 36px; }
	.material-icons.md-lg { font-size: 200px; }

	/* Rules for using icons as black on a light background. */
	.material-icons.md-dark { color: rgba(0, 0, 0, 0.54); }
	.material-icons.md-dark.md-inactive { color: rgba(0, 0, 0, 0.26); }

	/* Rules for using icons as white on a dark background. */
	.material-icons.md-light { color: rgba(255, 255, 255, 1); }
	.material-icons.md-light.md-inactive { color: rgba(255, 255, 255, 0.3); }
</style>