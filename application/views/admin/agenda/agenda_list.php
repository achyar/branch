<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">date_range</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Agenda
                            <a href="<?php echo site_url('admin/agenda/add') ?>" class="btn btn-danger btn-xs pull-right"><i class="material-icons">add_box</i> Tambah</a>
                            <p class="category">List</p></h4>

                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="text-danger">
                                        <th>No</th>
                                        <th>Agenda</th>
                                        <th>Tanggal</th>
                                        <th>Status</th>
                                        <th>Aksi</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($agenda)) {
                                            $i = 1;
                                            foreach ($agenda as $row):
                                                ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $row['agenda_text']; ?></td>
                                                <td><?php echo pretty_date($row['agenda_date'],'d F Y',false) ?></td>
                                                <td><?php echo ($row['agenda_status'] == 0) ? 'Non Aktif' : 'Aktif'  ?></td>
                                                <td>
                                                    <a href="<?php echo site_url('admin/agenda/view/' . $row['agenda_id']) ?>" rel="tooltip" title="Lihat" class="text-warning"><i class="material-icons">pageview</i></a> &nbsp;

                                                    <a href="<?php echo site_url('admin/agenda/edit/' . $row['agenda_id']) ?>" rel="tooltip" title="Edit" class="text-success view-pdf"><i class="material-icons">mode_edit</i></a> &nbsp;

                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                                endforeach;
                                            } else {
                                                ?>
                                                <tr id="row">
                                                    <td colspan="5" align="center">Data Kosong</td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
