<?php
if (isset($agenda)) {

    $Isi = $agenda['agenda_text'];
    $Date = $agenda['agenda_date'];
    $Ket = $agenda['agenda_status'];

} else {

    $Isi = set_value('agenda_text');
    $Date = set_value('agenda_date');    
    $Ket = set_value('agenda_status');    
}
?>

<div class="content">
    <div class="container-fluid">
        <?php echo form_open_multipart(current_url()); ?>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">border_color</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $operation ?></h4>
                        <div class="card-content">

                            <?php echo validation_errors(); ?>
                            <?php if (isset($agenda)) { ?>
                            <input type="hidden" name="agenda_id" value="<?php echo $agenda['agenda_id']; ?>">
                            <?php } ?>

                            <form>
                                <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Isi Agenda</label>
                                        <textarea class="form-control" name="agenda_text"><?php echo $Isi ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Tanggal Agenda</label>
                                        <input type="text" class="form-control datepicker" name="agenda_date" value="<?php echo $Date ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Status Tampil</label>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="agenda_status" value="1" <?php echo ($Ket == '1') ? 'checked' : ''; ?>> Aktif
                                            </label>
                                            &nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="radio" name="agenda_status" value="0" <?php echo ($Ket == '0') ? 'checked' : ''; ?>> Non Aktif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4"> 
            <div class="card card-profile">
                <div class="content">
                    <button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
                    <a href="<?php echo site_url('admin/agenda'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                    <?php if (isset($agenda)): ?>
                        <a href="#delModal" class="btn btn-flat btn-wd btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
</div>
<?php if (isset($agenda)) { ?>
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Konfirmasi Penghapusan</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin akan menghapus data ini ?</p>
            </div>
            <div class="modal-footer">
                <?php echo form_open('admin/agenda/delete/' . $agenda['agenda_id']); ?>
                <input type="hidden" name="delName" value="<?php echo $agenda['agenda_text']; ?>">
                <button type="button" class="btn btn-info btn-wd" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger btn-wd">Hapus</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>


