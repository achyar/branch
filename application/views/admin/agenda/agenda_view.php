<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agenda
            <small>Detail</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Master agenda</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                            <table class="table table-condensed">
                                <tbody>
                                    <tr>
                                        <td>Isi Agenda</td>
                                        <td>:</td>
                                        <td><?php echo $agenda['agenda_text'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal</td>
                                        <td>:</td>
                                        <td><?php echo pretty_date($agenda['agenda_date'],'d F Y',false); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        <td><?php echo ($agenda['agenda_status'] == 0) ? 'NON-AKTIF' : 'AKTIF' ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <a href="<?php echo site_url('admin/agenda') ?>" class="btn btn-app">
                                <i class="fa fa-arrow-circle-o-left"></i> Kembali
                            </a>
                            
                                <a href="<?php echo site_url('admin/agenda/edit/' . $agenda['agenda_id']) ?>" class="btn btn-app">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                                <?php if ($this->session->userdata('uid') != $agenda['agenda_id']) { ?>
                                    <a href="#delModal" data-toggle="modal" class="btn btn-app">
                                        <i class="fa fa-trash"></i> Hapus
                                    </a>
                               
                            <?php } ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->

    <div class="modal modal-danger fade" id="delModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title"><span class="fa fa-warning"></span> Konfirmasi penghapusan</h3>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini?</p>
                </div>
                <div class="modal-footer">
                    <?php echo form_open('admin/agenda/delete/' . $agenda['agenda_id']); ?>
                    <input type="hidden" name="del_nama" value="<?php echo $agenda['agenda_nama']; ?>">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Batal</button>
                    <button type="submit" class="btn btn-outline"><span class="fa fa-check"></span> Hapus</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>