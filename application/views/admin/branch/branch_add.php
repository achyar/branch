<?php
$this->load->view('admin/datepicker');
if (isset($branch)) {

    $Kode = $branch['branch_kode'];
    $Nama = $branch['branch_nama'];
   
} else {

    $Kode = set_value('branch_kode');
    $Nama = set_value('branch_nama');    
}
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Master Branch
            <small><?php echo $operation; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Branch</li>
            <li class="active"><?php echo $operation; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo form_open_multipart(current_url()); ?>
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-9">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo validation_errors(); ?>
                        <?php if (isset($branch)) { ?>
                            <input type="hidden" name="branch_id" value="<?php echo $branch['branch_id']; ?>">
                            <?php } ?>
                            <div class="form-group">
                                <label>Kode Branch <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <input name="branch_kode" type="text" class="form-control" value="<?php echo $Kode ?>" placeholder="Kode Branch">
                            </div>

                            <div class="form-group">
                                <label>Nama Branch <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <input name="branch_nama" type="text" class="form-control" value="<?php echo $Nama ?>" placeholder="Nama Branch">
                            </div>
                       
                        <p class="text-muted">*) Kolom wajib diisi.</p>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-3">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <button type="submit" class="btn btn-flat btn-block btn-success"><span class="fa fa-check"></span> Simpan</button>
                        <a href="<?php echo site_url('admin/branch'); ?>" class="btn btn-flat btn-block btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                        <?php if (isset($branch)): ?>
                            <a href="#delModal" class="btn btn-flat btn-block btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                        <?php endif; ?>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <?php if (isset($branch)) { ?>
        <div class="modal modal-danger fade" id="delModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h3 class="modal-title"><span class="fa fa-warning"></span> Konfirmasi penghapusan</h3>
                        </div>
                        <div class="modal-body">
                            <p>Apakah anda yakin akan menghapus data ini?</p>
                        </div>
                        <div class="modal-footer">
                            <?php echo form_open('admin/branch/delete/' . $branch['branch_id']); ?>
                            <input type="hidden" name="del_name" value="<?php echo $branch['branch_nama']; ?>">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Batal</button>
                            <button type="submit" class="btn btn-outline"><span class="fa fa-check"></span> Hapus</button>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <?php } ?>

        </div>


        