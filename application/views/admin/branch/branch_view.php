<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Master Branch
            <small>Detail</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Master Branch</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                            <table class="table table-condensed">
                                <tbody>
                                    <tr>
                                        <td>Kode Branch</td>
                                        <td>:</td>
                                        <td><?php echo $branch['branch_kode'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Branch</td>
                                        <td>:</td>
                                        <td><?php echo $branch['branch_nama'] ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <a href="<?php echo site_url('admin/branch') ?>" class="btn btn-app">
                                <i class="fa fa-arrow-circle-o-left"></i> Kembali
                            </a>
                            <?php if ($this->session->userdata('uroleid') == ROLE_MASTER) { ?>
                                <a href="<?php echo site_url('admin/branch/edit/' . $branch['branch_id']) ?>" class="btn btn-app">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                                <?php if ($this->session->userdata('uid') != $branch['branch_id']) { ?>
                                    <a href="#delModal" data-toggle="modal" class="btn btn-app">
                                        <i class="fa fa-trash"></i> Hapus
                                    </a>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->

    <div class="modal modal-danger fade" id="delModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title"><span class="fa fa-warning"></span> Konfirmasi penghapusan</h3>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin akan menghapus data ini?</p>
                </div>
                <div class="modal-footer">
                    <?php echo form_open('admin/branch/delete/' . $branch['branch_id']); ?>
                    <input type="hidden" name="del_nama" value="<?php echo $branch['branch_nama']; ?>">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Batal</button>
                    <button type="submit" class="btn btn-outline"><span class="fa fa-check"></span> Hapus</button>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>