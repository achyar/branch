<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Master Branch
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li class="pull-right"><a href="<?php echo site_url('admin/branch/add'); ?>" class="btn btn-info"><i class="fa fa-plus"></i> Tambah</a></li>
        </ol>
    </section><br>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-init table-bordered table-striped jambo_table">
                            <thead class="gradient">
                                <tr>
                                    <th>No</th> 
                                    <th>Kode Branch</th>
                                    <th>Nama Branch</th>                                    
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($branch > 0) {
                                    $i = 1;
                                    foreach ($branch as $row):
                                        ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['branch_kode']; ?></td>
                                        <td><?php echo $row['branch_nama']; ?></td>                                        
                                        <td>
                                            <a href="<?php echo site_url('admin/branch/detail/' . $row['branch_id']) ?>" data-toggle="tooltip" title="Lihat" class="text-warning"><span class="fa fa-eye"></span></a> &nbsp;
                                           
                                                <a href="<?php echo site_url('admin/branch/edit/' . $row['branch_id']) ?>" data-toggle="tooltip" title="Sunting" class="text-success"><span class="fa fa-edit"></span></a> &nbsp;
                                                      
                                                    </td>
                                                </tr>
                                                    <?php
                                                    $i++;
                                                    endforeach;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->

                    </section>
                    <!-- /.content -->
                </div>