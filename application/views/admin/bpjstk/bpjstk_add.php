<?php
if (isset($bpjstk)) {
    $inputNpp = $bpjstk['bpjstk_npp'];    
    $inputName = $bpjstk['bpjstk_name'];
    $inputDesc = $bpjstk['bpjstk_desc']; 
    $inputDate = $bpjstk['bpjstk_entry_date'];
    $inputCard = $bpjstk['bpjstk_card'];

} else {    
    $inputNpp = set_value('bpjstk_npp');   
    $inputName = set_value('bpjstk_name');
    $inputDesc = set_value('bpjstk_desc');
    $inputDate = set_value('bpjstk_entry_date');
    $inputCard = set_value('bpjstk_card');
}
?>

<div class="content">
    <div class="container-fluid">
        <?php echo form_open_multipart(current_url()); ?>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">border_color</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $operation ?></h4>
                        <div class="card-content">

                            <?php echo validation_errors(); ?>
                            <?php if (isset($bpjstk)) { ?>
                            <input type="hidden" name="bpjstk_id" value="<?php echo $bpjstk['bpjstk_id']; ?>">
                            <?php } ?>

                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama Karyawan</label>
                                            <input type="text" class="form-control" name="bpjstk_name" value="<?php echo $inputName ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">NPP</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="bpjstk_npp" value="KK041803" <?php echo ($inputNpp == 'KK041803') ? 'checked' : ''; ?>> REG
                                                </label>
                                                &nbsp;&nbsp;&nbsp;
                                                <label>
                                                    <input type="radio" name="bpjstk_npp" value="KK044024" <?php echo ($inputNpp == 'KK044024') ? 'checked' : ''; ?>> FRC
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nomor Kartu</label>
                                            <input class="form-control" name="bpjstk_card" value="<?php echo $inputCard ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Tanggal Kepesertaan Awal</label>
                                            <input class="form-control datepicker" placeholder="<?php echo date('Y-m-d') ?>" name="bpjstk_entry_date" value="<?php echo $inputDate ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Pilih Tujuan</label>
                                            <select name="bpjstk_desc" class="selectpicker" data-style="select-with-transition" title="--- Pilihan ---">
                                                <option value="Belum Mendapatkan Kartu Peserta BPJS Ketenagakerjaan Plastik" <?php echo ($inputDesc == 'Belum Mendapatkan Kartu Peserta BPJS Ketenagakerjaan Plastik') ? 'selected' : '' ?>>Belum Mendapatkan Kartu Peserta BPJS Ketenagakerjaan Plastik</option>
                                                <option value="Kartu BPJS Ketenagakerjaan Plastik Hilang" <?php echo ($inputDesc == 'Kartu BPJS Ketenagakerjaan Plastik Hilang') ? 'selected' : '' ?>>Kartu BPJS Ketenagakerjaan Plastik Hilang</option>
                                                <option value="Koreksi Kartu Kepesertaan" <?php echo ($inputDesc == 'Koreksi Kartu Kepesertaan') ? 'selected' : '' ?>>Koreksi Kartu Kepesertaan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4"> 
                <div class="card card-profile">
                    <div class="content">
                        <button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
                        <a href="<?php echo site_url('admin/bpjstk'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                        <?php if (isset($bpjstk)): ?>
                            <a href="#delModal" class="btn btn-flat btn-wd btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?php if (isset($bpjstk)) { ?>
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Konfirmasi Penghapusan</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin akan menghapus data ini ?</p>
            </div>
            <div class="modal-footer">
                <?php echo form_open('admin/bpjstk/delete/' . $bpjstk['bpjstk_id']); ?>
                <input type="hidden" name="delName" value="<?php echo $bpjstk['bpjstk_name']; ?>">
                <button type="button" class="btn btn-info btn-wd" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger btn-wd">Hapus</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>