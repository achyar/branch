<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-plain">
					<div class="card-header card-header-icon" data-background-color="rose">
						<i class="material-icons">details</i>
					</div>
					<h4 class="card-title">Profile</h4>
					<p class="category">Detail</p>
					<div class="col-md-10 col-sm-12 col-xs-12 pull-left">
						<br><br>
						<div class="row">
							<div class="col-md-2">
								<?php if (!empty($user['user_image'])) { ?>
								<img src="<?php echo upload_url('users/'.$user['user_image']) ?>" class="img-responsive avatar">
								<?php } else { ?>
								<img src="<?php echo media_url('img/photo.jpg') ?>" class="img-responsive avatar">
								<?php } ?>
							</div>
							<div class="card-content table-responsive">
								<table class="table table-hover">
									<tbody>
										<tr>
											<td>Nama Pengguna</td>
											<td>:</td>
											<td><?php echo $user['user_nik'] ?></td>
										</tr>
										<tr>
											<td>Nama Lengkap</td>
											<td>:</td>
											<td><?php echo $user['user_full_name'] ?></td>
										</tr>
										<tr>
											<td>Hak Akses</td>
											<td>:</td>
											<td><?php echo $user['role_name'] ?></td>
										</tr>
										<tr>
											<td>Di Buat Tanggal</td>
											<td>:</td>
											<td><?php echo pretty_date($user['user_input_date'],'d F Y',false) ?></td>
										</tr>

									</tbody>
								</table>
							</div>
							<div class="col-md-12">
								<a href="<?php echo site_url('admin') ?>" class="btn btn-app btn-success">
									<i class="fa fa-arrow-circle-o-left"></i> Kembali
								</a>
								<a href="<?php echo site_url('admin/profile/edit/') ?>" class="btn btn-app btn-info">
									<i class="fa fa-edit"></i> Edit
								</a>
								<a href="<?php echo site_url('admin/profile/cpw/') ?>" class="btn btn-app btn-warning">
									<i class="fa fa-repeat"></i> Ubah Password
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>