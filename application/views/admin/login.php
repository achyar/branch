
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login | HRD Apps</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo media_url('ico/favicon.png'); ?>" type="image/x-icon">
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo media_url() ?>css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo media_url() ?>css/material-dashboard.css" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo media_url() ?>css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="<?php echo media_url() ?>css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo media_url() ?>css/load-font.css"/>
    <!-- Theme initialization -->
    <!--   Core JS Files   -->

<script src="<?php echo media_url() ?>js/jquery-3.2.0.min.js" type="text/javascript"></script>
<script src="<?php echo media_url() ?>js/jquery-migrate-3.0.0.min.js"></script>
<script src="<?php echo media_url() ?>js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo media_url() ?>js/angular.min.js" type="text/javascript" ></script>
<script src="<?php echo media_url() ?>js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo media_url() ?>js/material.min.js" type="text/javascript"></script>
<script src="<?php echo media_url() ?>js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>


</head>

<body>
    <nav class="navbar navbar-primary navbar-transparent navbar-absolute">
        <div class="container">
            <!-- <div class="navbar-header">
                <a class="navbar-brand" href=" #"><img src="<?php echo media_url() ?>/img/alfamart.png" height="90px" width="auto" class="hidden-xs"></a>
            </div> -->
        </div>
    </nav>
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="<?php echo media_url() ?>/img/background.jpg">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <div class="card card-login card-hidden">
                                <div class="card-header text-center" data-background-color="red">
                                    <!-- <h4 class="card-title">Login</h4> -->
                                    <!-- <div class="social-line"> -->
                                        <img src="<?php echo media_url() ?>/img/alfamart.png">
                                    <!-- </div> -->
                                </div>
                                <p class="category text-center">
                                    Dear user login here!
                                </p>
                                <div class="card-content">
                                <?php echo form_open('admin/auth/login'); ?>
                            <?php
                            if (isset($_GET['location'])) {
                                echo '<input type="hidden" name="location" value="';
                                if (isset($_GET['location'])) {
                                    echo htmlspecialchars($_GET['location']);
                                }
                                echo '" />';
                            } ?>
                            <!-- Jika Error -->
                            <?php if ($this->session->flashdata('failed')) { ?>
                            <div class="danger">
                                <center><h5><i class="material-icons text-danger">warning</i> <?php echo $this->session->flashdata('failed') ?></h5></center>
                            </div>
                            <?php } ?>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">face</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label">NIK</label>
                                            <input type="text" autofocus name="username" class="form-control">
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="material-icons">lock_outline</i>
                                        </span>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Password</label>
                                            <input type="password" name="password" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-danger btn-wd btn-lg">Login</button>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container">
                    <nav class="pull-left">

                    </nav>
                    <p class="copyright pull-right">
                        Copyright &copy; <script>document.write(new Date().getFullYear())</script> All rights reserved
                    </p>
                </div>
            </footer>
        </div>
    </div>
</body>
<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>

</html>
<!-- Forms Validations Plugin -->
<script src="<?php echo media_url() ?>js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo media_url() ?>js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="<?php echo media_url() ?>js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="<?php echo media_url() ?>js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo media_url() ?>js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="<?php echo media_url() ?>js/jquery.sharrre.js"></script>
<!-- DateTimePicker Plugin -->
<script src="<?php echo media_url() ?>js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="<?php echo media_url() ?>js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="<?php echo media_url() ?>js/nouislider.min.js"></script>
<!-- Select Plugin -->
<script src="<?php echo media_url() ?>js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="<?php echo media_url() ?>js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?php echo media_url() ?>js/sweetalert2.js"></script>
<!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?php echo media_url() ?>js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="<?php echo media_url() ?>js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="<?php echo media_url() ?>js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo media_url() ?>js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo media_url() ?>js/demo.js"></script>
