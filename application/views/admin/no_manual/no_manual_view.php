<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">details</i>
                    </div>
                    <h4 class="card-title">Nomor Surat Manual</h4>
                    <p class="category">Detail</p>
                    <div class="col-md-10 col-sm-12 col-xs-12 pull-left">
                        <br><br>
                        <div class="row">
                            <div class="card-content table-responsive">
                                <table class="table table-hover">
                                    <tbody>
                                        <tr>
                                            <td>No Surat </td>
                                            <td>:</td>
                                            <td><?php echo $no_manual['no_manual_letter'] ?></td>
                                        </tr>

                                        <tr>
                                            <td>Keterangan </td>
                                            <td>:</td>
                                            <td><?php echo $no_manual['no_manual_desc'] ?></td>
                                        </tr>

                                        <tr>
                                            <td>Tanggal Buat</td>
                                            <td>:</td>
                                            <td><?php echo pretty_date($no_manual['no_manual_input_date'],'d F Y',false); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Pembuat </td>
                                            <td>:</td>
                                            <td><?php echo $no_manual['user_full_name'] ?></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <a href="<?php echo site_url('admin/no_manual') ?>" class="btn btn-app btn-success">
                                    <i class="fa fa-arrow-circle-o-left"></i> Kembali
                                </a>
                                <a href="<?php echo site_url('admin/no_manual/edit/' . $no_manual['no_manual_id']) ?>" class="btn btn-app btn-info">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                                <a href="#delModal" data-toggle="modal" class="btn btn-app btn-danger">
                                    <i class="fa fa-trash"></i> Hapus
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="delModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><b>Konfirmasi Penghapusan</b></h4>
                </div>
                <div class="modal-body">
                    <p>Data yang dipilih akan dihapus oleh sistem, apakah anda yakin?;</p>
                </div>
                <?php echo form_open('admin/no_manual/delete/' . $no_manual['no_manual_id']); ?>
                <div class="modal-footer">
                    <a><button style="float: right;margin-left: 10px" type="button" class="btn btn-info" data-dismiss="modal">Tidak</button></a>
                    <input type="hidden" name="del_id" value="<?php echo $no_manual['no_manual_id'] ?>" />
                    <input type="hidden" name="del_name" value="<?php echo $no_manual['no_manual_desc'] ?>" />
                    <button type="submit" class="btn btn-danger">Ya</button>
                </div>
                <?php echo form_close(); ?>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
