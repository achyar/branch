<?php
$this->load->view('admin/datepicker');
if (isset($no_manual)) {

    $No = $no_manual['no_manual_letter'];
    $Desc = $no_manual['no_manual_desc'];
    
} else {

    $No = set_value('no_manual_letter');
    $Desc = set_value('no_manual_desc');    
}
?>

<div class="content">
    <div class="container-fluid">
        <?php echo form_open_multipart(current_url()); ?>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">border_color</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $operation ?></h4>
                        <div class="card-content">

                            <?php echo validation_errors(); ?>
                            <?php if (isset($no_manual)) { ?>
                            <input type="hidden" name="no_manual_id" value="<?php echo $no_manual['no_manual_id']; ?>">
                            <?php } ?>

                            <form>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Keterangan Untuk *</label>
                                            <textarea class="form-control" name="no_manual_desc"><?php echo $Desc ?></textarea>
                                        </div>
                                    </div>
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4"> 
                <div class="card card-profile">
                    <div class="content">
                        <button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
                        <a href="<?php echo site_url('admin/no_manual'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                        <?php if (isset($no_manual)): ?>
                            <a href="#delModal" class="btn btn-flat btn-wd btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?php if (isset($no_manual)) { ?>
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Konfirmasi Penghapusan</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin akan menghapus data ini ?</p>
            </div>
            <div class="modal-footer">
                <?php echo form_open('admin/no_manual/delete/' . $no_manual['no_manual_id']); ?>
                <input type="hidden" name="delName" value="<?php echo $no_manual['no_manual_desc']; ?>">
                <button type="button" class="btn btn-info btn-wd" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger btn-wd">Hapus</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>