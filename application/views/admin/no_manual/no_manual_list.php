<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">work</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Nomor Surat Manual
                            <a href="<?php echo site_url('admin/no_manual/add') ?>" class="btn btn-danger btn-xs pull-right"><i class="material-icons">add_box</i> Tambah</a>
                            <p class="category">List</p></h4>

                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="text-danger">
                                        <th>No</th> 
                                        <th>No Surat</th>
                                        <th>Keterangan</th>                                    
                                        <th>Tanggal Buat</th>                                    
                                        <th>Aksi</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($no_manual)) {
                                            $i = 1;
                                            foreach ($no_manual as $row):
                                                ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $row['no_manual_letter']; ?></td>
                                                <td><?php echo $row['no_manual_desc']; ?></td>
                                                <td><?php echo pretty_date($row['no_manual_input_date'],'d F Y',false); ?></td>
                                                <td>
                                                    <a href="<?php echo site_url('admin/no_manual/view/' . $row['no_manual_id']) ?>" rel="tooltip" title="Lihat" class="text-warning"><i class="material-icons">pageview</i></a> &nbsp;

                                                    <a href="<?php echo site_url('admin/no_manual/edit/' . $row['no_manual_id']) ?>" rel="tooltip" title="Edit" class="text-success"><i class="material-icons">mode_edit</i></a> &nbsp;

                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                            endforeach;
                                        } else {
                                            ?>
                                            <tr id="row">
                                                <td colspan="5" align="center">Data Kosong</td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


