<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-header-icon" data-background-color="blue">
						<i class="material-icons">assignment</i>
					</div>
					<div class="card-content">
						<h4 class="card-title">Kategori
							<a href="<?php echo site_url('admin/category/add') ?>" class="btn btn-info btn-xs pull-right"><i class="material-icons">add_box</i> Tambah</a>
							<p class="category">List</p></h4>

							<div class="table-responsive">
								<table class="table table-hover">
									<thead class="text-info">
										<th>No</th>
										<th>Nama Kategori</th>
										<th>Aksi</th>
									</thead>
									<tbody>
										<?php
										if (!empty($category)) {
											$i = 1;
											foreach ($category as $row):
												?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row['category_name']; ?></td>
												<td>
													<a href="<?php echo site_url('admin/category/edit/' . $row['category_id']) ?>" rel="tooltip" title="Edit" class="text-success view-pdf"><i class="material-icons">mode_edit</i></a> &nbsp;

													</td>
												</tr>
												<?php
												$i++;
												endforeach;
											} else {
												?>
												<tr id="row">
													<td colspan="3" align="center">Data Kosong</td>
												</tr>
												<?php } ?>
											</tbody>
										</table>	
									</div>
								</div>
							</div>
							<div>
								<?php echo $this->pagination->create_links(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>

