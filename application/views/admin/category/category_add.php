<?php

if (isset($category)) {
	
	$NameValue = $category['category_name'];
	
} else {
	$NameValue = set_value('category_name');
}
?>

<div class="content">
	<div class="container-fluid">
		<?php echo form_open(current_url()); ?>
		<div class="row">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header card-header-icon" data-background-color="blue">
						<i class="material-icons">border_color</i>
					</div>
					<div class="card-content">
						<h4 class="card-title"><?php echo $operation ?></h4>
						<div class="card-content">

							<?php echo validation_errors(); ?>
							<?php if (isset($category)) { ?>
							<input type="hidden" name="category_id" value="<?php echo $category['category_id']; ?>">
							<?php } ?>

							<form>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group label-floating">
											<label class="control-label">Nama category</label>
											<input type="text" class="form-control" name="category_name" value="<?php echo $NameValue ?>">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card card-profile">
					
					<div class="content">
						
						<button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
						<a href="<?php echo site_url('admin/category'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
						<?php if (isset($category)): ?>
						<a href="#delModal" class="btn btn-flat btn-wd btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo form_close(); ?>
</div>
<?php if (isset($category)) { ?>
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="modal-title">Konfirmasi Penghapusan</h4>
			</div>
			<div class="modal-body">
				<p>Apakah anda yakin akan menghapus data ini ?</p>
			</div>
			<div class="modal-footer">
				<?php echo form_open('admin/category/delete/' . $category['category_id']); ?>
				<input type="hidden" name="delName" value="<?php echo $category['category_name']; ?>">
				<button type="button" class="btn btn-info btn-wd" data-dismiss="modal">Batal</button>
				<button type="submit" class="btn btn-danger btn-wd">Hapus</button>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>

