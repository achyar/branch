<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Cetak BPJS Kesehatan
            <small>List</small>
        </h1>
        
    </section><br>
    <form action="<?php echo site_url('admin/cetak/multiple'); ?>" method="post">&nbsp;&nbsp;&nbsp;
        <button data-toggle="tooltip" data-placement="top" title="Cetak" class="btn btn-sm btn-success" style="border-radius:10px 0px 10px 0px" name="action" value="printPdf" onclick="$('form').attr('target', '_blank');"><span class="fa fa-print"></span>&nbsp;Print</button>
        <button data-toggle="tooltip" data-placement="top" title="Remove daftar cetak" class="btn btn-sm btn-info" style="border-radius:10px 0px 10px 0px" name="action" value="uncheck"><span class="fa fa-close"></span>&nbsp;Daftar Cetak</button>  

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-init table-bordered table-striped jambo_table">
                            <thead class="gradient">
                                <tr>
                                    <th><input type="checkbox" id="selectall" value="checkbox" name="checkbox"></th>
                                    <th>NPP</th>
                                    <th>NIK KTP</th>
                                    <th>Nama</th>
                                    <th>Hub Keluarga</th>
                                    <th>Faskes</th>                        
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($bpjs > 0) {                                 
                                    foreach ($bpjs as $row):
                                        ?>
                                    <tr>  
                                        <td><input type="checkbox" class="checkbox" name="msg[]" value="<?php echo $row['bpjs_id']; ?>"></td>                                       
                                        <td><?php echo $row['bpjs_npp']; ?></td>
                                        <td><?php echo $row['bpjs_ktp']; ?></td>
                                        <td><?php echo $row['bpjs_name']; ?></td>
                                        <td><?php echo $row['bpjs_hub']; ?></td>
                                        <td><?php echo $row['bpjs_faskes']; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('admin/bpjs/uncetak/' . $row['bpjs_id']) ?>" data-toggle="tooltip" title="Remove" class="text-warning"><span class="fa fa-close"></span></a> &nbsp;

                                        </td>
                                    </tr>
                                    <?php
                                    
                                    endforeach;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>

<script>
    $(document).ready(function() {
        $("#selectall").change(function() {
            $(".checkbox").prop('checked', $(this).prop("checked"));
        });
    });
</script>