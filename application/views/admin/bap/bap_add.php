<?php
$this->load->view('admin/datepicker');
if (isset($bap)) {
   
    $TglK = $bap['bap_tgl_kejadian'];
    $TglB = $bap['bap_tgl_surat'];
    $Nama = $bap['bap_karyawan_nama'];
    $Nik = $bap['bap_karyawan_nik'];
    $Pro = $bap['bap_proses'];
    $Branch = $bap['branch_branch_id'];
    $Desc = $bap['desc_desc_id'];

} else {
    $TglK = set_value('bap_tgl_kejadian');
    $TglB = set_value('bap_tgl_surat');
    $Nama = set_value('bap_karyawan_nama');
    $Nik = set_value('bap_karyawan_nik');
    $Pro = set_value('bap_proses');
    $Branch = set_value('branch_id');
    $Desc = set_value('desc_id');
}
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rekap BAP
            <small><?php echo $operation; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Rekap BAP</li>
            <li class="active"><?php echo $operation; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo form_open_multipart(current_url()); ?>
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-9">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo validation_errors(); ?>
                        <?php if (isset($bap)) { ?>
                            <input type="hidden" name="bap_id" value="<?php echo $bap['bap_id']; ?>">
                            <?php } ?>

                                <div class="form-group">
                                    <label>NIK <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input name="bap_karyawan_nik" type="text" class="form-control" value="<?php echo $Nik ?>" placeholder="NIK">
                                </div>

                                <div class="form-group">
                                    <label>Nama Karyawan <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input name="bap_karyawan_nama" type="text" class="form-control" value="<?php echo $Nama ?>" placeholder="Nama">
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Kejadian <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input name="bap_tgl_kejadian" type="text" class="form-control datepicker" value="<?php echo $TglK ?>" placeholder="Tanggal Kejadian">
                                </div>

                                 <div class="form-group">
                                    <label>Tanggal BAP <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input name="bap_tgl_surat" type="text" class="form-control datepicker" value="<?php echo $TglB ?>" placeholder="Tanggal BAP">
                                </div>

                                <div ng-controller="descCtrl">
                                <div class="form-group">
                                    <label >Description *</label>
                                    <select name="desc_id" class="form-control autocomplete">
                                        <option value="">-- Pilih Description --</option>
                                        <option ng-repeat="desc in descs" ng-selected="desc_data.index == desc.desc_id" value="{{desc.desc_id}}">{{desc.desc_pilihan}}</option>
                                    </select>
                                    <a data-toggle="modal" href="#myModal"><span class="ion-plus-circled"></span> Tambah Description</a><br><br>
                                    <div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-dialog modal-sm"> 
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Tambah Description</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <label >Deskripsi *</label>
                                                        <input type="text" name="desc_pilihan" ng-model="descForm.desc_pilihan" placeholder="Deskripsi" class="form-control"><br>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                                        <button type="button" class="btn btn-primary" ng-disabled="!(!!descForm.desc_pilihan)" ng-click="submit(descForm)" type="button" data-dismiss="modal">Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="form-group">
                                <label >Pilih Branch *</label>
                                    <select name="branch_id" class="form-control autocomplete">
                                        <option value="">-- Pilih Branch --</option>
                                        <?php foreach ($branch as $row): ?>
                                            <option value="<?php echo $row['branch_id']; ?>" <?php echo ($Branch == $row['branch_id']) ? 'selected' : '' ?>><?php echo $row['branch_nama']; ?></option>
                                        <?php endforeach; ?>
                                    </select><br>
                                </div>
                            </div>
                            </div>

                            <p class="text-muted">*) Kolom wajib diisi.</p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box box-primary">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <button type="submit" class="btn btn-flat btn-block btn-success"><span class="fa fa-check"></span> Simpan</button>
                            <a href="<?php echo site_url('admin/bap'); ?>" class="btn btn-flat btn-block btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                            <?php if (isset($bap)): ?>
                                <a href="#delModal" class="btn btn-flat btn-block btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                            <?php endif; ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.row -->
        </section>
        <!-- /.content -->
        <?php if (isset($bap)) { ?>
            <div class="modal modal-danger fade" id="delModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title"><span class="fa fa-warning"></span> Konfirmasi penghapusan</h3>
                            </div>
                            <div class="modal-body">
                                <p>Apakah anda yakin akan menghapus data ini?</p>
                            </div>
                            <div class="modal-footer">
                                <?php echo form_open('admin/bap/delete/' . $bap['bap_id']); ?>
                                <input type="hidden" name="del_name" value="<?php echo $bap['bap_karyawan_nama']; ?>">
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Batal</button>
                                <button type="submit" class="btn btn-outline"><span class="fa fa-check"></span> Hapus</button>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <?php } ?>

            </div>


<script src="<?php echo media_url() ?>js/angular.min.js" type="text/javascript"></script>
<script>
    var comApp = angular.module("comApp", []);
    var SITEURL = "<?php echo site_url() ?>";

    comApp.controller('descCtrl', function($scope, $http) {
        $scope.descs = [];
        <?php if (isset($bap)): ?>
        $scope.desc_data = {index: <?php echo $bap['desc_desc_id']; ?>};
    <?php endif; ?>

    $scope.getDesc = function() {

        var url = SITEURL + 'api/get_desc/';
        $http.get(url).then(function(response) {
            $scope.descs = response.data;
        });

    };

    $scope.submit = function(bap) {
        var postData = $.param(bap);
        $.ajax({
            method: "POST",
            url: SITEURL + "admin/bap/desc/add",
            data: postData,
            success: function(data) {
                $scope.getDesc();
                $scope.descForm.desc_pilihan = '';
            }
        });
    };

    angular.element(document).ready(function() {
        $scope.getDesc();
    });

});

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#target').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputImage").change(function() {
        readURL(this);
    });


</script>