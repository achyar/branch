<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rekap BAP Nasional
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li class="pull-right"><a href="<?php echo site_url('admin/bap/add'); ?>" class="btn btn-info"><i class="fa fa-plus"></i> Tambah</a>
            <a class="btn btn-sm btn-default" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" ><span class="glyphicon glyphicon-align-justify"></span></a>
            </li>
        </ol>
    </section><br>

    <div class="collapse" id="collapseExample">
            <?php echo form_open(current_url(), array('method' => 'get')) ?>
            <div class="row">                
                <div class="col-md-1">
                    &nbsp;
                </div>
                <div class="col-md-3">
                    <input type="text" name="ds" placeholder="Tanggal Mulai" value="" class="form-control datepicker">
                </div>
                <div class="col-md-3">
                    <input type="text" name="de" placeholder="Tanggal Akhir" value="" class="form-control datepicker">
                </div>
                <div class="col-md-2">
                    <input type="submit" class="btn btn-success" value="Filter">
                
                <a data-toggle="tooltip" data-placement="top" title="Download xls" class="btn btn-sm btn-success" href="<?php echo site_url('admin/bap/export_excel' . '/?' . http_build_query($q)) ?>" ><span class="glyphicon glyphicon-print"></span></a></div>
            </div>
            <?php echo form_close() ?>
        </div>
        <?php echo validation_errors() ?>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-init table-bordered table-striped jambo_table">
                            <thead class="gradient">
                                <tr>
                                    <th>No</th> 
                                    <th>Branch</th>                                   
                                    <th>Tanggal BAP</th>
                                    <th>NIK</th>
                                    <th>Nama</th>
                                    <th>Deskripsi</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($bap > 0) {
                                    $i = 1;
                                    foreach ($bap as $row):
                                        ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['branch_nama']; ?></td>                                        
                                        <td><?php echo pretty_date($row['bap_tgl_surat'],'d F Y',false) ?></td>
                                        <td><?php echo $row['bap_karyawan_nik']; ?></td>
                                        <td><?php echo $row['bap_karyawan_nama']; ?></td>
                                        <td><?php echo $row['desc_pilihan']; ?></td>
                                        <td><?php echo ($row['bap_proses'] == NULL ) ? 'Pending' : $row['bap_proses'] ?></td>
                                        <td>

                                            <a href="<?php echo site_url('admin/bap/edit/' . $row['bap_id']) ?>" data-toggle="tooltip" title="Sunting" class="text-success"><span class="fa fa-edit"></span></a> &nbsp;


                                            <a data-toggle="tooltip" data-placement="top" title="OK" class="btn btn-success btn-xs" href="<?php echo site_url('admin/bap/ok/' . $row['bap_id']); ?>" ><span class="fa fa-check"></span>OK</a>
                                            <a data-toggle="tooltip" data-placement="top" title="NOK" class="btn btn-primary btn-xs" href="<?php echo site_url('admin/bap/nok/' . $row['bap_id']); ?>" ><span class="fa fa-close"></span>NOK</a>

                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                    endforeach;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>