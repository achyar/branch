<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-plain">
					<div class="card-header card-header-icon" data-background-color="blue">
						<i class="material-icons">details</i>
					</div>
					<h4 class="card-title">Katalog</h4>
					<p class="category">Detail</p>
					<div class="col-md-10 col-sm-12 col-xs-12 pull-left">
						<br><br>
						<div class="row">
							<div class="col-md-2">
								<?php if(!empty($catalog['catalog_image'])){ ?>
								<img src="<?php echo upload_url($catalog['catalog_image']) ?>" class="img-responsive avatar">
								<?php }else{ ?>
								<img src="<?php echo base_url('media/img/missing-image.png') ?>" class="img-responsive avatar">
								<?php } ?>
							</div>
							<div class="card-content table-responsive">
								<table class="table table-hover">
									<tbody>
										<tr>
											<td>SKU</td>
											<td>:</td>
											<td><?php echo $catalog['catalog_sku'] ?></td>
										</tr>
										<tr>
											<td>Title</td>
											<td>:</td>
											<td><?php echo $catalog['catalog_name'] ?></td>
										</tr>
										<tr>
											<td>Bahan</td>
											<td>:</td>
											<td><?php echo $catalog['catalog_bhn'] ?></td>
										</tr>
										<tr>
											<td>Model</td>
											<td>:</td>
											<td><?php echo $catalog['catalog_model'] ?></td>
										</tr>
										<tr>
											<td>Berat</td>
											<td>:</td>
											<td><?php echo $catalog['catalog_weight'] ?> gr</td>
										</tr>
										<tr>
											<td>Bahan</td>
											<td>:</td>
											<td><?php echo $catalog['catalog_bhn'] ?></td>
										</tr>
										<tr>
											<td>Base Price</td>
											<td>:</td>
											<td><?php echo idr_format($catalog['catalog_price']) ?></td>
										</tr>
										<tr>
											<td>Varian</td>
											<td>:</td>
											<td><?php echo $catalog['variant_name'] ?></td>
										</tr>
										<tr>
											<td>Kategori</td>
											<td>:</td>
											<td><?php echo $catalog['category_name'] ?></td>
										</tr>
										<tr>
											<td>Supplier</td>
											<td>:</td>
											<td><?php echo $catalog['supplier_name'] ?></td>
										</tr>
										<tr>
											<td>Stock</td>
											<td>:</td>
											<td><?php echo $catalog['catalog_stock'] ?></td>
										</tr>
										<tr>
											<td>Tanggal Input</td>
											<td>:</td>
											<td><?php echo pretty_date($catalog['catalog_input_date'],'d F Y',false) ?></td>
										</tr>
										<tr>
											<td>Tanggal Update</td>
											<td>:</td>
											<td><?php echo pretty_date($catalog['catalog_last_update'],'d F Y',false) ?></td>
										</tr>
										<tr>
											<td>User Input</td>
											<td>:</td>
											<td><?php echo $catalog['user_full_name'] ?></td>
										</tr>

									</tbody>
								</table>
							</div>
							<div class="col-md-12">
								<a href="<?php echo site_url('admin/catalog') ?>" class="btn btn-app btn-success">
									<i class="fa fa-arrow-circle-o-left"></i> Kembali
								</a>
								<a href="<?php echo site_url('admin/catalog/edit/' . $catalog['catalog_id']) ?>" class="btn btn-app btn-info">
									<i class="fa fa-edit"></i> Edit
								</a>
								<?php if ($this->session->userdata('catalog_id') != $catalog['catalog_id']) { ?>
								<a href="#delModal" data-toggle="modal" class="btn btn-app btn-danger">
									<i class="fa fa-trash"></i> Hapus
								</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<h4>Gambar</h4>
				<hr>
				<div class="row">
					<?php $no = 0 ?>
					<?php foreach ($image as $key): ?>

						<div class="col-md-3 imgs">

							<img src="<?php echo upload_url($key['catalog_image_path']) ?>" class="img img-responsive">
							<div class="img-info">

								<div class="imgaction">
									<a class="zoomin">
										<i class="material-icons">zoom_in</i>
										<input type="hidden" value="<?php echo upload_url($key['catalog_image_path'])?>" >
									</a>
									<a onclick="return confirm('Anda yakin akan menghapus?')" href="<?php echo site_url('/admin/catalog/delete_image/' . $key['catalog_image_id'] . '/' . $key['catalog_catalog_id']) ?>">
										<i class="material-icons">delete</i>
									</a>
								</div>
							</div>
						</div>
						<?php $no++; ?>
					<?php endforeach ?>
				</div>
			</div>
			<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							<p></p>
						</div>
						<div class="modal-body img_centre">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="delModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><b>Konfirmasi Penghapusan</b></h4>
			</div>
			<div class="modal-body">
				<p>Data yang dipilih akan dihapus oleh sistem, apakah anda yakin?;</p>
			</div>
			<?php echo form_open('admin/catalog/delete/' . $catalog['catalog_id']); ?>
			<div class="modal-footer">
				<a><button style="float: right;margin-left: 10px" type="button" class="btn btn-info" data-dismiss="modal">Tidak</button></a>
				<input type="hidden" name="del_id" value="<?php echo $catalog['catalog_id'] ?>" />
				<input type="hidden" name="del_name" value="<?php echo $catalog['catalog_name'] ?>" />
				<button type="submit" class="btn btn-danger">Ya</button>
			</div>
			<?php echo form_close(); ?>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

