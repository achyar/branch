<?php
$this->load->view('admin/separator_rupiah');
$this->load->view('admin/separator_numeric');
if (isset($catalog)) {
	
	$SkuValue = $catalog['catalog_sku'];
	$NameValue = $catalog['catalog_name'];
	$BahanValue = $catalog['catalog_bhn'];
	$ModelValue = $catalog['catalog_model'];
	$WeightValue = $catalog['catalog_weight'];
	$PriceValue = $catalog['catalog_price'];
	$DescValue = $catalog['catalog_desc'];
	$ImageValue = $catalog['catalog_image'];
	$StockValue = $catalog['catalog_stock'];
	$VarValue = $catalog['variant_variant_id'];
	$SuppValue = $catalog['supplier_supplier_id'];
	$CatValue = $catalog['category_category_id'];
	
} else {
	$SkuValue = set_value('catalog_sku');
	$NameValue = set_value('catalog_name');
	$BahanValue = set_value('catalog_bhn');
	$ModelValue = set_value('catalog_model');
	$WeightValue = set_value('catalog_weight');
	$PriceValue = set_value('catalog_price');
	$DescValue = set_value('catalog_desc');
	$ImageValue = set_value('catalog_image');
	$StockValue = set_value('catalog_stock');
	$VarValue = set_value('variant_id');
	$SuppValue = set_value('supplier_id');
	$CatValue = set_value('category_id');
	
}
?>

<div class="content" >
	<div class="container-fluid">
		<?php echo form_open_multipart(current_url()); ?>
		<div class="row">
			<div class="col-md-8">
				<div class="card">
					<div class="card-header card-header-icon" data-background-color="blue">
						<i class="material-icons">border_color</i>
					</div>
					<div class="card-content">
						<h4 class="card-title"><?php echo $operation ?></h4>
						<div class="card-content">

							<?php echo validation_errors(); ?>
							<?php if (isset($catalog)) { ?>
							<input type="hidden" name="catalog_id" value="<?php echo $catalog['catalog_id']; ?>">
							<?php } ?>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">SKU</label>
										<input type="text" class="form-control" name="catalog_sku" value="<?php echo $SkuValue ?>">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Title</label>
										<input type="text" class="form-control" name="catalog_name" value="<?php echo $NameValue ?>">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Bahan</label>
										<input type="text" class="form-control" name="catalog_bhn" value="<?php echo $BahanValue ?>">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Model</label>
										<input type="text" class="form-control" name="catalog_model" value="<?php echo $ModelValue ?>">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Berat</label>
										<input type="number" class="form-control" name="catalog_weight" value="<?php echo $WeightValue ?>">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Base Price</label>
										<input type="text" id="asli" class="form-control" name="catalog_price" value="<?php echo $PriceValue ?>">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Stock</label>
										<input type="number" class="form-control" name="catalog_stock" value="<?php echo $StockValue ?>">
									</div>
								</div>
							</div>

							

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label>Kategori</label>
										<select name="category_id" class="form-control">
											<option value="">-- Pilih Kategori --</option>
											<?php foreach ($category as $row): ?>
												<option value="<?php echo $row['category_id']; ?>" <?php echo ($CatValue == $row['category_id']) ? 'selected' : '' ?>><?php echo $row['category_name']; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label>Supplier</label>
										<select name="supplier_id" class="form-control">
											<option value="">-- Pilih Supplier --</option>
											<?php foreach ($supplier as $row): ?>
												<option value="<?php echo $row['supplier_id']; ?>" <?php echo ($SuppValue == $row['supplier_id']) ? 'selected' : '' ?>><?php echo $row['supplier_name']; ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
							</div>

							<div ng-controller="VarCtrl">
								<div class="form-group">
									<label >Variant *</label>
									<select name="variant_id" class="form-control" class="selectpicker" data-style="select-with-transition" title="--- Pilihan ---">>
										<option ng-repeat="variant in variants" ng-selected="variant_data.index == variant.variant_id" value="{{variant.variant_id}}">{{variant.variant_name}}</option>
									</select>
									<a data-toggle="modal" href="#myModal"><span class="ion-plus-circled"></span> Tambah Variant</a><br><br>
									<div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document"> 
											<div class="modal-dialog modal-sm"> 
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
														<h4 class="modal-title" id="myModalLabel">Tambah Variant</h4>
													</div>
													<div class="modal-body form-group label-floating">
														<label>Varian</label>
														<input type="text" name="variant_name" ng-model="variantForm.variant_name" class="form-control"><br>
													</div>
													<div class="modal-footer">
														<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
														<button type="button" class="btn btn-primary" ng-disabled="!(!!variantForm.variant_name)" ng-click="submit(variantForm)" type="button" data-dismiss="modal">Simpan</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<div class="form-group label-floating">
										<label class="control-label">Deskripsi</label>
										<textarea class="form-control" rows="5" name="catalog_desc"><?php echo $DescValue ?></textarea>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="card card-profile">
						<div class="content">
							<?php if (isset($catalog)): ?>
								<?php foreach ($image as $key): ?>
									<a target="_blank" href="<?php echo upload_url($key['catalog_image_path']) ?>"><?php echo $key['catalog_image_path'] ?></a>
									<br>
								<?php endforeach ?>

							<?php endif ?>
							<hr>
							<label>Upload Gambar</label>
							<div id="p_upload">
								<p>
									<input type="file" name="catalogImage[]">
									<br>
								</p>
							</div>
							<a href="#" id="addUpload"><span class="fa fa-plus"></span> Tambah gambar lainnya</a>
							<br>
							<button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
							<a href="<?php echo site_url('admin/catalog'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
							<?php if (isset($catalog)): ?>
								<a href="#delModal" class="btn btn-flat btn-wd btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php echo form_close(); ?>
	</div>
	<?php if (isset($catalog)) { ?>
	<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						<i class="material-icons">clear</i>
					</button>
					<h4 class="modal-title">Konfirmasi Penghapusan</h4>
				</div>
				<div class="modal-body">
					<p>Apakah anda yakin akan menghapus data ini ?</p>
				</div>
				<div class="modal-footer">
					<?php echo form_open('admin/catalog/delete/' . $catalog['catalog_id']); ?>
					<input type="hidden" name="delName" value="<?php echo $catalog['catalog_name']; ?>">
					<button type="button" class="btn btn-info btn-wd" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-danger btn-wd">Hapus</button>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>

<script>
	var comApp = angular.module("comApp", []);
	var SITEURL = "<?php echo site_url() ?>";

	comApp.controller('VarCtrl', function($scope, $http) {
		$scope.variants = [];
		<?php if (isset($catalog)): ?>
		$scope.variant_data = {index: <?php echo $catalog['variant_variant_id']; ?>};
	<?php endif; ?>

	$scope.getVariant = function() {

		var url = SITEURL + '/api/get_variant/';
		$http.get(url).then(function(response) {
			$scope.variants = response.data;
		});

	}; 

	$scope.submit = function(catalog) {
		var postData = $.param(catalog);
		$.ajax({
			method: "POST",
			url: SITEURL + "/admin/catalog/variant/add",
			data: postData,
			success: function(data) {
				$scope.getVariant();
				$scope.variantForm.variant_name = '';
			}
		});
	};

	angular.element(document).ready(function() {
		$scope.getVariant();
	});

});

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#target').attr('src', e.target.result);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}

	$("#inputImage").change(function() {
		readURL(this);
	});

</script>

<script>
	$(function() {
		var divUpload = $('#p_upload');
		var i = $('#p_upload p').size() + 1;

		$("#addUpload").click(function() {
			$('<p>' +
				'<input type="file" name="catalogImage[]" multiple="">' +
				'<a href="#" class="remUpload">Hapus</a>' +
				'<br>' +
				'</p>').appendTo(divUpload);

			i++;
			return false;
		});

		$(document).on("click", ".remUpload", function() {
			if (i > 2) {
				$(this).parents('p').remove();
				i--;
			}
			return false;
		});
	});
</script>