<div class="content">
	<div class="container-fluid">
		<div class="collapse" id="collapseFilter">
			<?php echo form_open(current_url(), array('method' => 'get')) ?> <br>
			<div class="row">                
				<div class="col-md-2">
					<input autofocus type="text" name="n" placeholder="SKU" value="" class="form-control">
				</div>  
				<div class="col-md-2">
					<input type="text" name="k" placeholder="Title" value="" class="form-control">
				</div>               
				<input type="submit" class="btn btn-info" value="Cari">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-header-icon" data-background-color="blue">
						<i class="material-icons">assignment</i>
					</div>
					<div class="card-content">
						<h4 class="card-title">Item Stock
							<a href="<?php echo site_url('admin/catalog/add') ?>" class="btn btn-info btn-xs pull-right"><i class="material-icons">add_box</i> Tambah</a>
							<a href="#collapseFilter" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseFilter"><span class="fa fa-search"></span></a>
							<p class="category">List</p></h4>
							<div class="table-responsive">
								<table class="table table-hover">
									<thead class="text-info">
										<th>No</th>
										<th>SKU</th>
										<th>Title</th>
										<th>Model</th>
										<th>Varian</th>
										<th>Base Price</th>
										<th>Aksi</th>
									</thead>
									<tbody>
										<?php
										if (!empty($catalog)) {
											$i = 1;
											foreach ($catalog as $row):
												?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo $row['catalog_sku']; ?></td>
												<td><?php echo $row['catalog_name']; ?></td>
												<td><?php echo $row['catalog_model']; ?></td>
												<td><?php echo $row['variant_name']; ?></td>
												<td><?php echo idr_format($row['catalog_price']) ?></td>
												<td>
													<a href="<?php echo site_url('admin/catalog/detail/' . $row['catalog_id']) ?>" rel="tooltip" title="Lihat" class="text-warning"><i class="material-icons">pageview</i></a> &nbsp;

													<a href="<?php echo site_url('admin/catalog/edit/' . $row['catalog_id']) ?>" rel="tooltip" title="Edit" class="text-success view-pdf"><i class="material-icons">mode_edit</i></a> &nbsp;

												</td>
											</tr>
											<?php
											$i++;
											endforeach;
										} else {
											?>
											<tr id="row">
												<td colspan="10" align="center">Data Kosong</td>
											</tr>
											<?php } ?>
										</tbody>
									</table>	
								</div>
							</div>
						</div>
						<div>
							<?php echo $this->pagination->create_links(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

