<div class="wrapper">

    <div class="sidebar" data-active-color="red" data-background-color="black" data-image="<?php echo media_url() ?>img/sidebar.jpg">

        <div class="logo">
            <img src="<?php echo media_url() ?>img/alfamart.png" height="60px" width="auto" style="padding-left: 50px; padding-right: 50px; padding-bottom: 5px; padding-top: 5px">
        </div>

        <div class="logo logo-mini">
            <center><img src="<?php echo media_url() ?>ico/favicon.png" height="50px" width="50px" style="padding-left: 5px; padding-right: 5px; padding-bottom: 5px; padding-top: 5px"></center>
        </div>

        <div class="sidebar-wrapper">
            <div class="user">
                <div class="photo">
                    <?php if (!empty($this->session->userdata('user_image'))) { ?>
                    <img src="<?php echo upload_url().'/users/'.$this->session->userdata('user_image'); ?>">
                    <?php } else { ?>
                    <img src="<?php echo media_url('img/photo.jpg') ?>">
                    <?php } ?>
                </div>

                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                        <?php echo ucfirst($this->session->userdata('ufullname')); ?>
                        <b class="caret"></b>
                    </a>
                    <div class="collapse" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="<?php echo site_url('admin/profile') ?>">My Profile</a>
                            </li>
                            <li>
                                <a href="#">Setting</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <ul class="nav">
                <li class="<?php echo ($this->uri->segment(2) == 'dashboard' OR $this->uri->segment(2) == NULL) ? 'active' : '' ?>">
                   <a href="<?php echo site_url('admin') ?>">
                      <i class="material-icons">dashboard</i>
                      <p>Dashboard</p>
                  </a>
              </li>

              <li class="<?php echo ($this->uri->segment(2) == 'agenda') ? 'active' : '' ?> ">
                <a href="<?php echo site_url('admin/agenda')?>">
                    <i class="material-icons">date_range</i>
                    <p>Agenda</p>
                </a>
            </li>

              <li class="<?php echo ($this->uri->segment(2) == 'sket') ? 'active' : '' ?> ">
                <a href="<?php echo site_url('admin/sket')?>">
                    <i class="material-icons">mail</i>
                    <p>Surat Keterangan</p>
                </a>
            </li>

            <li class="<?php echo ($this->uri->segment(2) == 'spb') ? 'active' : '' ?> ">
                <a href="<?php echo site_url('admin/spb')?>">
                    <i class="material-icons">credit_card</i>
                    <p>Surat Pengantar Bank</p>
                </a>
            </li>

              <li class="<?php echo ($this->uri->segment(2) == 'bpjstk' OR $this->uri->segment(2) == 'disnaker') ? 'active' : '' ?> ">
                <a data-toggle="collapse" href="#bpjstk">
                    <i class="material-icons">work</i>
                    <p>BPJS TK
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse <?php echo ($this->uri->segment(2) == 'bpjstk'  OR $this->uri->segment(2) == 'disnaker') ? 'in' : '' ?> " id="bpjstk">
                    <ul class="nav">
                        <li class="<?php echo ($this->uri->segment(2) == 'bpjstk') ? 'active' : '' ?>">
                            <a href="<?php echo site_url('admin/bpjstk') ?>">Surat BPJS TK</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == 'disnaker') ? 'active' : '' ?>">
                            <a href="<?php echo site_url('admin/disnaker') ?>">Surat Disnaker</a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="<?php echo ($this->uri->segment(2) == 'no_manual') ? 'active' : '' ?> ">
                <a href="<?php echo site_url('admin/no_manual')?>">
                    <i class="material-icons">description</i>
                    <p>No Surat Manual</p>
                </a>
            </li>

              <li class="<?php echo ($this->uri->segment(2) == 'employee') ? 'active' : '' ?> ">
                <a data-toggle="collapse" href="#pagesExamples">
                    <i class="material-icons">person</i>
                    <p>Karyawan
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse <?php echo ($this->uri->segment(2) == 'employee') ? 'in' : '' ?> " id="pagesExamples">
                    <ul class="nav">
                        <li class="<?php echo ($this->uri->segment(2) == 'employee' AND $this->uri->segment(3) != 'import') ? 'active' : '' ?>">
                            <a href="<?php echo site_url('admin/employee') ?>">List Karyawan</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == 'employee' AND $this->uri->segment(3) == 'import') ? 'active' : '' ?>">
                            <a href="<?php echo site_url('admin/employee/import') ?>">Upload Data Karyawan</a>
                        </li>
                    </ul>
                </div>
            </li>
            <?php if($this->session->userdata('uroleid') == SPECIAL_USER){ ?>

            <li class="<?php echo ($this->uri->segment(2) == 'users') ? 'active' : '' ?> ">
                <a data-toggle="collapse" href="#users">
                    <i class="material-icons">person</i>
                    <p>Management Users
                        <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse <?php echo ($this->uri->segment(2) == 'users') ? 'in' : '' ?> " id="users">
                    <ul class="nav">
                        <li class="<?php echo ($this->uri->segment(2) == 'users' AND $this->uri->segment(3) != 'add') ? 'active' : '' ?>">
                            <a href="<?php echo site_url('admin/users') ?>">List Users</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(2) == 'users' AND $this->uri->segment(3) == 'add') ? 'active' : '' ?>">
                            <a href="<?php echo site_url('admin/users/add') ?>">Tambah Users</a>
                        </li>
                    </ul>
                </div>
            </li>
            <?php } ?>

            <li class="<?php echo ($this->uri->segment(2) == 'setting') ? 'active' : '' ?> ">
                <a href="<?php echo site_url('admin/setting')?>">
                    <i class="material-icons">settings</i>
                    <p>Pengaturan</p>
                </a>
            </li>
            <li class="<?php echo ($this->uri->segment(2) == 'logs') ? 'active' : '' ?> ">
                <a href="<?php echo site_url('admin/logs')?>">
                    <i class="material-icons">history</i>
                    <p>History</p>
                </a>
            </li>
        </ul>
    </div>
</div>
