<?php

if (isset($spb)) {
    $Date = $spb['spb_date'];
    $inputName1 = $spb['spb_name1'];
    $inputName2 = $spb['spb_name2'];
    $inputName3 = $spb['spb_name3'];
    $inputName4 = $spb['spb_name4'];
    $inputName5 = $spb['spb_name5'];
    $inputName6 = $spb['spb_name6'];
    $inputName7 = $spb['spb_name7'];
    $inputName8 = $spb['spb_name8'];
    $inputName9 = $spb['spb_name9'];
    $inputName10 = $spb['spb_name10'];
    $inputNik1 = $spb['spb_nik1'];
    $inputNik2 = $spb['spb_nik2'];
    $inputNik3 = $spb['spb_nik3'];
    $inputNik4 = $spb['spb_nik4'];
    $inputNik5 = $spb['spb_nik5'];
    $inputNik6 = $spb['spb_nik6'];
    $inputNik7 = $spb['spb_nik7'];
    $inputNik8 = $spb['spb_nik8'];
    $inputNik9 = $spb['spb_nik9'];
    $inputNik10 = $spb['spb_nik10'];
    $inputBank = $spb['bank_bank_id'];

} else {

    $Date = set_value('spb_date');
    $inputName1 = set_value('spb_name1');
    $inputName2 = set_value('spb_name2');
    $inputName3 = set_value('spb_name3');
    $inputName4 = set_value('spb_name4');
    $inputName5 = set_value('spb_name5');
    $inputName6 = set_value('spb_name6');
    $inputName7 = set_value('spb_name7');
    $inputName8 = set_value('spb_name8');
    $inputName9 = set_value('spb_name9');
    $inputName10 = set_value('spb_name10');
    $inputNik1 = set_value('spb_nik1');
    $inputNik2 = set_value('spb_nik2');
    $inputNik3 = set_value('spb_nik3');
    $inputNik4 = set_value('spb_nik4');
    $inputNik5 = set_value('spb_nik5');
    $inputNik6 = set_value('spb_nik6');
    $inputNik7 = set_value('spb_nik7');
    $inputNik8 = set_value('spb_nik8');
    $inputNik9 = set_value('spb_nik9');
    $inputNik10 = set_value('spb_nik10');
    $inputBank = set_value('bank_id');

}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">border_color</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $operation ?></h4>
                        <div class="card-content">

                            <?php echo validation_errors(); ?>
                            <?php if (isset($spb)) { ?>
                            <input type="hidden" name="spb_id" value="<?php echo $spb['spb_id']; ?>">
                            <input type="hidden" name="spb_number" value="<?php echo $spb['spb_number']; ?>">
                            <?php } ?>

                            <?php echo form_open_multipart(current_url()); ?>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Untuk Tanggal</label>
                                        <input class="form-control datepicker" name="spb_date" value="<?php echo $Date ?>">
                                    </div>
                                </div>
                            </div>


                            <div ng-controller="bankCtrl">
                                <div class="form-group">
                                    <label>Bank *</label>
                                    <select name="bank_id" class="form-control autocomplete">
                                        <option value="">-- Pilih Bank --</option>
                                        <option ng-repeat="bank in banks" ng-selected="bank_data.index == bank.bank_id" value="{{bank.bank_id}}">{{bank.bank_name}}</option>
                                    </select>
                                    <a data-toggle="modal" href="#myModal"><span class="btn btn-success btn-xs">Tambah Bank</span></a><br><br>
                                    <div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Tambah Bank</h4>
                                                    </div>
                                                    <div class="modal-body form-group label-floating">
                                                        <label>Nama Bank *</label>
                                                        <input type="text" name="bank_name" ng-model="bankForm.bank_name" class="form-control"><br>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                        <button type="button" class="btn btn-info" ng-disabled="!(!!bankForm.bank_name)" ng-click="submit(bankForm)" type="button" data-dismiss="modal">Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama 1</label>
                                            <input class="form-control" name="spb_name1" value="<?php echo $inputName1 ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No KTP 1</label>
                                            <input class="form-control" name="spb_nik1" value="<?php echo $inputNik1 ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama 2</label>
                                            <input class="form-control" name="spb_name2" value="<?php echo $inputName2 ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No KTP 2</label>
                                            <input class="form-control" name="spb_nik2" value="<?php echo $inputNik2 ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama 3</label>
                                            <input class="form-control" name="spb_name3" value="<?php echo $inputName3 ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No KTP 3</label>
                                            <input class="form-control" name="spb_nik3" value="<?php echo $inputNik3 ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama 4</label>
                                            <input class="form-control" name="spb_name4" value="<?php echo $inputName4 ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No KTP 4</label>
                                            <input class="form-control" name="spb_nik4" value="<?php echo $inputNik4 ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama 5</label>
                                            <input class="form-control" name="spb_name5" value="<?php echo $inputName5 ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No KTP 5</label>
                                            <input class="form-control" name="spb_nik5" value="<?php echo $inputNik5 ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama 6</label>
                                            <input class="form-control" name="spb_name6" value="<?php echo $inputName6 ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No KTP 6</label>
                                            <input class="form-control" name="spb_nik6" value="<?php echo $inputNik6 ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama 7</label>
                                            <input class="form-control" name="spb_name7" value="<?php echo $inputName7 ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No KTP 7</label>
                                            <input class="form-control" name="spb_nik7" value="<?php echo $inputNik7 ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama 8</label>
                                            <input class="form-control" name="spb_name8" value="<?php echo $inputName8 ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No KTP 8</label>
                                            <input class="form-control" name="spb_nik8" value="<?php echo $inputNik8 ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama 9</label>
                                            <input class="form-control" name="spb_name9" value="<?php echo $inputName9 ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No KTP 9</label>
                                            <input class="form-control" name="spb_nik9" value="<?php echo $inputNik9 ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama 10</label>
                                            <input class="form-control" name="spb_name10" value="<?php echo $inputName10 ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No KTP 10</label>
                                            <input class="form-control" name="spb_nik10" value="<?php echo $inputNik10 ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-profile">
                    <div class="content">
                        <button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
                        <a href="<?php echo site_url('admin/spb'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                        <?php if (isset($spb)): ?>
                            <a href="#delModal" class="btn btn-flat btn-wd btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (isset($spb)) { ?>
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Konfirmasi Penghapusan</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin akan menghapus data ini ?</p>
            </div>
            <div class="modal-footer">
                <?php echo form_open('admin/spb/delete/' . $spb['spb_id']); ?>
                <input type="hidden" name="delName" value="<?php echo $spb['spb_employee_name']; ?>">
                <button type="button" class="btn btn-info btn-wd" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger btn-wd">Hapus</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- /.row -->

<script>
    var comApp = angular.module("comApp", []);
    var SITEURL = "<?php echo site_url() ?>";

    comApp.controller('bankCtrl', function($scope, $http) {
        $scope.banks = [];
        <?php if (isset($spb)): ?>
        $scope.bank_data = {index: <?php echo $spb['bank_bank_id']; ?>};
    <?php endif; ?>

    $scope.getBank = function() {

        var url = SITEURL + 'api/get_bank/';
        $http.get(url).then(function(response) {
            $scope.banks = response.data;
        });

    };

    $scope.submit = function(spb) {
        var postData = $.param(spb);
        $.ajax({
            method: "POST",
            url: SITEURL + "admin/spb/bank/add",
            data: postData,
            success: function(data) {
                $scope.getBank();
                $scope.bankForm.bank_name = '';
            }
        });
    };

    angular.element(document).ready(function() {
        $scope.getBank();
    });

});

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#target').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputImage").change(function() {
        readURL(this);
    });


</script>

<script type="text/javascript">
    function validate(evt) {
      var theEvent = evt || window.event;
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode( key );
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
