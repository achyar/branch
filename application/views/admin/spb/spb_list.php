<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">credit_card</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Surat Pengatar Bank
                            <a href="<?php echo site_url('admin/spb/add') ?>" class="btn btn-danger btn-xs pull-right"><i class="material-icons">add_box</i> Tambah</a>
                            <p class="category">List</p></h4>

                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="text-red">
                                        <th>No</th>
                                        <th>Nomor Surat</th>
                                        <th>Tanggal</th>
                                        <th>Aksi</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($spb)) {
                                            $i = 1;
                                            foreach ($spb as $row):
                                                ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $row['spb_number']; ?></td>
                                                <td><?php echo pretty_date($row['spb_date'],'d F Y',false) ?></td>
                                                <td>
                                                    <a href="<?php echo site_url('admin/spb/view/' . $row['spb_id']) ?>" rel="tooltip" title="Lihat" class="text-warning"><i class="material-icons">pageview</i></a> &nbsp;

                                                    <a href="<?php echo site_url('admin/spb/edit/' . $row['spb_id']) ?>" rel="tooltip" title="Edit" class="text-success"><i class="material-icons">mode_edit</i></a> &nbsp;

                                                    <a href="<?php echo site_url('admin/spb/printPdf/' . $row['spb_id']) ?>" rel="tooltip" title="Print" class="text-info view-pdf"><i class="material-icons">print</i></a> &nbsp;

                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                            endforeach;
                                        } else {
                                            ?>
                                            <tr id="row">
                                                <td colspan="4" align="center">Data Kosong</td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div>
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            (function(a){a.createModal=function(b){defaults={title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false};var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";html='<div class="modal fade" id="myModal">';html+='<div class="modal-dialog">';html+='<div class="modal-content">';html+='<div class="modal-header">';html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';if(b.title.length>0){html+='<h4 class="modal-title">'+b.title+"</h4>"}html+="</div>";html+='<div class="modal-body" '+c+">";html+=b.message;html+="</div>";html+='<div class="modal-footer">';if(b.closeButton===true){html+='<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>'}html+="</div>";html+="</div>";html+="</div>";html+="</div>";a("body").prepend(html);a("#myModal").modal().on("hidden.bs.modal",function(){a(this).remove()})}})(jQuery);

/*
* Here is how you use it
*/
$(function(){    
    $('.view-pdf').on('click',function(){
        var pdf_link = $(this).attr('href');
        //var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>'
        //var iframe = '<object data="'+pdf_link+'" type="application/pdf"><embed src="'+pdf_link+'" type="application/pdf" /></object>'        
        var iframe = '<object type="application/pdf" data="'+pdf_link+'" width="100%" height="500">No Support</object>'
        $.createModal({
            title:'Surat Keterangan',
            message: iframe,
            closeButton:true,
            scrollable:false
        });
        return false;        
    });    
})
</script>
