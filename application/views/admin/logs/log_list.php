<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header card-header-icon" data-background-color="rose">
						<i class="material-icons">assignment</i>
					</div>
					<div class="card-content">
						<h4 class="card-title">Logs
							<p class="category">List</p></h4>

							<div class="table-responsive">
								<table class="table table-hover">
									<thead class="text-rose">
										<th>No</th>
										<th>Tanggal</th>
										<th>Modul</th>
										<th>Aksi</th>
										<th>Info</th>
										<th>Penulis</th>
									</thead>
									<tbody>
										<?php
										if (!empty($logs)) {
											$i = 1;
											foreach ($logs as $row):
												?>
											<tr>
												<td><?php echo $i; ?></td>
												<td><?php echo pretty_date($row['log_date'],'d M Y h:m:s',false) ?></td>
												<td><?php echo $row['log_module']; ?></td>
												<td><?php echo $row['log_action']; ?></td>
												<td><?php echo $row['log_info']; ?></td>
												<td><?php echo $row['user_full_name']; ?></td>	
											</tr>
											<?php
											$i++;
											endforeach;
										} else {
											?>
											<tr id="row">
												<td colspan="6" align="center">Data Kosong</td>
											</tr>
											<?php } ?>
										</tbody>
									</table>	
								</div>
							</div>
						</div>
						<div>
							<?php echo $this->pagination->create_links(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

