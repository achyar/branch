<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BPJS Kesehatan
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
        <?php if ($this->session->userdata('uroleid') == SPECIAL_USER) { ?>
        <li class="pull-right"><a href="<?php echo site_url('admin/bpjs/delete'); ?>" onclick="return confirm('Apakah Anda akan menghapus semua data BPJS?')" class="btn btn-danger"> Hapus Semua</a></li>  
         <?php } ?>
            <li class="pull-right"><a href="<?php echo site_url('admin/bpjs/add'); ?>" class="btn btn-info"><i class="fa fa-plus"></i> Tambah</a></li>
        </ol>
    </section><br>
    <form action="<?php echo site_url('admin/bpjs/multiple'); ?>" method="post">&nbsp;&nbsp;&nbsp;
        <button data-toggle="tooltip" data-placement="top" title="Cetak" class="btn btn-sm btn-success" style="border-radius:10px 0px 10px 0px" name="action" value="printPdf" onclick="$('form').attr('target', '_blank');"><span class="fa fa-print"></span>&nbsp;Print</button>
        <button data-toggle="tooltip" data-placement="top" title="Tambah ke daftar cetak" class="btn btn-sm btn-info" style="border-radius:10px 0px 10px 0px" name="action" value="cetak"><span class="fa fa-check"></span>&nbsp;Daftar Cetak</button>  

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-init table-bordered table-striped jambo_table">
                            <thead class="gradient">
                                <tr>
                                    <th><input type="checkbox" id="selectall" value="checkbox" name="checkbox"></th>
                                    <th>NPP</th>
                                    <th>NIK KTP</th>
                                    <th>Nama</th>
                                    <th>Hub Keluarga</th>
                                    <th>Faskes</th>                        
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($bpjs > 0) {                                 
                                    foreach ($bpjs as $row):
                                        ?>
                                    <tr>  
                                        <td><input type="checkbox" class="checkbox" name="msg[]" value="<?php echo $row['bpjs_id']; ?>"></td>                                       
                                        <td><?php echo $row['bpjs_npp']; ?></td>
                                        <td><?php echo $row['bpjs_ktp']; ?></td>
                                        <td><?php echo $row['bpjs_name']; ?></td>
                                        <td><?php echo $row['bpjs_hub']; ?></td>
                                        <td><?php echo $row['bpjs_faskes']; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('admin/bpjs/cetak/' . $row['bpjs_id']) ?>" data-toggle="tooltip" title="Daftar Cetak" class="text-warning"><span class="fa fa-check"></span></a> &nbsp;

                                            <a href="<?php echo site_url('admin/bpjs/edit/' . $row['bpjs_id']) ?>" data-toggle="tooltip" title="Sunting" class="text-success"><span class="fa fa-edit"></span></a> &nbsp;

                                            <a href="<?php echo site_url('admin/bpjs/printPdf/' . $row['bpjs_id']) ?>" data-toggle="tooltip" title="Print" class="text-success view-pdf"><span class="fa fa-print"></span></a> &nbsp;

                                        </td>
                                    </tr>
                                    <?php
                                    
                                    endforeach;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">
    (function(a){a.createModal=function(b){defaults={title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false};var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";html='<div class="modal fade" id="myModal">';html+='<div class="modal-dialog">';html+='<div class="modal-content">';html+='<div class="modal-header">';html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';if(b.title.length>0){html+='<h4 class="modal-title">'+b.title+"</h4>"}html+="</div>";html+='<div class="modal-body" '+c+">";html+=b.message;html+="</div>";html+='<div class="modal-footer">';if(b.closeButton===true){html+='<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>'}html+="</div>";html+="</div>";html+="</div>";html+="</div>";a("body").prepend(html);a("#myModal").modal().on("hidden.bs.modal",function(){a(this).remove()})}})(jQuery);

/*
* Here is how you use it
*/
$(function(){    
    $('.view-pdf').on('click',function(){
        var pdf_link = $(this).attr('href');
        //var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>'
        //var iframe = '<object data="'+pdf_link+'" type="application/pdf"><embed src="'+pdf_link+'" type="application/pdf" /></object>'        
        var iframe = '<object type="application/pdf" data="'+pdf_link+'" width="100%" height="500">No Support</object>'
        $.createModal({
            title:'E-ID',
            message: iframe,
            closeButton:true,
            scrollable:false
        });
        return false;        
    });    
})
</script>

<script>
    $(document).ready(function() {
        $("#selectall").change(function() {
            $(".checkbox").prop('checked', $(this).prop("checked"));
        });
    });
</script>