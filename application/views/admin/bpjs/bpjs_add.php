<?php $this->load->view('admin/datepicker'); ?>

<?php
if (isset($bpjs)) {
    $inputNoka = $bpjs['bpjs_noka'];
    $inputKtp = $bpjs['bpjs_ktp'];
    $inputNpp = $bpjs['bpjs_npp'];    
    $inputName = $bpjs['bpjs_name'];
    $inputHub = $bpjs['bpjs_hub']; 
    $inputDate = $bpjs['bpjs_date'];
    $inputTmt = $bpjs['bpjs_tmt'];
    $inputFaskes = $bpjs['bpjs_faskes'];
    $inputKelas = $bpjs['bpjs_kelas'];
} else {    
    $inputNoka = set_value('bpjs_noka');
    $inputKtp = set_value('bpjs_ktp');
    $inputNpp = set_value('bpjs_npp');   
    $inputName = set_value('bpjs_name');
    $inputHub = set_value('bpjs_hub');
    $inputDate = set_value('bpjs_date');
    $inputTmt = set_value('bpjs_tmt');
    $inputFaskes = set_value('bpjs_faskes');
    $inputKelas = set_value('bpjs_kelas');
}
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BPJS Kesehatan
            <small><?php echo $operation; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li> BPJS Kesehatan</li>
            <li class="active"><?php echo $operation; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo form_open(current_url()); ?>
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-9">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo validation_errors(); ?>
                        <?php if (isset($bpjs)) { ?>
                            <input type="hidden" name="bpjs_id" value="<?php echo $bpjs['bpjs_id']; ?>">
                            <?php } ?>

                            <div class="form-group">
                                <label>NOKA <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <input name="bpjs_noka" type="text" class="form-control" value="<?php echo $inputNoka ?>" placeholder="NOKA">
                            </div>

                            <div class="form-group">
                                <label>Nomor NIK <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <input name="bpjs_ktp" type="text" class="form-control" value="<?php echo $inputKtp ?>" placeholder="NIK">
                            </div>

                            <div class="form-group">
                                <label>NPP Karyawan <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <input name="bpjs_npp" type="text" class="form-control" value="<?php echo $inputNpp ?>" placeholder="NPP Karyawan">
                            </div>

                            <div class="form-group">
                                <label>Nama Karyawan <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <input name="bpjs_name" class="form-control" value="<?php echo $inputName ?>" placeholder="Nama">
                            </div>

                            <div class="form-group">
                                <label>PISA <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <select name="bpjs_hub" class="form-control">
                                    <option value="">--- Pilih PISA ---</option>
                                    <option value="PESERTA" <?php echo ($inputHub == 'PESERTA') ? 'selected' : '' ?>>PESERTA</option>
                                    <option value="ISTRI" <?php echo ($inputHub == 'ISTRI') ? 'selected' : '' ?>>ISTRI</option>
                                    <option value="ANAK" <?php echo ($inputHub == 'ANAK') ? 'selected' : '' ?>>ANAK</option>
                                    <option value="SUAMI" <?php echo ($inputHub == 'SUAMI') ? 'selected' : '' ?>>SUAMI</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Tanggal Lahir <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <input name="bpjs_date" id="bpjs_date" class="form-control" value="<?php echo $inputDate ?>" placeholder="Tanggal Lahir">
                            </div>

                            <div class="form-group">
                                <label>TMT <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <input name="bpjs_tmt" id="bpjs_tmt" type="text" class="form-control" value="<?php echo $inputTmt ?>" placeholder="TMT">
                            </div>

                            <div class="form-group">
                                <label>Faskes <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <input name="bpjs_faskes" type="text" class="form-control" value="<?php echo $inputFaskes ?>" placeholder="Faskes">
                            </div>

                            <div class="form-group">
                                <label>Kelas Rawat <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="bpjs_kelas" value="1" <?php echo ($inputKelas == '1') ? 'checked' : ''; ?>> Kelas I(SATU)
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="bpjs_kelas" value="2" <?php echo ($inputKelas == '2') ? 'checked' : ''; ?>> Kelas II(DUA)
                                    </label>
                                </div>
                            </div>

                            <p class="text-muted">*) Kolom wajib diisi.</p>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box box-primary">
                        <!-- /.box-header -->
                        <div class="box-body">
                            <button type="submit" class="btn btn-flat btn-block btn-success"><span class="fa fa-check"></span> Simpan</button>
                            <a href="<?php echo site_url('admin/bpjs'); ?>" class="btn btn-flat btn-block btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                            <?php if (isset($bpjs)): ?>
                                <a href="#delModal" class="btn btn-flat btn-block btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                            <?php endif; ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            <!-- /.row -->
        </section>
        <!-- /.content -->
        <?php if (isset($bpjs)) { ?>
            <div class="modal modal-danger fade" id="delModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                                <h3 class="modal-title"><span class="fa fa-warning"></span> Konfirmasi penghapusan</h3>
                            </div>
                            <div class="modal-body">
                                <p>Apakah anda yakin akan menghapus data ini?</p>
                            </div>
                            <div class="modal-footer">
                                <?php echo form_open('admin/bpjs/delete/' . $bpjs['bpjs_id']); ?>
                                <input type="hidden" name="delName" value="<?php echo $bpjs['bpjs_name']; ?>">
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Batal</button>
                                <button type="submit" class="btn btn-outline"><span class="fa fa-check"></span> Hapus</button>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <?php } ?>

            </div>

            <script type="text/javascript">
                $(function() {
                    $("#bpjs_date").inputmask("dd-mm-yyyy", {"placeholder": "DD-MM-YYYY"});
                    $("#bpjs_tmt").inputmask("dd-mm-yyyy", {"placeholder": "DD-MM-YYYY"});

                });
            </script>
