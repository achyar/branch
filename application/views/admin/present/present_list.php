<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Rekap Presensi
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <a class="btn btn-sm btn-default" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample" ><span class="glyphicon glyphicon-align-justify"></span></a>
            <a data-toggle="tooltip" data-placement="top" title="Download xls" class="btn btn-sm btn-success" href="<?php echo site_url('admin/present/export_excel' . '/?' . http_build_query($q)) ?>" ><span class="glyphicon glyphicon-print"></span></a>
        </li>
    </ol>
    <div class="collapse" id="collapseExample">
        <?php echo form_open(current_url(), array('method' => 'get')) ?>
        <div class="row">   
        &nbsp;&nbsp;            
            <div class="col-md-3">
            <input type="text" name="n" placeholder="NIPM" value="" class="form-control">
            </div>
            <div class="col-md-3">
                <input type="text" name="ds" placeholder="Dari Tanggal" value="" class="form-control datepicker">
            </div>
            <div class="col-md-3">
                <input type="text" name="de" placeholder="Sampai Tanggal" value="" class="form-control datepicker">
            </div>
            <div class="col-md-2">
                <input type="submit" class="btn btn-success" value="Filter">
            </div>
        </div>
        <?php echo form_close() ?>
    </div>
    <?php echo validation_errors() ?>
</section><br>

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-init table-bordered table-striped jambo_table">
                        <thead class="gradient">
                            <tr>
                                <th>No.</th>
                                <th>Tanggal</th>
                                <th>Nama</th>
                                <th>Masuk</th>
                                <th>Pulang</th>
                                <th>Kehadiran</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i= 1; foreach ($present as $row): ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo pretty_date($row['present_date'], 'l, d m Y', FALSE) ?></td>
                                <td><?php echo $row['prakerin_name'] ?></td>
                                <td><?php echo ($row['present_entry_time'] == NULL) ? '-' : $row['present_entry_time'] ?></td>
                                <td><?php echo ($row['present_out_time'] == NULL) ? '-' : $row['present_out_time'] ?></td>
                                <td><?php echo $row['present_desc'] ?></td>
                                <td><?php echo ($row['present_is_late'] == 1) ? 'TELAT' : '-' ?></td>
                            </tr>
                            <?php $i++; endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->

</section>
<!-- /.content -->
</div>