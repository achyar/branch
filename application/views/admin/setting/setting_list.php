<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">border_color</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Pengaturan</h4>
                        <div class="card-content">

                            <?php echo form_open_multipart(current_url()); ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nama Branch</label>
                                        <input type="text" name="setting_branch" value="<?php echo $setting_branch['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Inisial Branch</label>
                                        <input type="text" name="setting_initial" value="<?php echo $setting_initial['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Kode Branch</label>
                                        <input type="text" name="setting_unit" value="<?php echo $setting_unit['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Inisial DBM / BM</label>
                                        <input type="text" name="setting_initial_bm" value="<?php echo $setting_initial_bm['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Inisial PDM / GSM</label>
                                        <input type="text" name="setting_initial_pdm" value="<?php echo $setting_initial_pdm['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Alamat Branch</label>
                                        <input name="setting_address" type="text" value="<?php echo $setting_address['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">No. Tlp Kantor (hunting)</label>
                                        <input type="text" name="setting_pic" value="<?php echo $setting_pic['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Kota/Kab</label>
                                        <input type="text" name="setting_city" value="<?php echo $setting_city['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Nama PDM / GSM</label>
                                        <input type="text" name="setting_employe_name" value="<?php echo $setting_employe_name['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">NIK PDM / GSM</label>
                                        <input type="text" name="setting_employe_nik" value="<?php echo $setting_employe_nik['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label">Jabatan PDM / GSM</label>
                                        <input type="text" name="setting_employe_position" value="<?php echo $setting_employe_position['setting_value'] ?>" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <input type="submit" value="Simpan" class="btn btn-success pull-right">
                                </div>
                            </div>
                            <?php echo form_close() ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="card-profile">
                    <div class="content">
                    <div class="alert alert-danger">
                            Kolom tidak boleh kosong, Jika ingin di nonaktifkan silakan beri tanda ( - ) pada kolom yang tersedia.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
