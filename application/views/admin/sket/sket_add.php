<?php
// $this->load->view('admin/datepicker');
if (isset($sket)) {
    $sket['sket_id'];
    $Nomor = $sket['sket_number'];
    $Nik = $sket['sket_employee_nik'];
    $Nama = $sket['sket_employee_name'];
    $Jab = $sket['sket_employee_position'];
    $Tmpt = $sket['sket_employee_place'];
    $Tgl = $sket['sket_employee_birth_date'];
    $Addr = $sket['sket_employee_address'];
    $Ent = $sket['sket_employee_entry_date'];
    $Stat = $sket['sket_status'];
    $Ket = $sket['ket_ket_id'];
    
} else {
    $Nomor = set_value('sket_number');
    $Nik = set_value('employee_nik');
    $Nama = set_value('employee_name');
    $Jab = set_value('employee_position');
    $Tmpt = set_value('employee_place');
    $Tgl = set_value('employee_birth_date');
    $Addr = set_value('employee_address');
    $Ent = set_value('employee_entry_date');
    $Stat = set_value('sket_status');
    $Ket = set_value('ket_id');
}
?>

<div class="content">
    <div class="container-fluid"> 
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">border_color</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $operation ?></h4>
                        <div class="card-content">

                            <?php echo validation_errors(); ?>
                            <?php if (isset($sket)) { ?>
                            <input type="hidden" name="sket_id" value="<?php echo $sket['sket_id']; ?>">
                            <input type="hidden" name="sket_number" value="<?php echo $sket['sket_number']; ?>">
                            <?php } ?>

                            <?php echo form_open_multipart(current_url()); ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="">Nama Karyawan</label>
                                        <input name="employee_nik" id="field_id" type="hidden" class="form-control"  value="<?php echo $Nik ?>">
                                        <input name="employee_name" id="field_name" type="hidden" class="form-control"  value="<?php echo $Nama ?>">
                                        <input name="employee_position" id="field_pos" type="hidden" class="form-control"  value="<?php echo $Jab ?>">
                                        <input name="employee_address" id="field_add" type="hidden" class="form-control"  value="<?php echo $Addr ?>">
                                        <input name="employee_place" id="field_tl" type="hidden" class="form-control"  value="<?php echo $Tmpt ?>">
                                        <input name="employee_birth_date" id="field_tgl" type="hidden" class="form-control"  value="<?php echo $Tgl ?>">
                                        <input name="employee_entry_date" id="field_ent" type="hidden" class="form-control"  value="<?php echo $Ent ?>">
                                        <input id="field" type="text" class="form-control" placeholder="Ketik NIK atau Nama Karyawan.." value="<?php echo (isset($sket)) ? $sket['sket_employee_name'] : '' ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Status Karyawan</label>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="sket_status" value="tetap" <?php echo ($Stat == 'tetap') ? 'checked' : ''; ?>> Tetap
                                            </label>
                                            &nbsp;&nbsp;&nbsp;
                                            <label>
                                                <input type="radio" name="sket_status" value="kontrak" <?php echo ($Stat == 'kontrak') ? 'checked' : ''; ?>> Kontrak
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div ng-controller="ketCtrl">
                                <div class="form-group"> 
                                    <label >Keterangan Surat *</label>
                                    <select name="ket_id" class="form-control autocomplete">
                                        <option value="">-- Pilih Keterangan --</option>
                                        <option ng-repeat="ket in kets" ng-selected="ket_data.index == ket.ket_id" value="{{ket.ket_id}}">{{ket.ket_surat}}</option>
                                    </select>
                                    <a data-toggle="modal" href="#myModal"><span class="btn btn-success btn-xs">Tambah Keterangan</span></a><br><br>
                                    <div class="modal fade bs-example-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-dialog modal-sm"> 
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Tambah Keterangan Surat</h4>
                                                    </div>
                                                    <div class="modal-body form-group label-floating">
                                                        <label>Keterangan Surat Untuk *</label>
                                                        <input type="text" name="ket_surat" ng-model="ketForm.ket_surat" class="form-control"><br>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                                                        <button type="button" class="btn btn-info" ng-disabled="!(!!ketForm.ket_surat)" ng-click="submit(ketForm)" type="button" data-dismiss="modal">Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="col-md-4"> 
                <div class="card card-profile">
                    <div class="content">
                        <button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
                        <a href="<?php echo site_url('admin/sket'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                        <?php if (isset($sket)): ?>
                            <a href="#delModal" class="btn btn-flat btn-wd btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if (isset($sket)) { ?>
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Konfirmasi Penghapusan</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin akan menghapus data ini ?</p>
            </div>
            <div class="modal-footer">
                <?php echo form_open('admin/sket/delete/' . $sket['sket_id']); ?>
                <input type="hidden" name="delName" value="<?php echo $sket['sket_employee_name']; ?>">
                <button type="button" class="btn btn-info btn-wd" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger btn-wd">Hapus</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- /.row -->



<script>
    $(function() {

        var employee_list = [
        <?php foreach ($employee as $row): ?>
        {
            "id": "<?php echo $row['employee_position'] ?>",
            "value": "<?php echo $row['employee_name'] ?>",
            "label": "<?php echo $row['employee_name'] ?>",
            "adr": "<?php echo $row['employee_address'] ?>",
            "ent": "<?php echo $row['employee_entry_date'] ?>",
            "tgl": "<?php echo $row['employee_birth_date'] ?>",
            "tl": "<?php echo $row['employee_place'] ?>",
            "ent": "<?php echo $row['employee_entry_date'] ?>",
            "label_nik": "<?php echo $row['employee_nik'] ?>"

        },
    <?php endforeach; ?>
    ];
    function custom_source(request, response) {
        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
        response($.grep(employee_list, function(value) {
            return matcher.test(value.label)
            || matcher.test(value.label_nik);
        }));
    }

    $("#field").autocomplete({
        source: custom_source,
        minLength: 1,
        select: function(event, ui) {
                // feed hidden id field                
                $("#field_id").val(ui.item.label_nik);  
                $("#field_name").val(ui.item.value);
                $("#field_pos").val(ui.item.id);
                $("#field_add").val(ui.item.adr);
                $("#field_ent").val(ui.item.ent);
                $("#field_tgl").val(ui.item.tgl);
                $("#field_tl").val(ui.item.tl);


                // update number of returned rows
            },
            open: function(event, ui) {
                // update number of returned rows
                var len = $('.ui-autocomplete > li').length;
            },
            close: function(event, ui) {
                // update number of returned rows
            },
            // mustMatch implementation
            change: function(event, ui) {
                if (ui.item === null) {
                    $(this).val('');
                    $('#field_id').val('');
                }
            }
        });

        // mustMatch (no value) implementation
        $("#field").focusout(function() {
            if ($("#field").val() === '') {
                $('#field_id').val('');
            }
        });
    });
</script>
<style type="text/css">
   /* http://docs.jquery.com/UI/Autocomplete#theming*/
   .ui-autocomplete { 
    position: absolute; 
    cursor: default; 
    background: transparent;
    color: #000;
    font-weight: bold;
    border: 1px solid #00ACD6;
    
}  
.ui-autocomplete:hover { 
    position: absolute; 
    cursor: pointer; 
    background: #00BCD4;
    color: #FFF;
    font-weight: bold;
    
}   

/* workarounds */
html .ui-autocomplete { width:1px; } /* without this, the menu expands to 100% in IE6 */
.ui-menu {
    list-style:none;
    padding: 2px;
    margin: 0;
    display:block;
    float: left;
}
.ui-menu .ui-menu {
    margin-top: -3px;
}
.ui-menu .ui-menu-item {
    margin:0;
    padding: 0;
    zoom: 1;
    float: left;
    clear: left;
    width: 100%;
}
.ui-menu .ui-menu-item a {
    text-decoration:none;
    display:block;
    padding:.2em .4em;
    line-height:1.5;
    zoom:1;
}
.ui-menu .ui-menu-item a.ui-state-hover,
.ui-menu .ui-menu-item a.ui-state-active {
    font-weight: normal;
    margin: -1px;
}
</style>

<script src="<?php echo media_url() ?>js/angular.min.js" type="text/javascript"></script>
<script>
    var comApp = angular.module("comApp", []);
    var SITEURL = "<?php echo site_url() ?>";

    comApp.controller('ketCtrl', function($scope, $http) {
        $scope.kets = [];
        <?php if (isset($sket)): ?>
        $scope.ket_data = {index: <?php echo $sket['ket_ket_id']; ?>};
    <?php endif; ?>

    $scope.getKet = function() {

        var url = SITEURL + 'api/get_ket/';
        $http.get(url).then(function(response) {
            $scope.kets = response.data;
        });

    };

    $scope.submit = function(sket) {
        var postData = $.param(sket);
        $.ajax({
            method: "POST",
            url: SITEURL + "admin/sket/ket/add",
            data: postData,
            success: function(data) {
                $scope.getKet();
                $scope.ketForm.ket_surat = '';
            }
        });
    };

    angular.element(document).ready(function() {
        $scope.getKet();
    });

});

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#target').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputImage").change(function() {
        readURL(this);
    });


</script>