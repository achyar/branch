<?php $this->load->helper('tanggal'); ?>
<html>
<head>
<title><?php echo $sket['sket_employee_nik'] ?> - <?php echo $sket['sket_employee_nama'] ?></title>
  <style type="text/css">
   .upper { text-transform: uppercase; }
   .lower { text-transform: lowercase; }
   .cap   { text-transform: capitalize; }
   .small { font-variant:   small-caps; }
 </style>
 <style type="text/css">
  @page {
    margin-top: 3.75cm;
    margin-bottom: 0.1em;
    margin-left: 5.0em;
    margin-right: 5.0em;
  } .style12 {font-size: 10px}
  .style13 {
   font-size: 14pt;
   font-weight: bold;
 }
</style>
</head>
<body>

  <p align="center"><span class="style13"><u>SURAT KETERANGAN BEKERJA</u></span><br>
    No : <?php echo $sket['sket_number'] ?></p>
  </br></br><br>

  <p align="left">Yang bertanda tangan dibawah ini:</p>
  <table width="100%" border="0">
    <tr>
      <td width="130">Nama</td>
      <td width="5">:</td>
      <td width=""><strong><?php echo $setting_employe_name['setting_value'] ?></strong></td>
    </tr>

    <tr>
      <td>Jabatan</td>
      <td>:</td>
      <td><span class="cap"><?php echo $setting_employe_position['setting_value'] ?></span></td>
    </tr>

    <tr>
      <td>Perusahaan</td>
      <td>:</td>
      <td>PT. Sumber Alfaria Trijaya,Tbk</td>
    </tr>

    <tr>
      <td>Alamat</td>
      <td>:</td>
      <td><span class="cap"><?php echo $alamat['setting_value'] ?></td>
    </tr>

    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><i>Telp. <?php echo $tlp['setting_value'] ?> ( Hunting )</i></td>
    </tr>
  </table><br>

  <p align="left">Menerangkan dengan sesungguhnya, bahwa :
    <table width="100%" border="0">
      <tr>
        <td width="130">Nama</td>
        <td width="5">:</td>
        <td width=""><strong><?php echo $sket['sket_employee_name'] ?></strong></td>
      </tr>

      <tr>
        <td>Tempat/tanggal lahir</td>
        <td>:</td>
        <td><span class="cap"><?php echo $sket['sket_employee_place'] ?>, <?php echo pretty_date($sket['sket_employee_birth_date'],'d F Y',false) ?></span></td>
      </tr>

      <tr>
        <td>Alamat</td>
        <td>:</td>
        <td><span class="cap"><?php echo $sket['sket_employee_address'] ?></span></td>
      </tr>
    </table>
    <br>
    <p align="justify">Adalah benar karyawan PT Sumber Alfaria Trijaya Tbk dengan NIK <?php echo $sket['sket_employee_nik']; ?>
     dengan jabatan sebagai "<span class="cap"><?php echo $sket['sket_employee_position'] ?></span>" di <span class="cap"><?php echo $branch['setting_value'] ?></span>. Yang bersangkutan bekerja sejak 
     <?php echo pretty_date($sket['sket_employee_entry_date'],'d F Y',false) ?> sebagai karyawan <?php echo $sket['sket_status'] ?> dan masih aktif bekerja sampai dengan sekarang.</p>
     <p align="justify">Demikian surat keterangan ini dibuat dengan sesungguhnya untuk dipergunakan sebagai syarat untuk pengantar <?php echo $sket['ket_surat'] ?>.</p><br>

     <table width="100%">
      <tr>
      <td><span class="cap"><?php echo $kota['setting_value'] ?></span>, <?php echo pretty_date($sket['sket_input_date'],'d F Y',false) ?></td>
      </tr>
      <tr>
        <td><strong>PT. Sumber Alfaria Trijaya, Tbk</strong></td>
      </tr>
    </table>
    <br><br><br><br>
    <table width="100%">
      <tr>
        <td><strong><u><span class="upper"><?php echo $setting_employe_name['setting_value'] ?></span></u></strong></td>
      </tr>
      <tr>
        <td><strong><i><span class="cap"><?php echo $setting_employe_position['setting_value'] ?></span></i></strong></td>
      </tr>
    </table>


  </body>
  </html>