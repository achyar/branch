<?php
// $this->load->view('admin/datepicker');
if (isset($disnaker)) {

    $Nomor = $disnaker['disnaker_number'];
    $Nik = $disnaker['disnaker_employee_nik'];
    $Nama = $disnaker['disnaker_employee_name']; 
    $Addr = $disnaker['disnaker_employee_address'];
    $Ent = $disnaker['disnaker_employee_entry_date'];
    $Out = $disnaker['disnaker_employee_out_date'];
    

} else {
    $Nomor = set_value('disnaker_number');
    $Nik = set_value('disnaker_employee_nik');
    $Nama = set_value('disnaker_employee_name');
    $Addr = set_value('disnaker_employee_address');
    $Ent = set_value('disnaker_employee_entry_date');
    $Out = set_value('disnaker_employee_out_date');
}
?>

<div class="content">
    <div class="container-fluid">
        <?php echo form_open_multipart(current_url()); ?>
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">border_color</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $operation ?></h4>
                        <div class="card-content">

                            <?php echo validation_errors(); ?>
                            <?php if (isset($disnaker)) { ?>
                            <input type="hidden" name="disnaker_id" value="<?php echo $disnaker['disnaker_id']; ?>">
                            <?php } ?>

                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">No Surat</label>
                                            <input type="text" class="form-control" name="disnaker_number" value="<?php echo $Nomor ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">NIK</label>
                                            <input class="form-control" name="disnaker_employee_nik" value="<?php echo $Nik ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nama Karyawan</label>
                                            <input class="form-control" name="disnaker_employee_name" value="<?php echo $Nama ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Tanggal Masuk</label>
                                            <input class="form-control datepicker" placeholder="<?php echo date('Y-m-d') ?>" name="disnaker_employee_entry_date" value="<?php echo $Ent ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Tanggal Keluar</label>
                                            <input class="form-control datepicker" placeholder="<?php echo date('Y-m-d') ?>" name="disnaker_employee_out_date" value="<?php echo $Out ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Alamat</label>
                                            <textarea class="form-control" name="disnaker_employee_address"><?php echo $Addr ?></textarea>
                                        </div>
                                    </div>
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4"> 
                <div class="card card-profile">
                    <div class="content">
                        <button type="submit" class="btn btn-flat btn-wd btn-success"><span class="fa fa-check"></span> Simpan</button>
                        <a href="<?php echo site_url('admin/disnaker'); ?>" class="btn btn-flat btn-wd btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                        <?php if (isset($disnaker)): ?>
                            <a href="#delModal" class="btn btn-flat btn-wd btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?php if (isset($disnaker)) { ?>
<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="material-icons">clear</i>
                </button>
                <h4 class="modal-title">Konfirmasi Penghapusan</h4>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin akan menghapus data ini ?</p>
            </div>
            <div class="modal-footer">
                <?php echo form_open('admin/disnaker/delete/' . $disnaker['disnaker_id']); ?>
                <input type="hidden" name="delName" value="<?php echo $disnaker['disnaker_employee_name']; ?>">
                <button type="button" class="btn btn-info btn-wd" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-danger btn-wd">Hapus</button>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>