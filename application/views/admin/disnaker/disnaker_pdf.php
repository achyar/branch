<?php $this->load->helper('tanggal'); ?>
<html>
<head>
  <title><?php echo $disnaker['disnaker_employee_nik'] ?> - <?php echo $disnaker['disnaker_employee_name'] ?></title>
  <style type="text/css">
   .upper { text-transform: uppercase; }
   .lower { text-transform: lowercase; }
   .cap   { text-transform: capitalize; }
   .small { font-variant:   small-caps; }
 </style>
 <style type="text/css">
  @page {
    margin-top: 3.75cm;
    margin-bottom: 0.1em;
    margin-left: 5.0em;
    margin-right: 5.0em;
  } .style12 {font-size: 10px}
  .style13 {
   font-size: 14pt;
   font-weight: bold;
 }
</style>
</head>
<body>

  <table width="100%">
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><span class="cap"><?php echo $kota['setting_value'] ?></span>, <?php echo pretty_date($disnaker['disnaker_input_date'],'d F Y',false) ?></td>
    </tr>

    <tr>
      <td>Nomor</td>
      <td>:</td>
      <td><?php echo $disnaker['disnaker_number'] ?></td>
      <td>Kepada Yth</td>
    </tr>

    <tr>
      <td>Lampiran</td>
      <td>:</td>
      <td>-</td>
      <td>Dinas Ketenagakerjaan</td>
    </tr>

    <tr>
      <td>Perihal</td>
      <td>:</td>
      <td><strong>Surat Pemberitahuan Pengunduran</strong></td>
      <td>di</td>
    </tr>

    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><strong>Diri Karyawan</strong></td>
      <td><span class="cap">Tempat</span></td>
    </tr>
  </table><br>

  <p align="left">Dengan Hormat,</p><br>
  <p align="justify">Berkaitan dengan Peraturan BPJS Ketenagakerjaan Nomor 7 Tahun 2015 Tentang Petunjuk Pelaksanaan
  Pembayaran Manfaat Jaminan Hari Tua sebagaimana tertuang dalam pasal 4 ayat (3).</p>
  <p align="justify">Bersama ini kami sampaikan Pengunduran Diri Karyawan kami yang bekerja di PT. Sumber Alfaria Trijaya, Tbk.
  Dengan data-data tenaga kerja sebagai berikut:</p>

  <table width="100%">
    <tr>
    <td>Nama</td>
    <td>:</td>
    <td><span class="upper"><strong><?php echo $disnaker['disnaker_employee_name'] ?></strong></span></td>
    </tr>
    <tr>
    <td>NIK</td>
    <td>:</td>
    <td><?php echo $disnaker['disnaker_employee_nik'] ?></td>
    </tr>
    <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><span class="cap"><?php echo $disnaker['disnaker_employee_address'] ?></span></td>
    </tr>
  </table><br>

  <p align="justify">Adalah benar karyawan kami sejak tanggal <strong><?php echo pretty_date($disnaker['disnaker_employee_entry_date'],'d F Y',false) ?></strong> sampai dengan <strong><?php echo pretty_date($disnaker['disnaker_employee_out_date'],'d F Y',false) ?></strong>.</p>
  <p align="justify">Demikian surat ini dibuat untuk mendapatkan legalisir dari Dinas Ketenagakerjaan Kota Tangerang agar dapat dipergunakan dalam pengurusan BPJS Ketenagakerjaan (Jaminan Hari Tua) yang bersangkutan.</p>
  <p align="justify">Atas Perhatian dan kerjasamanya kami ucapkan terima kasih.</p>
  <br><br>

  <table width="100%">
      <tr>
      <td>Hormat Kami</td>
      </tr>
      <tr>
        <td><strong>PT. Sumber Alfaria Trijaya, Tbk</strong></td>
      </tr>
    </table>
    <br><br><br><br>
    <table width="100%">
      <tr>
        <td><strong><u><span class="upper"><?php echo $setting_employe_name['setting_value'] ?></span></u></strong></td>
      </tr>
      <tr>
        <td><strong><i><span class="cap"><?php echo $setting_employe_position['setting_value'] ?></span></i></strong></td>
      </tr>
    </table>


</body>
</html>