<?php
$this->load->view('admin/datepicker');
if (isset($prakerin)) {


    $NameValue = $prakerin['prakerin_name'];
    $SexValue = $prakerin['prakerin_gender'];
    $PlaceValue = $prakerin['prakerin_place'];
    $BirthValue = $prakerin['prakerin_birth_date'];
    $SchoolValue = $prakerin['prakerin_school'];
    $EduValue = $prakerin['prakerin_education'];
    $PhoneValue = $prakerin['prakerin_phone'];
    $AddValue = $prakerin['prakerin_address'];
    $ImageValue = $prakerin['prakerin_image'];
    $MentorValue = $prakerin['prakerin_mentor_name'];
    $NikValue = $prakerin['prakerin_mentor_nik'];
    $DeptValue = $prakerin['prakerin_dept'];
    $EntryValue = $prakerin['prakerin_entry_date'];
    $EndValue = $prakerin['prakerin_out_date'];
    $StatusValue = $prakerin['prakerin_status'];
} else {
    $NameValue = set_value('prakerin_name');    
    $SexValue = set_value('prakerin_gender');
    $PlaceValue = set_value('prakerin_place');
    $BirthValue = set_value('prakerin_birth_date');
    $SchoolValue = set_value('prakerin_school');
    $EduValue = set_value('prakerin_education');
    $PhoneValue = set_value('prakerin_phone');
    $AddValue = set_value ('prakerin_address');
    $ImageValue = set_value('prakerin_image');
    $DeptValue = set_value('employee_dept');
    $MentorValue = set_value('employee_name');
    $NikValue = set_value('employee_nik');
    $EntryValue = set_value('prakerin_entry_date');
    $EndValue = set_value('prakerin_out_date');
    $StatusValue = set_value('prakerin_status');
}
?>
<?php echo isset($alert) ? ' ' . $alert : null; ?>
<?php echo validation_errors(); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Prakerin
            <small><?php echo $operation; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Prakerin</li>
            <li class="active"><?php echo $operation; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo form_open_multipart(current_url()); ?>
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-9">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">                        
                        <?php if (isset($prakerin)) { ?>
                            <input type="hidden" name="prakerin_id" value="<?php echo $prakerin['prakerin_id']; ?>">
                            <input type="hidden" name="prakerin_nip" value="<?php echo $prakerin['prakerin_nip']; ?>">
                            <?php } ?>
                            
                            <div class="form-group">
                                <label>Nama Lengkap <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <input type="text" placeholder="Nama Lengkap" class="form-control" name="prakerin_name" value="<?php echo $NameValue ?>">
                            </div>

                            <?php if (!isset($prakerin)) { ?>
                                <div class="form-group">
                                    <label>Password <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input name="prakerin_password" type="password" class="form-control" placeholder="Password">
                                </div>            

                                <div class="form-group">
                                    <label>Konfirmasi Password <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input name="passconf" type="password" class="form-control" placeholder="Konfirmasi Password">
                                </div>       
                                <?php } ?>


                                <div class="form-group">
                                    <label>Jenis Kelamin <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="prakerin_gender" value="LAKI-LAKI" <?php echo ($SexValue == 'LAKI-LAKI') ? 'checked' : ''; ?>> Laki-laki
                                        </label>

                                        &nbsp;&nbsp;&nbsp;
                                        <label>
                                            <input type="radio" name="prakerin_gender" value="PEREMPUAN" <?php echo ($SexValue == 'PEREMPUAN') ? 'checked' : ''; ?>> Perempuan
                                        </label>
                                    </div> 
                                </div>

                                <div class="form-group">
                                    <label>Tempat Lahir <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input name="prakerin_place" type="text" class="form-control" value="<?php echo $PlaceValue ?>" placeholder="Tempat Lahir">
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Lahir <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input name="prakerin_birth_date" class="form-control datepicker" value="<?php echo $BirthValue ?>" placeholder="Tanggal Lahir">
                                </div>

                                <div class="form-group">
                                <label>Jenjang Pendidikan <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                <select name="prakerin_education" class="form-control">
                                    <option value="">--- Pilih Pendidikan ---</option>
                                    <option value="SMK" <?php echo ($EduValue == 'SMK') ? 'selected' : '' ?>>SMK</option>
                                    <option value="SMA" <?php echo ($EduValue == 'SMA') ? 'selected' : '' ?>>SMA</option>
                                    <option value="UNIVERSITAS" <?php echo ($EduValue == 'UNIVERSITAS') ? 'selected' : '' ?>>UNIVERSITAS</option>
                                    <option value="AKADEMI" <?php echo ($EduValue == 'AKADEMI') ? 'selected' : '' ?>>AKADEMI</option>
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label>Asal Sekolah <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input name="prakerin_school" type="text" class="form-control" value="<?php echo $SchoolValue ?>" placeholder="Sekolah">
                                </div>

                                <div class="form-group">
                                    <label>Nomor Telepon/HP <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input type="text" placeholder="No. Tlp" class="form-control" name="prakerin_phone" value="<?php echo $PhoneValue ?>">
                                </div>

                                <div class="form-group">
                                    <label>Alamat <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <textarea class="form-control" placeholder="Alamat" name="prakerin_address"><?php echo $AddValue ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label>NIK<small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input name="employee_nik" id="field_id" type="hidden" class="form-control"  value="<?php echo $MentorValue ?>">
                                    <input name="employee_name" id="field_name" type="hidden" class="form-control"  value="<?php echo $NikValue ?>">
                                    <input name="employee_dept" id="field_dept" type="hidden" class="form-control"  value="<?php echo $DeptValue ?>">
                                    <input id="field" type="text" class="form-control" placeholder="Ketik NIK atau Nama Pembimbing.." value="<?php echo (isset($prakerin)) ? $prakerin['prakerin_mentor_name'] : '' ?>">
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Masuk <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input  placeholder="Tanggal Masuk" class="form-control datepicker" name="prakerin_entry_date" value="<?php echo $EntryValue ?>">
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Keluar <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                    <input  placeholder="Tanggal Keluar" class="form-control datepicker" name="prakerin_out_date" value="<?php echo $EndValue ?>">
                                </div>

                                <p class="text-muted">*) Kolom wajib diisi.</p>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="box box-primary">
                            <!-- /.box-header -->
                            <div class="box-body">

                                <label>Status *</label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="prakerin_status" value="0" <?php echo ($StatusValue == '0') ? 'checked' : ''; ?>> Non-Aktif
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="prakerin_status" value="1" <?php echo ($StatusValue == '1') ? 'checked' : ''; ?>> Aktif
                                    </label>
                                </div>
                                <label >Foto</label>
                                <a href="#" class="thumbnail">
                                    <?php if (isset($prakerin) AND $prakerin['prakerin_image'] != NULL) { ?>
                                        <img src="<?php echo upload_url('prakerin/' . $prakerin['prakerin_image']) ?>" class="img-responsive avatar">
                                        <?php } else { ?>
                                            <img id="target" alt="Choose image to upload">
                                            <?php } ?>
                                        </a>
                                        <input type='file' id="inputImage" name="inputImage">
                                        <br>
                                        <button type="submit" class="btn btn-flat btn-block btn-success"><span class="fa fa-check"></span> Simpan</button>
                                        <a href="<?php echo site_url('admin/prakerin'); ?>" class="btn btn-flat btn-block btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                                        <?php if (isset($prakerin)): ?>
                                            <a href="#delModal" class="btn btn-flat btn-block btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                                        <?php endif; ?>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                    <?php if (isset($prakerin)) { ?>
                        <div class="modal modal-danger fade" id="delModal">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title"><span class="fa fa-warning"></span> Konfirmasi penghapusan</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p>Apakah anda yakin akan menghapus data ini?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <?php echo form_open('admin/prakerin/delete/' . $id); ?>
                                            <input type="hidden" name="del_id" value="<?php echo $id ?>" />
                                            <input type="hidden" name="del_name" value="<?php echo $prakerin['prakerin_name'] ?>" />

                                            <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Batal</button>
                                            <button type="submit" class="btn btn-outline"><span class="fa fa-check"></span> Hapus</button>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <?php } ?>

                        </div>

                        <script>
                            function readURL(input) {
                                if (input.files && input.files[0]) {
                                    var reader = new FileReader();
                                    reader.onload = function(e) {
                                        $('#target').attr('src', e.target.result);
                                    };

                                    reader.readAsDataURL(input.files[0]);
                                }
                            }

                            $("#inputImage").change(function() {
                                readURL(this);
                            });
                        </script>

 <script>
                $(function() {

                    var employee_list = [
                    <?php foreach ($employee as $row): ?>
                    {
                        
                        "value": "<?php echo $row['employee_name'] ?>",
                        "label": "<?php echo $row['employee_name'] ?>",
                        "dept": "<?php echo $row['employee_dept'] ?>",
                        "label_nik": "<?php echo $row['employee_nik'] ?>"

                    },
                <?php endforeach; ?>
                ];
                function custom_source(request, response) {
                    var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                    response($.grep(employee_list, function(value) {
                        return matcher.test(value.label)
                        || matcher.test(value.label_nik);
                    }));
                }

                $("#field").autocomplete({
                    source: custom_source,
                    minLength: 1,
                    select: function(event, ui) {
                // feed hidden id field                
                $("#field_id").val(ui.item.label_nik);  
                $("#field_name").val(ui.item.value);
                $("#field_dept").val(ui.item.dept);

                // update number of returned rows
            },
            open: function(event, ui) {
                // update number of returned rows
                var len = $('.ui-autocomplete > li').length;
            },
            close: function(event, ui) {
                // update number of returned rows
            },
            // mustMatch implementation
            change: function(event, ui) {
                if (ui.item === null) {
                    $(this).val('');
                    $('#field_id').val('');
                }
            }
        });

        // mustMatch (no value) implementation
        $("#field").focusout(function() {
            if ($("#field").val() === '') {
                $('#field_id').val('');
            }
        });
    });
</script>