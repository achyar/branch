<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="red">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">List Prakerin
                            <a href="<?php echo site_url('admin/prakerin/add') ?>" class="btn btn-danger btn-xs pull-right"><i class="material-icons">add_box</i> Tambah</a>
                            <p class="category">List</p></h4>

                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead class="text-danger">
                                        <th>No</th>
                                        <th>NIPM Prakerin</th>
                                        <th>Nama</th>
                                        <th>Sekolah</th>
                                        <th>Aksi</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($prakerin)) {
                                            $i = 1;
                                            foreach ($prakerin as $row):
                                                ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $row['prakerin_nik']; ?></td>
                                                <td><?php echo $row['prakerin_full_name']; ?></td>
                                                <td><?php echo $row['prakerin_school']; ?></td>
                                                <td>
                                                    <a href="<?php echo site_url('admin/prakerin/view/' . $row['prakerin_id']) ?>" rel="tooltip" title="Lihat" class="text-warning"><i class="material-icons">pageview</i></a> &nbsp;

                                                    <a href="<?php echo site_url('admin/prakerin/edit/' . $row['prakerin_id']) ?>" rel="tooltip" title="Edit" class="text-success"><i class="material-icons">mode_edit</i></a> &nbsp;

                                                    <a href="<?php echo site_url('admin/prakerin/rpw/' . $row['prakerin_id']) ?>" rel="tooltip" title="Reset Password" class="text-info"><i class="material-icons">lock_open</i></a> &nbsp;
                                                    

                                                    </td>
                                                </tr>
                                                <?php
                                                $i++;
                                                endforeach;
                                            } else {
                                                ?>
                                                <tr id="row">
                                                    <td colspan="5" align="center">Data Kosong</td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <?php echo $this->pagination->create_links(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
