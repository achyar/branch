<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Surat Keterangan Magang
            <small>List</small>
        </h1>
        <ol class="breadcrumb">
            <li class="pull-right"><a href="<?php echo site_url('admin/supra/add'); ?>" class="btn btn-info"><i class="fa fa-plus"></i> Tambah</a></li>
        </ol>
    </section><br>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-init table-bordered table-striped jambo_table">
                            <thead class="gradient">
                                <tr>
                                    <th>No</th> 
                                    <th>No Surat</th>
                                    <th>NIPM</th>
                                    <th>Nama</th>
                                    <th>Sekolah</th>
                                    <th>Tanggal Buat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if ($supra > 0) {
                                    $i = 1;
                                    foreach ($supra as $row):
                                        ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $row['supra_number']; ?></td>
                                        <td><?php echo $row['supra_nip']; ?></td>
                                        <td><?php echo $row['supra_name']; ?></td>
                                        <td><?php echo $row['supra_education'].' '.$row['supra_school']; ?></td>
                                        <td><?php echo pretty_date($row['supra_input_date'],'d F Y',false) ?></td>
                                        <td>
                                            <a href="<?php echo site_url('admin/supra/detail/' . $row['supra_id']) ?>" data-toggle="tooltip" title="Lihat" class="text-warning"><span class="fa fa-eye"></span></a> &nbsp;

                                            <a href="<?php echo site_url('admin/supra/edit/' . $row['supra_id']) ?>" data-toggle="tooltip" title="Sunting" class="text-success"><span class="fa fa-edit"></span></a> &nbsp;

                                            <a href="<?php echo site_url('admin/supra/printPdf/' . $row['supra_id']) ?>" data-toggle="tooltip" title="Print" class="text-success view-pdf"><span class="fa fa-print"></span></a> &nbsp;

                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                    endforeach;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
</div>

<script type="text/javascript">
    (function(a){a.createModal=function(b){defaults={title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false};var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";html='<div class="modal fade" id="myModal">';html+='<div class="modal-dialog">';html+='<div class="modal-content">';html+='<div class="modal-header">';html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';if(b.title.length>0){html+='<h4 class="modal-title">'+b.title+"</h4>"}html+="</div>";html+='<div class="modal-body" '+c+">";html+=b.message;html+="</div>";html+='<div class="modal-footer">';if(b.closeButton===true){html+='<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>'}html+="</div>";html+="</div>";html+="</div>";html+="</div>";a("body").prepend(html);a("#myModal").modal().on("hidden.bs.modal",function(){a(this).remove()})}})(jQuery);

/*
* Here is how you use it
*/
$(function(){    
    $('.view-pdf').on('click',function(){
        var pdf_link = $(this).attr('href');
        //var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>'
        //var iframe = '<object data="'+pdf_link+'" type="application/pdf"><embed src="'+pdf_link+'" type="application/pdf" /></object>'        
        var iframe = '<object type="application/pdf" data="'+pdf_link+'" width="100%" height="500">No Support</object>'
        $.createModal({
            title:'Surat Keterangan Magang',
            message: iframe,
            closeButton:true,
            scrollable:false
        });
        return false;        
    });    
})
</script>