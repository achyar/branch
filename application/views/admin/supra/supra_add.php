<?php
$this->load->view('admin/datepicker');
if (isset($supra)) {

    $Nomor = $supra['supra_number'];
    $Nik = $supra['supra_nip'];
    $Nama = $supra['supra_name'];
    $Dept = $supra['supra_dept'];
    $Edu = $supra['supra_education'];
    $Sch = $supra['supra_school'];
    $Ent = $supra['supra_entry_date'];
    $Out = $supra['supra_out_date'];
    
    
} else {
    $Nomor = set_value('supra_number');
    $Nik = set_value('prakerin_nip');
    $Nama = set_value('prakerin_name');
    $Dept = set_value('prakerin_dept');
    $Edu = set_value('prakerin_education');
    $Sch = set_value('prakerin_school');
    $Ent = set_value('prakerin_entry_date');
    $Out = set_value('prakerin_out_date');
}
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Surat Keterangan Magang
            <small><?php echo $operation; ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Surat Keterangan Magang</li>
            <li class="active"><?php echo $operation; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo form_open_multipart(current_url()); ?>
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-9">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php echo validation_errors(); ?>
                        <?php if (isset($supra)) { ?>
                        <input type="hidden" name="supra_id" value="<?php echo $supra['supra_id']; ?>">
                        <input type="hidden" name="supra_number" value="<?php echo $supra['supra_number']; ?>">
                        <?php } ?>

                        <div class="form-group">
                            <label>NIPM<small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                            <input name="prakerin_nip" id="field_id" type="hidden" class="form-control"  value="<?php echo $Nik ?>">
                            <input name="prakerin_name" id="field_name" type="hidden" class="form-control"  value="<?php echo $Nama ?>">
                            <input name="prakerin_dept" id="field_dept" type="hidden" class="form-control"  value="<?php echo $Dept ?>">
                            <input name="prakerin_education" id="field_edu" type="hidden" class="form-control"  value="<?php echo $Edu ?>">
                            <input name="prakerin_school" id="field_sch" type="hidden" class="form-control"  value="<?php echo $Sch ?>">
                            <input name="prakerin_out_date" id="field_out" type="hidden" class="form-control"  value="<?php echo $Out ?>">
                            <input name="prakerin_entry_date" id="field_ent" type="hidden" class="form-control"  value="<?php echo $Ent ?>">
                            <input id="field" type="text" class="form-control" placeholder="Ketik NIP atau Nama Prakerin.." value="<?php echo (isset($supra)) ? $supra['supra_name'] : '' ?>">
                        </div>

                        <p class="text-muted">*) Kolom wajib diisi.</p>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-3">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <button type="submit" class="btn btn-flat btn-block btn-success"><span class="fa fa-check"></span> Simpan</button>
                        <a href="<?php echo site_url('admin/supra'); ?>" class="btn btn-flat btn-block btn-info"><span class="fa fa-arrow-left"></span> Batal</a>
                        <?php if (isset($supra)): ?>
                            <a href="#delModal" class="btn btn-flat btn-block btn-danger" data-toggle="modal" ><span class="fa fa-close"></span> Hapus</a>
                        <?php endif; ?>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <?php if (isset($supra)) { ?>
    <div class="modal modal-danger fade" id="delModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title"><span class="fa fa-warning"></span> Konfirmasi penghapusan</h3>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda yakin akan menghapus data ini?</p>
                    </div>
                    <div class="modal-footer">
                        <?php echo form_open('admin/supra/delete/' . $supra['supra_id']); ?>
                        <input type="hidden" name="del_name" value="<?php echo $supra['supra_number']; ?>">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="fa fa-close"></span> Batal</button>
                        <button type="submit" class="btn btn-outline"><span class="fa fa-check"></span> Hapus</button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <?php } ?>

    </div>


    <script>
        $(function() {

            var prakerin_list = [
            <?php foreach ($prakerin as $row): ?>
            {
                
                "value": "<?php echo $row['prakerin_name'] ?>",
                "label": "<?php echo $row['prakerin_name'] ?>",
                "edu": "<?php echo $row['prakerin_education'] ?>",
                "sch": "<?php echo $row['prakerin_school'] ?>",
                "dept": "<?php echo $row['prakerin_dept'] ?>",
                "ent": "<?php echo $row['prakerin_entry_date'] ?>",
                "out": "<?php echo $row['prakerin_out_date'] ?>",
                "label_nik": "<?php echo $row['prakerin_nip'] ?>"

            },
        <?php endforeach; ?>
        ];
        function custom_source(request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response($.grep(prakerin_list, function(value) {
                return matcher.test(value.label)
                || matcher.test(value.label_nik);
            }));
        }

        $("#field").autocomplete({
            source: custom_source,
            minLength: 1,
            select: function(event, ui) {
                // feed hidden id field                
                $("#field_id").val(ui.item.label_nik);  
                $("#field_name").val(ui.item.value);
                $("#field_dept").val(ui.item.dept);
                $("#field_edu").val(ui.item.edu);
                $("#field_ent").val(ui.item.ent);
                $("#field_out").val(ui.item.out);
                $("#field_sch").val(ui.item.sch);


                // update number of returned rows
            },
            open: function(event, ui) {
                // update number of returned rows
                var len = $('.ui-autocomplete > li').length;
            },
            close: function(event, ui) {
                // update number of returned rows
            },
            // mustMatch implementation
            change: function(event, ui) {
                if (ui.item === null) {
                    $(this).val('');
                    $('#field_id').val('');
                }
            }
        });

        // mustMatch (no value) implementation
        $("#field").focusout(function() {
            if ($("#field").val() === '') {
                $('#field_id').val('');
            }
        });
    });
</script>