<?php $this->load->helper('tanggal'); ?>
<?php
$startRaw = $supra['supra_entry_date'];
$start = DateTime::createFromFormat('Y-m-d', $startRaw );

$endRaw = $supra['supra_out_date'];
$end = DateTime::createFromFormat('Y-m-d', $endRaw );

$diff = $start->diff($end);
 ?>
<html>
<head>
<title><?php echo $supra['supra_nip'] ?> - <?php echo $supra['supra_name'] ?></title>
  <style type="text/css">
   .upper { text-transform: uppercase; }
   .lower { text-transform: lowercase; }
   .cap   { text-transform: capitalize; }
   .small { font-variant:   small-caps; }
 </style>
 <style type="text/css">
  @page {
    margin-top: 3.75cm;
    margin-bottom: 0.1em;
    margin-left: 5.0em;
    margin-right: 5.0em;
  } .style12 {font-size: 10px}
  .style13 {
   font-size: 14pt;
   font-weight: bold;
 }
</style>
</head>
<body>

  <p align="center"><span class="style13"><u>SURAT KETERANGAN MAGANG</u></span><br>
    No : <?php echo $supra['supra_number'] ?></p>
  </br></br><br>

  <p align="left">Yang bertanda tangan dibawah ini:</p>
  <table width="100%" border="0">
    <tr>
      <td width="130">Nama</td>
      <td width="5">:</td>
      <td width=""><strong><span class="upper"><?php echo $setting_employe_name['setting_value'] ?></span></strong></td>
    </tr>

    <tr>
      <td>Jabatan</td>
      <td>:</td>
      <td><span class="cap"><?php echo $setting_employe_position['setting_value'] ?></span></td>
    </tr>

    <tr>
      <td>Perusahaan</td>
      <td>:</td>
      <td>PT. Sumber Alfaria Trijaya,Tbk</td>
    </tr>

  </table><br>

  <p align="left">Menerangkan dengan sesungguhnya, bahwa :
    <table width="100%" border="0">
      <tr>
        <td width="130">Nama</td>
        <td width="5">:</td>
        <td width=""><strong><span class="upper"><?php echo $supra['supra_name'] ?></span></strong></td>
      </tr>

      <tr>
        <td>Sekolah</td>
        <td>:</td>
        <td><span class="upper"><?php echo $supra['supra_education'] ?></span>&nbsp;<span class="cap"><?php echo $supra['supra_school'] ?></span></td>
      </tr>

      <tr>
        <td>Lokasi Magang</td>
        <td>:</td>
        <td><span class="cap"><?php echo $supra['supra_dept'] ?></span></td>
      </tr>
    </table>
    <br>
    <p align="justify">Bahwa nama tersebut diatas telah melakukan aktifitas magang kerja di perusahaan kami PT. Sumber Alfaria Trijaya, Tbk selama <?php echo $diff->format('%m bulan') ?>, terhitung dari tanggal <?php echo pretty_date($supra['supra_entry_date'],'d F Y',false) ?> sampai dengan <?php echo pretty_date($supra['supra_out_date'],'d F Y',false) ?>.</p>
     <p align="justify">Demikian surat keterangan ini dibuat dengan sesungguhnya untuk dipergunakan sebagaimana mestinya.</p><br>

     <table width="100%">
      <tr>
      <td><span class="cap"><?php echo $kota['setting_value'] ?></span>, <?php echo pretty_date($supra['supra_input_date'],'d F Y',false) ?></td>
      </tr>
      <tr>
        <td><strong>PT. Sumber Alfaria Trijaya, Tbk</strong></td>
      </tr>
    </table>
    <br><br><br><br>
    <table width="100%">
      <tr>
        <td><strong><u><span class="upper"><?php echo $setting_employe_name['setting_value'] ?></span></u></strong></td>
      </tr>
      <tr>
        <td><strong><i><span class="cap"><?php echo $setting_employe_position['setting_value'] ?></span></i></strong></td>
      </tr>
    </table>


  </body>
  </html>