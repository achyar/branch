<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PT. SAT <?php echo isset($title) ? ' | ' . $title : null; ?> </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo media_url('ico/favicon.png'); ?>" type="image/x-icon">
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo media_url() ?>css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo media_url() ?>css/material-dashboard.css" rel="stylesheet" />
    <link href="<?php echo media_url() ?>css/mediamanager.css" rel="stylesheet" />
    <link href="<?php echo media_url() ?>css/mediapopup.css" rel="stylesheet" />
    <link href="<?php echo media_url() ?>css/modalpopup.css" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo media_url() ?>css/demo.css" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="<?php echo media_url() ?>css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo media_url() ?>css/load-font.css"/>
    <!-- Theme initialization -->
    <!--   Core JS Files   -->

    <script src="<?php echo media_url() ?>js/jquery-3.2.0.min.js" type="text/javascript"></script>
    <script src="<?php echo media_url() ?>js/jquery-migrate-3.0.0.min.js"></script>
    <script src="<?php echo media_url() ?>js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo media_url() ?>js/angular.min.js" type="text/javascript" ></script>
    <script src="<?php echo media_url() ?>js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo media_url() ?>js/material.min.js" type="text/javascript"></script>
    <script src="<?php echo media_url() ?>js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>

</head>

<body <?php echo isset($ngapp) ? $ngapp : null; ?>>

    <?php $this->load->view('admin/sidebar'); ?>

    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-minimize">
                    <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                        <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                        <i class="material-icons visible-on-sidebar-mini">view_list</i>
                    </button>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">PT. Sumber Alfaria Trijaya, Tbk</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                               <i class="material-icons">person</i>
                               <p class="hidden-lg hidden-md">Profile</p>
                           </a>
                           <ul class="dropdown-menu">
                            <li><a href="<?php echo site_url('admin/profile') ?>"><i class="material-icons">account_circle</i> Profile</a></li>
                            <li><a href="<?php echo site_url('admin/auth/logout?location=' . htmlspecialchars($_SERVER['REQUEST_URI'])) ?>"> <i class="material-icons">exit_to_app</i> Logout</a></li>
                        </ul>
                    </li>
                </ul>

                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group  is-empty">
                        <input type="text" class="form-control" disabled="" placeholder="Search">
                        <span class="material-input"></span>
                    </div>
                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                        <i class="material-icons">search</i><div class="ripple-container"></div>
                    </button>
                </form>
            </div>
        </div>
    </nav>

    <!-- Content Wrapper. Contains page content -->
    <?php isset($main) ? $this->load->view($main) : null; ?>

    <footer class="footer">
        <div class="container-fluid">
            <nav class="pull-left">
                <ul>
                    <li>
                        Copyright &copy; <script>document.write(new Date().getFullYear())</script> All rights reserved
                    </li>
                </ul>
            </nav>
            <nav class="pull-right">
                <ul>
                    <li>
                        Nia K | Achyar Anshorie&trade;
                    </li>
                </ul>
            </nav>
        </div>
    </footer>

</body>

</html>


<!-- Forms Validations Plugin -->
<script src="<?php echo media_url() ?>js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo media_url() ?>js/moment.min.js"></script>
<!--  Charts Plugin -->
<script src="<?php echo media_url() ?>js/chartist.min.js"></script>
<!--  Plugin for the Wizard -->
<script src="<?php echo media_url() ?>js/jquery.bootstrap-wizard.js"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo media_url() ?>js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<script src="<?php echo media_url() ?>js/jquery.sharrre.js"></script>
<script src="<?php echo media_url() ?>js/mediamanager.js"></script>
<script src="<?php echo media_url() ?>js/mediapopup.js"></script>
<script src="<?php echo media_url() ?>js/modalpopup.js"></script>
<!-- DateTimePicker Plugin -->
<script src="<?php echo media_url() ?>js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<script src="<?php echo media_url() ?>js/jquery-jvectormap.js"></script>
<!-- Sliders Plugin -->
<script src="<?php echo media_url() ?>js/nouislider.min.js"></script>
<!-- Select Plugin -->
<script src="<?php echo media_url() ?>js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="<?php echo media_url() ?>js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?php echo media_url() ?>js/sweetalert2.js"></script>
<!--    Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?php echo media_url() ?>js/jasny-bootstrap.min.js"></script>
<!--  Full Calendar Plugin    -->
<script src="<?php echo media_url() ?>js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<script src="<?php echo media_url() ?>js/jquery.tagsinput.js"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo media_url() ?>js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo media_url() ?>js/demo.js"></script>


<?php if ($this->session->flashdata('success')) { ?>
<script type="text/javascript">
    $.notify('<?php echo $this->session->flashdata('success'); ?>',
    {
        className: 'success',
        globalPosition: 'top center'
    }
    );

</script>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        demo.initFormExtendedDatetimepickers();
    });
</script>
<script type="text/javascript">
    function validate(evt) {
      var theEvent = evt || window.event;
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode( key );
      var regex = /[0-9]|\./;
      if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}
</script>
