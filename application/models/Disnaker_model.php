<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
* Disnaker Model Class
 *
 * @package     HCA
 * @subpackage  Models
 * @category    Models
 * @author      Achyar Anshorie
 */

class Disnaker_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array())
    {
        if(isset($params['id']))
        {
            $this->db->where('disnaker.disnaker_id', $params['id']);
        }

        if (isset($params['multiple_id'])) {
            $this->db->where_in('disnaker.disnaker_id', $params['multiple_id']);
        }

        if(isset($params['date_start']) AND isset($params['date_end']))
        {
            $this->db->where('disnaker_date', $params['date_start']);
            $this->db->or_where('disnaker_date', $params['date_end']);
        }

        if(isset($params['limit']))
        {
            if(!isset($params['offset']))
            {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by']))
        {
            $this->db->order_by($params['order_by'], 'desc');
        }
        else
        {
            $this->db->order_by('disnaker_last_update', 'desc');
        }

        $this->db->select('disnaker.disnaker_id, disnaker_number, disnaker_employee_nik, disnaker_employee_name, disnaker_employee_address, disnaker_employee_entry_date, disnaker_employee_out_date, user_nik, user_full_name,
            disnaker_input_date, disnaker_last_update');   
        $this->db->join('users', 'users.user_id = disnaker.user_user_id', 'left');  
        $res = $this->db->get('disnaker');

        if(isset($params['id']) OR (isset($params['limit']) AND $params['limit']==1))
        {
            return $res->row_array();
        }
        else
        {
            return $res->result_array();
        }
    }

    // Add and update to database
    function add($data = array()) {

       if(isset($data['disnaker_id'])) {
        $this->db->set('disnaker_id', $data['disnaker_id']);
    }

    if(isset($data['disnaker_number'])) {
        $this->db->set('disnaker_number', $data['disnaker_number']);
    }

    if(isset($data['disnaker_employee_nik'])) {
        $this->db->set('disnaker_employee_nik', $data['disnaker_employee_nik']);
    }

    if(isset($data['disnaker_employee_name'])) {
        $this->db->set('disnaker_employee_name', $data['disnaker_employee_name']);
    }        

    if(isset($data['disnaker_employee_entry_date'])) {
        $this->db->set('disnaker_employee_entry_date', $data['disnaker_employee_entry_date']);
    }

    if(isset($data['disnaker_employee_out_date'])) {
        $this->db->set('disnaker_employee_out_date', $data['disnaker_employee_out_date']);
    }

    if(isset($data['disnaker_employee_address'])) {
        $this->db->set('disnaker_employee_address', $data['disnaker_employee_address']);
    }

    if(isset($data['user_id'])) {
            $this->db->set('user_user_id', $data['user_id']);
        }

    if(isset($data['disnaker_input_date'])) {
        $this->db->set('disnaker_input_date', $data['disnaker_input_date']);
    }

    if(isset($data['disnaker_last_update'])) {
        $this->db->set('disnaker_last_update', $data['disnaker_last_update']);
    }   

    if (isset($data['disnaker_id'])) {
        $this->db->where('disnaker_id', $data['disnaker_id']);
        $this->db->update('disnaker');
        $id = $data['disnaker_id'];
    } else {
        $this->db->insert('disnaker');
        $id = $this->db->insert_id();
    }

    $status = $this->db->affected_rows();
    return ($status == 0) ? FALSE : $id;
}

    // Delete to database
function delete($id) {
    $this->db->where('disnaker_id', $id);
    $this->db->delete('disnaker');
}

}
