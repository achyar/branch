<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Agenda Model Class
 *
 * @package     HCA
 * @subpackage  Models
 * @category    Models
 * @author      Achyar Anshorie
 */
class Agenda_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array()) {
        if (isset($params['id'])) {
            $this->db->where('agenda_id', $params['id']);
        }

        if (isset($params['agenda_text'])) {
            $this->db->where('agenda_text', $params['agenda_text']);
        }

        if (isset($params['agenda_status'])) {
            $this->db->where('agenda_status', $params['agenda_status']);
        }

        if (isset($params['date_start']) AND isset($params['date_end'])) {
            $this->db->where('agenda_last_update >=', $params['date_start'] . ' 00:00:00');
            $this->db->where('agenda_last_update <=', $params['date_end'] . ' 23:59:59');
        }

        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } else {
            $this->db->order_by('agenda_id', 'desc');
        }

        $this->db->select('agenda.agenda_id, agenda_text, agenda_date, agenda_status, agenda_input_date, agenda_last_update');
        $this->db->select('user_full_name, user_nik');

        $this->db->join('users', 'users.user_id = agenda.user_user_id', 'left');
        $res = $this->db->get('agenda');

         if(isset($params['id']))
        {
            return $res->row_array();
        }
        else
        {
            return $res->result_array();
        }
    }

    // Add and update to database
    function add($data = array()) {

        if (isset($data['agenda_id'])) {
            $this->db->set('agenda_id', $data['agenda_id']);
        }

        if (isset($data['agenda_text'])) {
            $this->db->set('agenda_text', $data['agenda_text']);
        }

        if (isset($data['agenda_date'])) {
            $this->db->set('agenda_date', $data['agenda_date']);
        }

        if (isset($data['agenda_status'])) {
            $this->db->set('agenda_status', $data['agenda_status']);
        }

        if (isset($data['user_id'])) {
            $this->db->set('user_user_id', $data['user_id']);
        }

        if (isset($data['agenda_input_date'])) {
            $this->db->set('agenda_input_date', $data['agenda_input_date']);
        }

        if (isset($data['agenda_last_update'])) {
            $this->db->set('agenda_last_update', $data['agenda_last_update']);
        }

        if (isset($data['agenda_id'])) {
            $this->db->where('agenda_id', $data['agenda_id']);
            $this->db->update('agenda');
            $id = $data['agenda_id'];
        } else {
            $this->db->insert('agenda');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

}
