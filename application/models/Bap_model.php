<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
* Suratk Model Class
 *
 * @package     HCA
 * @subpackage  Models
 * @category    Models
 * @author      Achyar Anshorie
 */

class Bap_model extends CI_Model {

    function __construct() {
        parent::__construct();
    } 

    // Get From Databases
    function get($params = array())
    {
        if(isset($params['id']))
        {
            $this->db->where('bap.bap_id', $params['id']);
        }

        if (isset($params['multiple_id'])) {
            $this->db->where_in('bap.bap_id', $params['multiple_id']);
        }
        
        if(isset($params['date_start']) AND isset($params['date_end']))
        {
            $this->db->where('bap_input_date >=', $params['date_start']. ' 00:00:00');
            $this->db->where('bap_input_date <=', $params['date_end']. ' 23:59:59');
        }

        if(isset($params['limit']))
        {
            if(!isset($params['offset']))
            {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by']))
        {
            $this->db->order_by($params['order_by'], 'desc');
        }
        else
        {
            $this->db->order_by('bap_last_update', 'desc');
        }

        $this->db->select('bap.bap_id, bap_tgl_kejadian, bap_tgl_surat, bap_karyawan_nik, bap_karyawan_nama, bap_proses,  desc_desc_id, desc_pilihan, branch_branch_id, branch_kode, branch_nama,             
            bap.user_user_id,   user_name, user_full_name,
            bap_input_date, bap_last_update');
        $this->db->join('branch', 'branch.branch_id = branch_branch_id', 'left');
        $this->db->join('desc', 'desc.desc_id = desc_desc_id', 'left'); 
        $this->db->join('users', 'users.user_id = bap.user_user_id', 'left');               
        $res = $this->db->get('bap');

        if(isset($params['id']) OR (isset($params['limit']) AND $params['limit']==1))
        {
            return $res->row_array();
        }
        else
        {
            return $res->result_array();
        }
    }

    // Add and update to database
    function add($data = array()) {
        
         if(isset($data['bap_id'])) {
            $this->db->set('bap_id', $data['bap_id']);
        }
        
         if(isset($data['bap_tgl_kejadian'])) {
            $this->db->set('bap_tgl_kejadian', $data['bap_tgl_kejadian']);
        }
        
         if(isset($data['bap_tgl_surat'])) {
            $this->db->set('bap_tgl_surat', $data['bap_tgl_surat']);
        }
        
         if(isset($data['bap_karyawan_nik'])) {
            $this->db->set('bap_karyawan_nik', $data['bap_karyawan_nik']);
        }        
           
         if(isset($data['bap_karyawan_nama'])) {
            $this->db->set('bap_karyawan_nama', $data['bap_karyawan_nama']);
        }

        if(isset($data['bap_proses'])) {
            $this->db->set('bap_proses', $data['bap_proses']);
        }

        if(isset($data['desc_desc_id'])) {
            $this->db->set('desc_desc_id', $data['desc_desc_id']);
        }

        if(isset($data['branch_branch_id'])) {
            $this->db->set('branch_branch_id', $data['branch_branch_id']);
        }

         if(isset($data['user_id'])) {
            $this->db->set('user_user_id', $data['user_id']);
        }
        
         if(isset($data['bap_input_date'])) {
            $this->db->set('bap_input_date', $data['bap_input_date']);
        }
        
         if(isset($data['bap_last_update'])) {
            $this->db->set('bap_last_update', $data['bap_last_update']);
        }   
        
        if (isset($data['bap_id'])) {
            $this->db->where('bap_id', $data['bap_id']);
            $this->db->update('bap');
            $id = $data['bap_id'];
        } else {
            $this->db->insert('bap');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }
    

    // Get Type From Databases
    function get_desc($params = array()) {

        if (isset($params['id'])) {
            $this->db->where('desc.desc_id', $params['id']);
        }

        if (isset($params['desc_pilihan'])) {
            $this->db->where('desc_pilihan', $params['desc_pilihan']);
        }

        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } else {
            $this->db->order_by('desc_id', 'desc');
        }

        $this->db->select('desc_id, desc_pilihan');
        $res = $this->db->get('desc');

        if (isset($params['id'])) {
            return $res->row_array();
        } else {
            return $res->result_array();
        }
    }

    function add_desc($data = array()) {

        if (isset($data['desc_id'])) {
            $this->db->set('desc_id', $data['desc_id']);
        }

        if (isset($data['desc_pilihan'])) {
            $this->db->set('desc_pilihan', $data['desc_pilihan']);
        }

        if (isset($data['desc_id'])) {
            $this->db->where('desc_id', $data['desc_id']);
            $this->db->update('desc');
            $id = $data['desc_id'];
        } else {
            $this->db->insert('desc');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }


    // Delete to database
    function delete($id) {
        $this->db->where('bap_id', $id);
        $this->db->delete('bap');
    }

    function delete_desc($id) {
        $this->db->where('desc_id', $id);
        $this->db->delete('desc');
    }

    
}
