<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * employee Model Class
 *
 * @package     HCA
 * @subpackage  Models
 * @category    Models
 * @author      Achyar Anshorie
 */
class Employee_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array()) {
        if (isset($params['id'])) {
            $this->db->where('employee_id', $params['id']);
        }

        if (isset($params['employee_nik'])) {
            $this->db->where('employee_nik', $params['employee_nik']);
        }

        if (isset($params['date_start']) AND isset($params['date_end'])) {
            $this->db->where('employee_published_date >=', $params['date_start'] . ' 00:00:00');
            $this->db->where('employee_published_date <=', $params['date_end'] . ' 23:59:59');
        }

        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } else {
            $this->db->order_by('employee_id', 'desc');
        }

        $this->db->select('employee.employee_id, employee_nik, employee_name, employee_account, employee_place, employee_birth_date, employee_address, employee_entry_date, employee_position, employee_divisi, employee_dept, employee_unit, employee_bussiness, employee_phone');
        $res = $this->db->get('employee');

         if(isset($params['id']))
        {
            return $res->row_array();
        }
        else
        {
            return $res->result_array();
        }
    }

    // Add and update to database
    function add($data = array()) {

        if (isset($data['employee_id'])) {
            $this->db->set('employee_id', $data['employee_id']);
        }

        if (isset($data['employee_nik'])) {
            $this->db->set('employee_nik', $data['employee_nik']);
        }

        if (isset($data['employee_name'])) {
            $this->db->set('employee_name', $data['employee_name']);
        }

        if (isset($data['employee_account'])) {
            $this->db->set('employee_account', $data['employee_account']);
        }

        if (isset($data['employee_place'])) {
            $this->db->set('employee_place', $data['employee_place']);
        }

        if (isset($data['employee_birth_date'])) {
            $this->db->set('employee_birth_date', $data['employee_birth_date']);
        }

        if (isset($data['employee_address'])) {
            $this->db->set('employee_address', $data['employee_address']);
        }

        if (isset($data['employee_entry_date'])) {
            $this->db->set('employee_entry_date', $data['employee_entry_date']);
        }

        if (isset($data['employee_position'])) {
            $this->db->set('employee_position', $data['employee_position']);
        }

        if (isset($data['employee_divisi'])) {
            $this->db->set('employee_divisi', $data['employee_divisi']);
        }

        if (isset($data['employee_dept'])) {
            $this->db->set('employee_dept', $data['employee_dept']);
        }

        if (isset($data['employee_unit'])) {
            $this->db->set('employee_unit', $data['employee_unit']);
        }

        if (isset($data['employee_bussiness'])) {
            $this->db->set('employee_bussiness', $data['employee_bussiness']);
        }

        if (isset($data['employee_phone'])) {
            $this->db->set('employee_phone', $data['employee_phone']);
        }

        if (isset($data['employee_id'])) {
            $this->db->where('employee_id', $data['employee_id']);
            $this->db->update('employee');
            $id = $data['employee_id'];
        } else {
            $this->db->insert('employee');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

    // Delete to database
    function delete($id) {
        $this->db->where('employee_id', $id);
        $this->db->delete('employee');
    }

     // Delete all to database
    function delete_all() {
        $this->db->truncate('employee');
    }

    public function is_exist($field, $value)
    {
        $this->db->where($field, $value);        

        return $this->db->count_all_results('employee') > 0 ? TRUE : FALSE;
    }

}
