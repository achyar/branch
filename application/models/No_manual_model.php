<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
* Suratk Model Class
 *
 * @package     HCA
 * @subpackage  Models
 * @category    Models
 * @author      Achyar Anshorie
 */

class No_manual_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array())
    {
        if(isset($params['id']))
        {
            $this->db->where('no_manual.no_manual_id', $params['id']);
        }

        
        if(isset($params['date_start']) AND isset($params['date_end']))
        {
            $this->db->where('no_manual_date', $params['date_start']);
            $this->db->or_where('no_manual_date', $params['date_end']);
        }

        if(isset($params['limit']))
        {
            if(!isset($params['offset']))
            {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by']))
        {
            $this->db->order_by($params['order_by'], 'desc');
        }
        else
        {
            $this->db->order_by('no_manual_last_update', 'desc');
        }

        $this->db->select('no_manual.no_manual_id, no_manual_letter, no_manual_desc,             
            no_manual.user_user_id,   user_nik, user_full_name,
            no_manual_input_date, no_manual_last_update');
       
        $this->db->join('users', 'users.user_id = no_manual.user_user_id', 'left');               
        $res = $this->db->get('no_manual');

        if(isset($params['id']) OR (isset($params['limit']) AND $params['limit']==1))
        {
            return $res->row_array();
        }
        else
        {
            return $res->result_array();
        }
    }

    // Add and update to database
    function add($data = array()) {
        
         if(isset($data['no_manual_id'])) {
            $this->db->set('no_manual_id', $data['no_manual_id']);
        }
        
         if(isset($data['no_manual_letter'])) {
            $this->db->set('no_manual_letter', $data['no_manual_letter']);
        }
        
         if(isset($data['no_manual_desc'])) {
            $this->db->set('no_manual_desc', $data['no_manual_desc']);
        }
        
         if(isset($data['user_id'])) {
            $this->db->set('user_user_id', $data['user_id']);
        }
        
         if(isset($data['no_manual_input_date'])) {
            $this->db->set('no_manual_input_date', $data['no_manual_input_date']);
        }
        
         if(isset($data['no_manual_last_update'])) {
            $this->db->set('no_manual_last_update', $data['no_manual_last_update']);
        }   
        
        if (isset($data['no_manual_id'])) {
            $this->db->where('no_manual_id', $data['no_manual_id']);
            $this->db->update('no_manual');
            $id = $data['no_manual_id'];
        } else {
            $this->db->insert('no_manual');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

    // Delete to database
    function delete($id) {
        $this->db->where('no_manual_id', $id);
        $this->db->delete('no_manual');
    }
    
}
