<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
* Posts Model Class
 *
 * @package     HCA
 * @subpackage  Models
 * @category    Models
 * @author      Achyar Anshorie
 */

class Prakerin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array())
    {
        if(isset($params['id']))
        {
            $this->db->where('prakerin.prakerin_id', $params['id']);
        }
        if(isset($params['prakerin_nip']))
        {
            $this->db->where('prakerin_nip', $params['prakerin_nip']);
        }
        
        if(isset($params['prakerin_password']))
        {
            $this->db->where('prakerin_password', $params['prakerin_password']);
        }
        if(isset($params['prakerin_name']))
        {
            $this->db->where('prakerin_name', $params['prakerin_name']);
        }

        if(isset($params['employee_nik']))
        {
            $this->db->where('employee.prakerin_mentor_nik', $params['employee_nik']);
        }

        if(isset($params['status']))
        {
            $this->db->where('prakerin_status', $params['status']);
        }
        if(isset($params['date_start']) AND isset($params['date_end']))
        {
            $this->db->where('prakerin_entry_date >=', $params['date_start'] . ' 00:00:00');
            $this->db->where('prakerin_entry_date <=', $params['date_end'] . ' 23:59:59');
        }

        if(isset($params['limit']))
        {
            if(!isset($params['offset']))
            {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by']))
        {
            $this->db->order_by($params['order_by'], 'desc');
        }
        else
        {
            $this->db->order_by('prakerin_nip', 'desc');
        }

        $this->db->select('prakerin.prakerin_id, prakerin_nip,  prakerin_password, prakerin_name,
            prakerin_gender, prakerin_place, prakerin_birth_date, prakerin_school, prakerin_phone, prakerin_address, 
            prakerin_image, prakerin_education, prakerin_dept, prakerin_mentor_nik, prakerin_mentor_name, prakerin_status, prakerin_entry_date, prakerin_out_date,
            prakerin_input_date, prakerin_last_update');
        $this->db->join('employee', 'employee.employee_nik = prakerin_mentor_nik', 'left'); 
        $this->db->join('users', 'users.user_id = prakerin.user_user_id', 'left'); 
        $res = $this->db->get('prakerin');
 
        if(isset($params['id']) OR (isset($params['limit']) AND $params['limit'] == 1) OR (isset($params['prakerin_nip'])))
        {
            return $res->row_array();
        }
        else
        {
            return $res->result_array();
        }
    }

    // Add and update to database
    function add($data = array()) {
        
         if(isset($data['prakerin_id'])) {
            $this->db->set('prakerin_id', $data['prakerin_id']);
        }
        
         if(isset($data['prakerin_nip'])) {
            $this->db->set('prakerin_nip', $data['prakerin_nip']);
        }
    
         if(isset($data['prakerin_password'])) {
            $this->db->set('prakerin_password', $data['prakerin_password']);
        }
        
         if(isset($data['prakerin_name'])) {
            $this->db->set('prakerin_name', $data['prakerin_name']);
        }
        
         if(isset($data['prakerin_place'])) {
            $this->db->set('prakerin_place', $data['prakerin_place']);
        }
         
         if(isset($data['prakerin_place'])) {
            $this->db->set('prakerin_place', $data['prakerin_place']);
        }
        
         if(isset($data['prakerin_place'])) {
            $this->db->set('prakerin_place', $data['prakerin_place']);
        }

         if(isset($data['prakerin_birth_date'])) {
            $this->db->set('prakerin_birth_date', $data['prakerin_birth_date']);
        }

         if(isset($data['prakerin_education'])) {
            $this->db->set('prakerin_education', $data['prakerin_education']);
        }

         if(isset($data['prakerin_school'])) {
            $this->db->set('prakerin_school', $data['prakerin_school']);
        }

         if(isset($data['prakerin_address'])) {
            $this->db->set('prakerin_address', $data['prakerin_address']);
        }

         if(isset($data['prakerin_gender'])) {
            $this->db->set('prakerin_gender', $data['prakerin_gender']);
        }

         if(isset($data['prakerin_entry_date'])) {
            $this->db->set('prakerin_entry_date', $data['prakerin_entry_date']);
        } 

         if(isset($data['prakerin_out_date'])) {
            $this->db->set('prakerin_out_date', $data['prakerin_out_date']);
        } 

         if(isset($data['prakerin_phone'])) {
            $this->db->set('prakerin_phone', $data['prakerin_phone']);
        } 

         if(isset($data['prakerin_status'])) {
            $this->db->set('prakerin_status', $data['prakerin_status']);
        } 

         if(isset($data['prakerin_image'])) {
            $this->db->set('prakerin_image', $data['prakerin_image']);
        } 

         if(isset($data['prakerin_mentor_nik'])) {
            $this->db->set('prakerin_mentor_nik', $data['prakerin_mentor_nik']);
        } 

         if(isset($data['prakerin_mentor_name'])) {
            $this->db->set('prakerin_mentor_name', $data['prakerin_mentor_name']);
        } 

         if(isset($data['prakerin_dept'])) {
            $this->db->set('prakerin_dept', $data['prakerin_dept']);
        } 

          if(isset($data['user_id'])) {
            $this->db->set('user_user_id', $data['user_id']);
        }

        if(isset($data['prakerin_input_date'])) {
            $this->db->set('prakerin_input_date', $data['prakerin_input_date']);
        }
       
         if(isset($data['prakerin_last_update'])) {
            $this->db->set('prakerin_last_update', $data['prakerin_last_update']);
        }
        
        if (isset($data['prakerin_id'])) {
            $this->db->where('prakerin_id', $data['prakerin_id']);
            $this->db->update('prakerin');
            $id = $data['prakerin_id'];
        } else {
            $this->db->insert('prakerin');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }
    
    
    // Delete to database
    function delete($id) {
        $this->db->where('prakerin_id', $id);
        $this->db->delete('prakerin');
    }

    function change_password($id, $params) {
        $this->db->where('prakerin_id', $id);
        $this->db->update('prakerin', $params);
    }
    
    
}
