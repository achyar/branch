<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
* Suratk Model Class
 *
 * @package     HCA
 * @subpackage  Models
 * @category    Models
 * @author      Achyar Anshorie
 */

class Supra_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array())
    {
        if(isset($params['id']))
        {
            $this->db->where('supra.supra_id', $params['id']);
        }

        if (isset($params['multiple_id'])) {
            $this->db->where_in('supra.supra_id', $params['multiple_id']);
        }

        if(isset($params['prakerin_nip']))
        {
            $this->db->where('prakerin.supra_nip', $params['prakerin_nip']);
        }
        
        if(isset($params['date_start']) AND isset($params['date_end']))
        {
            $this->db->where('supra_input_date', $params['date_start']);
            $this->db->or_where('supra_input_date', $params['date_end']);
        }

        if(isset($params['limit']))
        {
            if(!isset($params['offset']))
            {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by']))
        {
            $this->db->order_by($params['order_by'], 'desc');
        }
        else
        {
            $this->db->order_by('supra_last_update', 'desc');
        }

        $this->db->select('supra.supra_id, supra_number, supra_nip, supra_name, supra_dept, supra_entry_date, supra_out_date, supra_education, supra_school,            
            supra.user_user_id,   user_nik, user_full_name,
            supra_input_date, supra_last_update');
        $this->db->join('prakerin', 'prakerin.prakerin_nip = supra_nip', 'left'); 
        $this->db->join('users', 'users.user_id = supra.user_user_id', 'left');               
        $res = $this->db->get('supra');

        if(isset($params['id']) OR (isset($params['limit']) AND $params['limit']==1))
        {
            return $res->row_array();
        }
        else
        {
            return $res->result_array();
        }
    }

    // Add and update to database
    function add($data = array()) {
        
         if(isset($data['supra_id'])) {
            $this->db->set('supra_id', $data['supra_id']);
        }
        
         if(isset($data['supra_number'])) {
            $this->db->set('supra_number', $data['supra_number']);
        }
        
         if(isset($data['supra_nip'])) {
            $this->db->set('supra_nip', $data['supra_nip']);
        }
        
         if(isset($data['supra_name'])) {
            $this->db->set('supra_name', $data['supra_name']);
        }        
           
         if(isset($data['supra_dept'])) {
            $this->db->set('supra_dept', $data['supra_dept']);
        }

        if(isset($data['supra_education'])) {
            $this->db->set('supra_education', $data['supra_education']);
        }

        if(isset($data['supra_school'])) {
            $this->db->set('supra_school', $data['supra_school']);
        }

        if(isset($data['supra_entry_date'])) {
            $this->db->set('supra_entry_date', $data['supra_entry_date']);
        }

        if(isset($data['supra_out_date'])) {
            $this->db->set('supra_out_date', $data['supra_out_date']);
        }

         if(isset($data['user_id'])) {
            $this->db->set('user_user_id', $data['user_id']);
        }
        
         if(isset($data['supra_input_date'])) {
            $this->db->set('supra_input_date', $data['supra_input_date']);
        }
        
         if(isset($data['supra_last_update'])) {
            $this->db->set('supra_last_update', $data['supra_last_update']);
        }   
        
        if (isset($data['supra_id'])) {
            $this->db->where('supra_id', $data['supra_id']);
            $this->db->update('supra');
            $id = $data['supra_id'];
        } else {
            $this->db->insert('supra');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }
    
    // Delete to database
    function delete($id) {
        $this->db->where('supra_id', $id);
        $this->db->delete('supra');
    }

    
}
