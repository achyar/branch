<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
* Suratk Model Class
 *
 * @package     HCA
 * @subpackage  Models
 * @category    Models
 * @author      Achyar Anshorie
 */

class Sket_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array())
    {
        if(isset($params['id']))
        {
            $this->db->where('sket.sket_id', $params['id']);
        }

        if (isset($params['multiple_id'])) {
            $this->db->where_in('sket.sket_id', $params['multiple_id']);
        }

        if(isset($params['employee_nik']))
        {
            $this->db->where('employee.sket_employee_nik', $params['employee_nik']);
        }
        
        if(isset($params['date_start']) AND isset($params['date_end']))
        {
            $this->db->where('sket_date', $params['date_start']);
            $this->db->or_where('sket_date', $params['date_end']);
        }

        if(isset($params['limit']))
        {
            if(!isset($params['offset']))
            {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by']))
        {
            $this->db->order_by($params['order_by'], 'desc');
        }
        else
        {
            $this->db->order_by('sket_last_update', 'desc');
        }

        $this->db->select('sket.sket_id, sket_number, sket_employee_nik, sket_employee_name, sket_employee_position, sket_employee_birth_date, sket_employee_place, sket_employee_address, sket_status, sket_employee_entry_date, ket_ket_id, ket_surat,             
            sket.user_user_id,   user_nik, user_full_name,
            sket_input_date, sket_last_update');
        $this->db->join('employee', 'employee.employee_nik = sket_employee_nik', 'left'); 
        $this->db->join('ket', 'ket.ket_id = ket_ket_id', 'left');
        $this->db->join('users', 'users.user_id = sket.user_user_id', 'left');               
        $res = $this->db->get('sket');

        if(isset($params['id']) OR (isset($params['limit']) AND $params['limit']==1))
        {
            return $res->row_array();
        }
        else
        {
            return $res->result_array();
        }
    }

    // Add and update to database
    function add($data = array()) {
        
         if(isset($data['sket_id'])) {
            $this->db->set('sket_id', $data['sket_id']);
        }
        
         if(isset($data['sket_number'])) {
            $this->db->set('sket_number', $data['sket_number']);
        }
        
         if(isset($data['sket_employee_nik'])) {
            $this->db->set('sket_employee_nik', $data['sket_employee_nik']);
        }
        
         if(isset($data['sket_employee_name'])) {
            $this->db->set('sket_employee_name', $data['sket_employee_name']);
        }        
           
         if(isset($data['sket_employee_place'])) {
            $this->db->set('sket_employee_place', $data['sket_employee_place']);
        }

        if(isset($data['sket_employee_birth_date'])) {
            $this->db->set('sket_employee_birth_date', $data['sket_employee_birth_date']);
        }

        if(isset($data['sket_employee_entry_date'])) {
            $this->db->set('sket_employee_entry_date', $data['sket_employee_entry_date']);
        }

        if(isset($data['sket_employee_position'])) {
            $this->db->set('sket_employee_position', $data['sket_employee_position']);
        }

        if(isset($data['sket_employee_address'])) {
            $this->db->set('sket_employee_address', $data['sket_employee_address']);
        }

        if(isset($data['sket_status'])) {
            $this->db->set('sket_status', $data['sket_status']);
        }

        if(isset($data['ket_ket_id'])) {
            $this->db->set('ket_ket_id', $data['ket_ket_id']);
        }

         if(isset($data['user_id'])) {
            $this->db->set('user_user_id', $data['user_id']);
        }
        
         if(isset($data['sket_input_date'])) {
            $this->db->set('sket_input_date', $data['sket_input_date']);
        }
        
         if(isset($data['sket_last_update'])) {
            $this->db->set('sket_last_update', $data['sket_last_update']);
        }   
        
        if (isset($data['sket_id'])) {
            $this->db->where('sket_id', $data['sket_id']);
            $this->db->update('sket');
            $id = $data['sket_id'];
        } else {
            $this->db->insert('sket');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }
    

    // Get Type From Databases
    function get_ket($params = array()) {

        if (isset($params['id'])) {
            $this->db->where('ket.ket_id', $params['id']);
        }

        if (isset($params['ket_surat'])) {
            $this->db->where('ket_surat', $params['ket_surat']);
        }

        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } else {
            $this->db->order_by('ket_id', 'desc');
        }

        $this->db->select('ket_id, ket_surat');
        $res = $this->db->get('ket');

        if (isset($params['id'])) {
            return $res->row_array();
        } else {
            return $res->result_array();
        }
    }

    function add_ket($data = array()) {

        if (isset($data['ket_id'])) {
            $this->db->set('ket_id', $data['ket_id']);
        }

        if (isset($data['ket_surat'])) {
            $this->db->set('ket_surat', $data['ket_surat']);
        }

        if (isset($data['ket_id'])) {
            $this->db->where('ket_id', $data['ket_id']);
            $this->db->update('ket');
            $id = $data['ket_id'];
        } else {
            $this->db->insert('ket');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }


    // Delete to database
    function delete($id) {
        $this->db->where('sket_id', $id);
        $this->db->delete('sket');
    }

    function delete_ket($id) {
        $this->db->where('ket_id', $id);
        $this->db->delete('ket');
    }

    
}
