<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
* branch Model Class
 *
 * @package     HRA CMS
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Achyar Anshorie
 */

class Branch_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Get From Databases
    function get($params = array())
    {
        if(isset($params['id']))
        {
            $this->db->where('branch.branch_id', $params['id']); 
        }


        if(isset($params['branch_kode']))
        {
            $this->db->where('branch.branch_kode', $params['branch_kode']);
        }

        if(isset($params['branch_nama']))
        {
            $this->db->where('branch.branch_nama', $params['branch_nama']);
        }
        
        
        if(isset($params['limit']))
        {
            if(!isset($params['offset']))
            {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if(isset($params['order_by']))
        {
            $this->db->order_by($params['order_by'], 'asc');
        }        

        $this->db->select('branch.branch_id, branch_kode, branch_nama');             
        $res = $this->db->get('branch');

        if(isset($params['id']))
        {
            return $res->row_array();
        }
        else
        {
            return $res->result_array();
        }
    }

    // Add and update to database
    function add($data = array()) {

     if(isset($data['branch_id'])) {
        $this->db->set('branch_id', $data['branch_id']);
    }

    if(isset($data['branch_kode'])) {
        $this->db->set('branch_kode', $data['branch_kode']);
    }    

    if(isset($data['branch_nama'])) {
        $this->db->set('branch_nama', $data['branch_nama']);
    } 
    
    if (isset($data['branch_id'])) {
        $this->db->where('branch_id', $data['branch_id']);
        $this->db->update('branch');
        $id = $data['branch_id'];
    } else {
        $this->db->insert('branch');
        $id = $this->db->insert_id();
    }

    $status = $this->db->affected_rows();
    return ($status == 0) ? FALSE : $id;
}

    // Delete to database
function delete($id) {
    $this->db->where('branch_id', $id);
    $this->db->delete('branch');
}

}
