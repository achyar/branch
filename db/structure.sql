SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `user_roles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `user_roles` (
  `role_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `role_name` VARCHAR(100) NULL DEFAULT NULL ,
  PRIMARY KEY (`role_id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `user_nik` VARCHAR(100) NULL DEFAULT NULL ,
  `user_password` VARCHAR(45) NULL DEFAULT NULL ,
  `user_full_name` VARCHAR(255) NULL DEFAULT NULL ,
  `user_image` VARCHAR(255) NULL ,
  `user_role_role_id` INT(11) NULL ,
  `user_is_deleted` TINYINT(1) NULL DEFAULT 0 ,
  `user_input_date` TIMESTAMP NULL DEFAULT NULL ,
  `user_last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`user_id`) ,
  INDEX `fk_user_user_role1_idx` (`user_role_role_id` ASC) ,
  CONSTRAINT `fk_user_user_role1`
    FOREIGN KEY (`user_role_role_id` )
    REFERENCES `user_roles` (`role_id` )
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `logs`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `logs` (
  `log_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `log_date` TIMESTAMP NULL DEFAULT NULL ,
  `log_action` VARCHAR(45) NULL DEFAULT NULL ,
  `log_module` VARCHAR(45) NULL DEFAULT NULL ,
  `log_info` TEXT NULL DEFAULT NULL ,
  `user_id` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`log_id`) ,
  INDEX `fk_g_activity_log_g_user1_idx` (`user_id` ASC) ,
  CONSTRAINT `fk_g_activity_log_g_user1`
    FOREIGN KEY (`user_id` )
    REFERENCES `users` (`user_id` )
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


-- -----------------------------------------------------
-- Table `ci_session`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ci_session` (
  `id` VARCHAR(40) NOT NULL ,
  `ip_address` VARCHAR(45) NOT NULL ,
  `timestamp` INT(10) UNSIGNED NOT NULL DEFAULT '0' ,
  `data` BLOB NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `ci_sessions_timestamp` (`timestamp` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `ket`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `ket` (
  `ket_id` INT NOT NULL AUTO_INCREMENT ,
  `ket_surat` TEXT NULL ,
  PRIMARY KEY (`ket_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `sket`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `sket` (
  `sket_id` INT NOT NULL AUTO_INCREMENT ,
  `sket_number` VARCHAR(45) NULL ,
  `sket_employee_nik` VARCHAR(8) NULL ,
  `sket_employee_name` VARCHAR(255) NULL ,
  `sket_employee_position` VARCHAR(100) NULL ,
  `sket_employee_place` VARCHAR(100) NULL ,
  `sket_employee_birth_date` DATE NULL ,
  `sket_employee_address` TEXT NULL ,
  `sket_employee_entry_date` DATE NULL ,
  `sket_status` ENUM('kontrak','tetap') NULL ,
  `ket_ket_id` INT NULL ,
  `user_user_id` INT NULL ,
  `sket_input_date` TIMESTAMP NULL ,
  `sket_last_update` TIMESTAMP NULL ,
  PRIMARY KEY (`sket_id`) ,
  INDEX `fk_sket_ket1_idx` (`ket_ket_id` ASC) ,
  CONSTRAINT `fk_sket_ket1`
    FOREIGN KEY (`ket_ket_id` )
    REFERENCES `ket` (`ket_id` )
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `disnaker`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `disnaker` (
  `disnaker_id` INT NOT NULL AUTO_INCREMENT ,
  `disnaker_number` VARCHAR(100) NULL ,
  `disnaker_employee_nik` VARCHAR(8) NULL ,
  `disnaker_employee_name` VARCHAR(255) NULL ,
  `disnaker_employee_address` TEXT NULL ,
  `disnaker_employee_entry_date` DATE NULL ,
  `disnaker_employee_out_date` DATE NULL ,
  `user_user_id` INT NULL ,
  `disnaker_input_date` TIMESTAMP NULL ,
  `disnaker_last_update` TIMESTAMP NULL ,
  PRIMARY KEY (`disnaker_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `employee`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `employee` (
  `employee_id` INT NOT NULL AUTO_INCREMENT ,
  `employee_nik` VARCHAR(8) NULL ,
  `employee_name` VARCHAR(255) NULL ,
  `employee_account` VARCHAR(25) NULL ,
  `employee_place` VARCHAR(100) NULL ,
  `employee_birth_date` DATE NULL ,
  `employee_address` TEXT NULL ,
  `employee_entry_date` DATE NULL ,
  `employee_position` VARCHAR(100) NULL ,
  `employee_divisi` VARCHAR(100) NULL ,
  `employee_dept` VARCHAR(100) NULL ,
  `employee_unit` VARCHAR(4) NULL ,
  `employee_bussiness` VARCHAR(255) NULL ,
  `employee_phone` VARCHAR(25) NULL ,
  PRIMARY KEY (`employee_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpjs`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bpjs` (
  `bpjs_id` INT NOT NULL AUTO_INCREMENT ,
  `bpjs_noka` VARCHAR(100) NULL ,
  `bpjs_ktp` VARCHAR(16) NULL ,
  `bpjs_npp` VARCHAR(8) NULL ,
  `bpjs_name` VARCHAR(255) NULL ,
  `bpjs_hub` ENUM('PESERTA','ISTRI','SUAMI','ANAK') NULL ,
  `bpjs_date` DATE NULL ,
  `bpjs_tmt` DATE NULL ,
  `bpjs_faskes` VARCHAR(255) NULL ,
  `bpjs_kelas` ENUM('1','2') NULL ,
  `bpjs_cetak` TINYINT(1) NULL DEFAULT 0 ,
  `bpjs_input_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `bpjs_last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  PRIMARY KEY (`bpjs_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `setting`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `setting` (
  `setting_id` INT NOT NULL AUTO_INCREMENT ,
  `setting_name` VARCHAR(255) NULL ,
  `setting_value` TEXT NULL ,
  `setting_last_update` TIMESTAMP NULL ,
  PRIMARY KEY (`setting_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bpjstk`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bpjstk` (
  `bpjstk_id` INT NOT NULL AUTO_INCREMENT ,
  `bpjstk_name` VARCHAR(255) NULL ,
  `bpjstk_card` VARCHAR(100) NULL ,
  `bpjstk_npp` VARCHAR(45) NULL ,
  `bpjstk_entry_date` DATE NULL ,
  `bpjstk_desc` TEXT NULL ,
  `user_user_id` INT NULL ,
  `bpjstk_input_date` TIMESTAMP NULL ,
  `bpjstk_last_update` TIMESTAMP NULL ,
  PRIMARY KEY (`bpjstk_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `procuration`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `procuration` (
  `procuration_id` INT NOT NULL AUTO_INCREMENT ,
  `procuration_number` VARCHAR(45) NULL ,
  `procuration_employee_nik` VARCHAR(8) NULL ,
  `procuration_employee_name` VARCHAR(255) NULL ,
  `procuration_employee_position` VARCHAR(100) NULL ,
  `procuration_desc` TEXT NULL ,
  `user_user_id` INT NULL ,
  `procuration_input_date` TIMESTAMP NULL ,
  `procuration_last_update` TIMESTAMP NULL ,
  PRIMARY KEY (`procuration_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `letter`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `letter` (
  `letter_id` INT NOT NULL AUTO_INCREMENT ,
  `letter_number` VARCHAR(100) NULL ,
  `letter_month` INT NULL ,
  `letter_year` YEAR NULL ,
  PRIMARY KEY (`letter_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contract`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `contract` (
  `contract_id` INT NOT NULL AUTO_INCREMENT ,
  `contract_number` VARCHAR(100) NULL ,
  `contract_employee_nik` VARCHAR(8) NULL ,
  `contract_employee_name` VARCHAR(255) NULL ,
  `contract_employee_position` VARCHAR(100) NULL ,
  `contract_option` ENUM('1','2') NULL ,
  `contract_date` DATE NULL ,
  `user_user_id` INT NULL ,
  `contract_input_date` TIMESTAMP NULL ,
  `contract_last_update` TIMESTAMP NULL ,
  PRIMARY KEY (`contract_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cost`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cost` (
  `cost_id` INT NOT NULL AUTO_INCREMENT ,
  `cost_code` VARCHAR(45) NULL ,
  `cost_name` VARCHAR(45) NULL ,
  PRIMARY KEY (`cost_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `par`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `par` (
  `par_id` INT NOT NULL AUTO_INCREMENT ,
  `par_number` VARCHAR(100) NULL ,
  `par_employee_nik` VARCHAR(8) NULL ,
  `par_employee_name` VARCHAR(255) NULL ,
  `par_employee_position` VARCHAR(100) NULL ,
  `par_employee_account` VARCHAR(25) NULL ,
  `par_employee_unit` VARCHAR(4) NULL ,
  `par_employee_bussiness` VARCHAR(255) NULL ,
  `par_employee_dept` VARCHAR(100) NULL ,
  `par_paid` DECIMAL NULL ,
  `cost_cost_id` INT NULL ,
  `par_input_date` TIMESTAMP NULL ,
  `par_last_update` TIMESTAMP NULL ,
  `user_user_id` INT NULL ,
  PRIMARY KEY (`par_id`) ,
  INDEX `fk_par_cost1_idx` (`cost_cost_id` ASC) ,
  CONSTRAINT `fk_par_cost1`
    FOREIGN KEY (`cost_cost_id` )
    REFERENCES `cost` (`cost_id` )
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bank`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `bank` (
  `bank_id` INT NOT NULL AUTO_INCREMENT ,
  `bank_name` VARCHAR(100) NULL ,
  PRIMARY KEY (`bank_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `spb`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `spb` (
  `spb_id` INT NOT NULL AUTO_INCREMENT ,
  `spb_number` VARCHAR(100) NULL ,
  `spb_date` DATE NULL ,
  `spb_name1` VARCHAR(255) NULL ,
  `spb_name2` VARCHAR(255) NULL ,
  `spb_name3` VARCHAR(255) NULL ,
  `spb_name4` VARCHAR(255) NULL ,
  `spb_name5` VARCHAR(255) NULL ,
  `spb_name6` VARCHAR(255) NULL ,
  `spb_name7` VARCHAR(255) NULL ,
  `spb_name8` VARCHAR(255) NULL ,
  `spb_name9` VARCHAR(255) NULL ,
  `spb_name10` VARCHAR(255) NULL ,
  `spb_nik1` VARCHAR(16) NULL ,
  `spb_nik2` VARCHAR(16) NULL ,
  `spb_nik3` VARCHAR(16) NULL ,
  `spb_nik4` VARCHAR(16) NULL ,
  `spb_nik5` VARCHAR(16) NULL ,
  `spb_nik6` VARCHAR(16) NULL ,
  `spb_nik7` VARCHAR(16) NULL ,
  `spb_nik8` VARCHAR(16) NULL ,
  `spb_nik9` VARCHAR(16) NULL ,
  `spb_nik10` VARCHAR(16) NULL ,
  `spb_input_date` TIMESTAMP NULL ,
  `spb_last_update` TIMESTAMP NULL ,
  `bank_bank_id` INT NULL ,
  `user_user_id` INT NULL ,
  PRIMARY KEY (`spb_id`) ,
  INDEX `fk_spb_bank1_idx` (`bank_bank_id` ASC) ,
  CONSTRAINT `fk_spb_bank1`
    FOREIGN KEY (`bank_bank_id` )
    REFERENCES `bank` (`bank_id` )
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `prakerin`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `prakerin` (
  `prakerin_id` INT NOT NULL AUTO_INCREMENT ,
  `prakerin_nip` VARCHAR(8) NULL ,
  `prakerin_password` VARCHAR(45) NULL ,
  `prakerin_name` VARCHAR(255) NULL ,
  `prakerin_place` VARCHAR(100) NULL ,
  `prakerin_birth_date` DATE NULL ,
  `prakerin_education` ENUM('SMK','SMA','UNIVERSITAS','AKADEMI') NULL ,
  `prakerin_school` VARCHAR(100) NULL ,
  `prakerin_address` TEXT NULL ,
  `prakerin_gender` ENUM('LAKI-LAKI','PEREMPUAN') NULL ,
  `prakerin_entry_date` DATE NULL ,
  `prakerin_out_date` DATE NULL DEFAULT NULL ,
  `prakerin_phone` VARCHAR(25) NULL ,
  `prakerin_status` TINYINT(1) NULL DEFAULT 0 ,
  `prakerin_image` VARCHAR(255) NULL ,
  `prakerin_mentor_nik` VARCHAR(8) NULL ,
  `prakerin_mentor_name` VARCHAR(255) NULL ,
  `prakerin_dept` VARCHAR(100) NULL ,
  `user_user_id` INT NULL ,
  `prakerin_input_date` TIMESTAMP NULL ,
  `prakerin_last_update` TIMESTAMP NULL ,
  PRIMARY KEY (`prakerin_id`, `prakerin_nip`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `present`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `present` (
  `present_id` INT NOT NULL AUTO_INCREMENT ,
  `present_year` YEAR NULL ,
  `present_month` INT NULL ,
  `present_date` DATETIME NULL ,
  `present_entry_time` TIME NULL ,
  `present_out_time` TIME NULL ,
  `present_desc` ENUM('HADIR','SAKIT','IZIN') NULL ,
  `present_is_late` TINYINT(1) NULL DEFAULT 0 ,
  `present_is_before` TINYINT(1) NULL DEFAULT 0 ,
  `prakerin_prakerin_id` INT NULL ,
  `prakerin_prakerin_nip` VARCHAR(8) NULL ,
  PRIMARY KEY (`present_id`) ,
  INDEX `fk_present_prakerin1_idx` (`prakerin_prakerin_id` ASC, `prakerin_prakerin_nip` ASC) ,
  CONSTRAINT `fk_present_prakerin1`
    FOREIGN KEY (`prakerin_prakerin_id` , `prakerin_prakerin_nip` )
    REFERENCES `prakerin` (`prakerin_id` , `prakerin_nip` )
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `supra`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `supra` (
  `supra_id` INT NOT NULL AUTO_INCREMENT ,
  `supra_number` VARCHAR(45) NULL ,
  `supra_nip` VARCHAR(8) NULL ,
  `supra_name` VARCHAR(255) NULL ,
  `supra_dept` VARCHAR(100) NULL ,
  `supra_education` VARCHAR(45) NULL ,
  `supra_school` VARCHAR(45) NULL ,
  `supra_entry_date` DATE NULL ,
  `supra_out_date` DATE NULL ,
  `supra_input_date` TIMESTAMP NULL ,
  `supra_last_update` TIMESTAMP NULL ,
  `user_user_id` INT NULL ,
  PRIMARY KEY (`supra_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `agenda`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `agenda` (
  `agenda_id` INT NOT NULL AUTO_INCREMENT ,
  `agenda_text` TEXT NULL ,
  `agenda_date` DATE NULL ,
  `agenda_status` TINYINT(1) NULL DEFAULT 0 ,
  `user_user_id` INT NULL ,
  `agenda_input_date` TIMESTAMP NULL ,
  `agenda_last_update` TIMESTAMP NULL ,
  PRIMARY KEY (`agenda_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `no_manual`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `no_manual` (
  `no_manual_id` INT NOT NULL AUTO_INCREMENT ,
  `no_manual_letter` VARCHAR(45) NULL ,
  `no_manual_desc` TEXT NULL ,
  `user_user_id` INT NULL ,
  `no_manual_input_date` TIMESTAMP NULL ,
  `no_manual_last_update` TIMESTAMP NULL ,
  PRIMARY KEY (`no_manual_id`) )
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
