
--
-- Dumping data for table `user_role`
--

INSERT INTO `user_roles` (`role_id`, `role_name`) VALUES
(1, 'SPECIAL_USER'),
(2, 'USER');

--
-- Dumping data for table `user`
--

INSERT INTO `users` (`user_id`, `user_nik`, `user_password`, `user_full_name`,  `user_input_date`, `user_last_update`, `user_role_role_id`, `user_is_deleted`) VALUES
(1, '12096398', '7a992b774936aea5299d75a419414d8c875e7e68', 'Administrator', '2016-10-23 04:32:54', '2016-10-23 04:32:54', 1, 0);

INSERT INTO `setting` (`setting_id`, `setting_name`, `setting_value`, `setting_last_update`) VALUES
(1, 'setting_branch', '-', NOW()),
(2, 'setting_address', '-', NOW()),
(3, 'setting_city', '-', NOW()),
(4, 'setting_tlp', '-', NOW()),
(5, 'setting_employe_nik', '-', NOW()),
(6, 'setting_employe_name', '-', NOW()),
(7, 'setting_employe_position', '-', NOW()),
(8, 'setting_initial', '-', NOW()),
(9, 'setting_initial_bm', '-', NOW()),
(10, 'setting_initial_pdm', '-', NOW()),
(11, 'setting_unit', '-', NOW());