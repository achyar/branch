
-- -----------------------------------------------------
-- Schema neptune
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `user_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_roles` (
  `role_id` INT(11) NOT NULL AUTO_INCREMENT,
  `role_name` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`))

AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(100) NULL DEFAULT NULL,
  `user_password` VARCHAR(45) NULL DEFAULT NULL,
  `user_full_name` VARCHAR(255) NULL DEFAULT NULL,
  `user_image` VARCHAR(255) NULL,
  `user_role_role_id` INT(11) NULL,
  `user_is_deleted` TINYINT(1) NULL DEFAULT 0,
  `user_input_date` TIMESTAMP NULL DEFAULT NULL,
  `user_last_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  INDEX `fk_user_user_role1_idx` (`user_role_role_id` ASC),
  CONSTRAINT `fk_user_user_role1`
    FOREIGN KEY (`user_role_role_id`)
    REFERENCES `user_roles` (`role_id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)

AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `logs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `logs` (
  `log_id` INT(11) NOT NULL AUTO_INCREMENT,
  `log_date` TIMESTAMP NULL DEFAULT NULL,
  `log_action` VARCHAR(45) NULL DEFAULT NULL,
  `log_module` VARCHAR(45) NULL DEFAULT NULL,
  `log_info` TEXT NULL DEFAULT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  INDEX `fk_g_activity_log_g_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_g_activity_log_g_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`user_id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)

DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ci_session`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ci_session` (
  `id` VARCHAR(40) NOT NULL,
  `ip_address` VARCHAR(45) NOT NULL,
  `timestamp` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` BLOB NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `ci_sessions_timestamp` (`timestamp` ASC))
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `variant`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `variant` (
  `variant_id` INT NOT NULL AUTO_INCREMENT,
  `variant_name` VARCHAR(45) NULL,
  PRIMARY KEY (`variant_id`));



-- -----------------------------------------------------
-- Table `category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `category` (
  `category_id` INT NOT NULL AUTO_INCREMENT,
  `category_name` VARCHAR(100) NULL,
  `category_input_date` TIMESTAMP NULL,
  `category_last_update` TIMESTAMP NULL,
  PRIMARY KEY (`category_id`));



-- -----------------------------------------------------
-- Table `supplier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `supplier` (
  `supplier_id` INT NOT NULL AUTO_INCREMENT,
  `supplier_name` VARCHAR(255) NULL,
  `supplier_phone` VARCHAR(45) NULL,
  `supplier_input_date` TIMESTAMP NULL,
  `supplier_last_update` TIMESTAMP NULL,
  PRIMARY KEY (`supplier_id`));



-- -----------------------------------------------------
-- Table `catalog`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `catalog` (
  `catalog_id` INT NOT NULL AUTO_INCREMENT,
  `catalog_sku` VARCHAR(100) NULL,
  `catalog_name` VARCHAR(255) NULL,
  `catalog_bhn` VARCHAR(100) NULL,
  `catalog_model` VARCHAR(100) NULL,
  `catalog_weight` DECIMAL NULL,
  `catalog_price` DECIMAL NULL,
  `catalog_desc` TEXT NULL,
  `catalog_image` VARCHAR(255) NULL,
  `catalog_stock` DECIMAL(10,0) NULL DEFAULT 0,
  `variant_variant_id` INT NULL,
  `category_category_id` INT NULL,
  `supplier_supplier_id` INT NULL,
  `user_user_id` INT NULL,
  `catalog_input_date` TIMESTAMP NULL,
  `catalog_last_update` TIMESTAMP NULL,
  PRIMARY KEY (`catalog_id`),
  INDEX `fk_catalog_variant1_idx` (`variant_variant_id` ASC),
  INDEX `fk_catalog_category1_idx` (`category_category_id` ASC),
  INDEX `fk_catalog_supplier1_idx` (`supplier_supplier_id` ASC),
  CONSTRAINT `fk_catalog_variant1`
    FOREIGN KEY (`variant_variant_id`)
    REFERENCES `variant` (`variant_id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL,
  CONSTRAINT `fk_catalog_category1`
    FOREIGN KEY (`category_category_id`)
    REFERENCES `category` (`category_id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL,
  CONSTRAINT `fk_catalog_supplier1`
    FOREIGN KEY (`supplier_supplier_id`)
    REFERENCES `supplier` (`supplier_id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL);



-- -----------------------------------------------------
-- Table `catalog_image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `catalog_image` (
  `catalog_image_id` INT NOT NULL AUTO_INCREMENT,
  `catalog_catalog_id` INT NULL,
  `catalog_image_path` VARCHAR(255) NULL,
  PRIMARY KEY (`catalog_image_id`),
  INDEX `fk_catalog_image_catalog1_idx` (`catalog_catalog_id` ASC),
  CONSTRAINT `fk_catalog_image_catalog1`
    FOREIGN KEY (`catalog_catalog_id`)
    REFERENCES `catalog` (`catalog_id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL);

